#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "toolkit/transform_layer"

include { _main } from './main'

workflow {
    _main()
}