#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/conform_layer"

include { _main } from './main'

workflow {
    _main()
}