### Managing Change

Functions evolve with the modelling process, but Workflow Templates are designed around specific schema constructs,
and schemas are designed around specific R implementations. 
Modifying a Template or Schema can impact existing user Projects or the reproducability of previous Results.

The addition of new parameters to a model is reasonably straight forward, but changing existing 
requires careful thought. The schema structure ultimately defines expected behaviour within the 
platform UI and existing user Projects.

While historic Nextflow pipelines and Docker images are retained in this repository 
for reproducibility, the platform always reflects the lastest version of a Function 
as the UI does not support previous versions.

This means existing current platform data must be considered when modifying workflows.

Change may require one of the following actions:

- Testing existing Projects to ensure they still behave as expected.
- Testing existing Projects to ensure they are producing the same outputs as they were previously.
- Add/update Workflow Templates to reflect changes.
- Add/update UI behaviour to support new or changed fields.
- Migrate Project data in the Workflow API with a Django Migration.


### The API will migrate some Template changes to user Projects automatically ([data_migrate.py](https://gitlab.com/ecocommons-australia/ecocommons-platform/job-manager-api/-/blob/master/src/workflow_manager/data_migrate.py?ref_type=heads))
User projects do retain some history. While forms (and underlying schemas) change, the overall project structure is a snapshot of when the project
was created, this means modifying Templates will not generally have a direct affect on an existing Project. 

Some exceptions to this rule:
- Template schema URL modified are updated in existing Projects automatically.
- Template `outputs` changes are updated in existing Projects automatically.
- Adding new items to a Project will always show a selection based on the latest Template.

## Validation
As much as possible the deployment pipeline attempts to detect discrepancies between 
the Schema and Workflow Template and halt on problems.

The API will reject:
- Schemas that do not conform to the Draft-07 spec.
- Templates that do not conform to the [Template interface](https://swagger.dev.ecocommons.org.au/?urls.primaryName=Job+Manager#/workflow/api_template_create).
- Template refers to non existent schema URL.
- Template refers to non existent property within schema.
- Template refers to input that does not exist.
- Template refers to Job/Resultset that does not exist.


## Example change: adding a new required param to a Function
This action will potentially break all existing Projects if not done with care. 
Consider what required means given the context. Is it possible to nominate a default value, 
or derive it from other parameters?

#### Basic steps
- Add the new param to the Function schema JSON under `properties` and `required`. 
- Set a value as `default` or leave it blank if none can be predetermined.
- Set an appropriate value for all Workflow Template case study examples where required.
- Update the UI to ensure this field is ordered appropriately (required should generally be at the top).

Regardless of whether a default it set, existing Projects will not have this new value. 
There are two solutions.

1. Do nothing, a user can enter the new value as required when authoring an existing project. 
2. Use the API to migrate existing Projects and add the default or derived value.

