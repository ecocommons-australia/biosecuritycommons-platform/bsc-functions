# Biosecurity Commons Workflows 

# Workflow Templates General Introduction

The Workflow Template API provides modular template driven workflows that are defined 
in JSON and deployed to the platform.  The source is stored in `src/workflows/`.  

## Workflow API
<img src="docs/resources/import-flow.png"  width="600">

See [job-manager-api/docs/workflows.md](https://gitlab.com/ecocommons-australia/ecocommons-platform/job-manager-api/-/blob/master/docs/workflow.md?ref_type=heads)


# Workflow Implementations

These descriptions relate specifically to the Workflow Template API implementation.

For general information about these workflows see the 
[Biosecurity Commons Support site](https://support.biosecuritycommons.org.au/support/home).


## Biosecurity Risk Mapping [bsrmap](https://github.com/cebra-analytics/bsrmap/tree/main)

Risk Mapping is a non linear workflow with a complex Template 
containing many different Functions, Datasets and preset variants. 
Elements in a Risk Mapping project execute as independent Functions and 
produces descrete outputs that are chained together.

- Complex Template with many param presets and data.
- Many Functions with relatively simple flat schemas.
- Single Parameter Component representing the global 'study region'.
- Function inputs and outputs are mostly spatial rasters.
- Initial project provides basic structure with key steps.
- Requires an SDM Result as input

## Biosecurity Dispersal Modelling [bsspread](https://github.com/cebra-analytics/bsspread/tree/main)

Dispersal modelling is a linear workflow that executes as a single Function. 

Unlike Risk Mapping which is made of many independent Functions with simple schemas, 
dispersal modelling is described by a large, singular schema with many branching rules. 
Within a Project, the schema heirachy is broken down into different Components to reflect
different stages of the process, and different branches available to the user. 

- Moderately simple Template with one Function and many Parameter components.
- Complex schema with many sub schemas and branches.

## Biosecurity Surveillance Design [bsdesign](https://github.com/cebra-analytics/bsdesign/tree/main)

Surveillance Design is a linear workflow that executes as a single Function. 
It has a large complex schema with many branching rules.

- Moderately simple Template with one Function and many Parameter components.
- Complex schema with many sub schemas and branches.

## Biosecurity Proof of Freedom [bsdesign_pof](https://github.com/cebra-analytics/bsdesign/tree/main)

Proof of Freedom is a linear workflow that executes as a single Function. 
It has a simple schema with minimal branches, and a limited number of parameters and outputs.

## Biosecurity Impact Analysis [bsimpact](https://github.com/cebra-analytics/bsimpact/tree/main)

Impact Analysis is a linear workflow that executes as a single Function. 
It has a moderately complex schema with some branching rules.

## Biosecurity Resource Allocation [bsmanage](https://github.com/cebra-analytics/bsmanage/tree/main)

Resource Allocation is broken down into two linear workflows that execute as a single Function each. 
It heavily reuses elements from other workflows and shares schemas and backend integrations with 
bsspread, bsimpact and to an extent bsdesign.

- Dependency on bsspread, bsimpact. Shares schemas and UI and R integrations. 
- Implemented as a single Template, with two sub Template variants.

## Toolkit
Standalone implementation of `.toolkit`.

Flat selection of standalone Functions that offer various ad-hoc data transformations.

All Workflows can be augmented with ad-hoc toolbox Functions to provide additional data transformations.


# Managing change
[change.md](./change.md)

# Exemplar Templates

## Results
The following applies to the authoring of curated case study or demo Template only. 

Regular Templates should generally not contain **Result** types, instead any required input data 
should be imported into the platform as a publically available Dataset. There may be special cases however.

Results are not computed every time a user creates a new Project from an exemplar.  
Instead they are created during the Template publishing process.

### Results import checklist 
- Develop and fully deploy the Workflow Template structure.
- Create a Project using the template and ensure it is working correctly and has the expected parameters.
- Run each Function manually within the Project to ensure Results can be created and verify they produce expected outputs.
- Identify (by their `name` property) which Functions in the Project need to be executed. Often this will only be one top level function, but some workflows will comprise of many Functions.
- In the Workflow Template, add a new `job_request_uuid` to the identified functions (create with `uuidgen` from shell).

```json
"components": {
    "some_function" : {
        "job_request_uuid": "768133c8-1ff4-4505-9915-4dd84a2ec781"
    }
}
```

This will automatically trigger the creation of a new Job Request.

---
Note, often a component will already have `job_uuid`. This can be renamed `job_request_uuid` to make it execute.
---

### Notes/Issues
- Pre-populated Workflow Result data is a complex procedure in it's infancy. Templates generated by the UI arent perfect. Things WILL go wrong.
- Functions that depend on nested Results will fail to execute until their dependencies are available.
- The pipeline supports the env `IMPORT_OVERWRITE=true` which is useful for debugging but should NOT be used in Production. 
- If a specific Result needs to be re-created, delete it manually and then run the pipeline as missing Results will be re-created.


## .toolkit
Generic functions available to other templates by using `.toolkit` as parent. Additional configuration may be required.

Functions that require the 'template' param need a workflow specific input mapping override in the target template eg,

```json
    "toolkit_combine_layers": {
        "inputs": [
            "template:/study_region.region_rast"
        ]
    }
```
