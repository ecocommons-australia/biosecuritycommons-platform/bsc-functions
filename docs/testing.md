# Biosecurity Commons Functions - Testing

## Automated pipeline tests

Automated workflow tests are defined that execute in Gitlab when merging code. 
These must pass for deployment to occur.

## OLD method
Tests execute Nextflow pipelines in plain Gitlab cloud runners with a maximum of 2 cpus, so must be fairly lean.

## NEW method
Tests run directly in the platform K8 environment via the Exec API.

### How it works

- Instead of running a Nextflow pipeline directly using Docker in Docker, the Gitlab runner invokes 
a platform API to create a new Job in the Test execution cluster.
- The output comparison tests have been integrated directly into the Nextflow pipeline 
and will conditionally activate on the presence of the `_tests` parameter.

Note, This does not use the deployed API (via https), 
but rather runs an instance of the API in the pipeline itself with a local K8 config.  
This works via a command line interface able to run and monitor a pipeline in the foreground until it completes.

### Example `_tests` param

```json
{
  "_tests": [{
    "label": "rangebagging case_1",
    "type": "image",
    "a": "eval/Proj_current_Hieracium.pilosella..occurrences._rangebag.tif",
    "b": "tests/sdm/output/Proj_current_Hieracium.pilosella..occurrences._rangebag_case_1.tif",
    "arg": { "SSIM_MIN": 0.9, "MSE_MAX": 0.1 }
  }]
}
```

The tests are defined at [.gitlab-ci-tests.yml](./.gitlab-ci-tests.yml).

## Test data

Most test data is stored in the seperate [BSC-test](https://gitlab.com/ecocommons-australia/biosecuritycommons-platform/bsc-test) repository.  
This helps keep this repository lean and performant as it is used directly in platform operations.  
Comparison data used to validate outputs is currently still retained in this repository however.

All data and paramerters are in the directory `tests/`. 


Specifying test data within JSON parameter files can be done in two ways: 

- Fetch directly from the `BSC-test` repo using the `external` interface.
- Specify a UUID of a publicly available Dataset or Result on the BSC 'Dev' platform.

Example:

```json
{
    "spatial": {
        "url": "https://gitlab.com/api/v4/projects/ecocommons-australia%2fbiosecuritycommons-platform%2fbsc-test/repository/files/data%2fbsrmap%2finput%2fala_occurrences_parthenium.csv/raw",
        "source_type": "external"
    },
    "template": {
      "uuid": "9ccad5ab-d8fd-538f-b052-a7d1453c8ec8",
      "layers": [
        "template_au"
      ],
      "source_type": "dataset"
    },
    "alpha": 100000,
    "buffer": 1,
    "buffer_unit": "km",
    "hull": "alpha"
}
```



## Authoring tests

Test data should be compact and generally of a coarser resolution than actual usage.

Function tests should at a minimum, 

- Run a Nextflow pipeline with a representative set of parameters.
- Validate key outputs against reference artefacts (currently GeoTIFF, CSV).



