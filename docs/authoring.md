# Workflow Functions - Authoring

## Workflow development guide

Developing a platform integrated Workflow involves conceptually dividing work into 
what the platform calls a Function.

A Function is defined by:
- A JSONSchema manifest that describes all input parameters and possible output artefacts.
- A Nextflow pipeline able to consume JSON parameters as input.

A Function can:
- Implement a single operation that is executed either by itself, or as part of a dynamic chain of other Functions.
- Implement a series of operations within a single pre-determined pipeline.

A Nextflow pipeline can contain any number of processes of any type, however to effectively 
integrate with the platform there are two key processes that should always be included:
- **prepareInputs** - Reads the JSON parameters and fetches platform data.
- **uploadResults** - Collates the outputs based on the schema definition and uploads to permenant storage.

A pipeline can additionally:
- Read data directly from the Git repository it is located.

For more information on the platform integration that runs within Nextflow 
see https://gitlab.com/ecocommons-australia/lib/django-func-utils.


### Scoping a workflow

Deciding how to stucture a workflow depends on several factors, but should be considered primarily 
in terms of how the user is expected to interact with it.

Some possible questions to ask:

- Does the workflow involve many branching decision points by the user during the composition process, 
or is it composed as a single linear set of steps?

- Does the user need to see incremental outputs during the process of composing the workflow? 
(user decisions based on intermediate outputs may mean the Workflow needs to be broken down into multiple Functions).


### Perform an analysis on the overall workflow objectives and available scientific code implementations.

Implementing scientific code as a workflow requires the appropriate input parameters 
and their type to be determined, as well as identifying the expected outputs.

A data dictionary should be drafted and reviewed early in the implementation discussion. 
An ad-hoc document or napkin diagram can be used intially but the Function schema 
should be developed early as the source of truth.

- Draft Function schemas (see below)
  - Determine initial input parameters (eg. spatial_dataset, numeric params, choice enums etc)
  - Determine initial output artifacts (eg. spatial_output.tif, report.csv)

- Draft R integration following a parameter structure that conforms to the schema, use placeholders initially if required.
- Develop Dockerfile that includes any in house scientific packages.
- Develop 'base' Dockerfile with other 3rd party dependencies.


## Function 'Toolkit' (`src/content/toolkit/`)

The Function toolkit contains R integration scripts and a platform 'parameter config' file.  
The structure of this directory follows a predefined convention.

### Example

pipeline.nf
```
function = 'helloworld'
```

toolkit
```shell
src/content/toolkit/helloworld/helloworld.txt
src/content/toolkit/helloworld/helloworld.R
```

hellworld.txt (param config)
```json
"script": [
    "{function}/{function}.R"
]
```

### Namespaced example

pipeline.nf
```
function = 'example/helloworld'
```

toolkit
```shell
src/content/toolkit/example/helloworld/helloworld.txt
src/content/toolkit/example/helloworld/helloworld.R
```
hellworld.txt (param config)
```json
"script": [
    "{function}/{function_basename}.R"
]
```

### R integration scripts

The platform R integration sole responsiblity is to map the Job parameters JSON file to the scientific functions and execute them. This includes reading raw data files and mapping values to function arguments.  

Should:

- Perform any parameter transformations required to translate the schema structure to the scientific package inputs.
- Execute scientific code and save any data outputs following the naming conventions specified in the Schema under `outputs`.
- Implement presence checking for parameter values and sanity checks as required.
- Load raw image and csv files into R spatial libs, eg. Terra

Should not:

- Contain the actual algorithms. This code should be packaged and published outside of the function integration.
- Care about platform stuff like data UUIDs or where data comes from (keep R 'dumb' to the platform).
- Do data fetches, eg pulling anything from the platform API's or ObjectStore (Everything should be provided for R during the pipeline 'prepare' task).
- Reimplement all parameter validation in the schema. It should assume params have been validated and are compliant.

### R integration 'parameter config' file
TODO

## Function schemas (`src/schemas/`)

The Function schema is the essential source of truth for how a function behaves.  All Function inputs should be validated against it's schema, but it can also be used to generate a frontend form and other metadata views.

- Schemas inputs are defined using the JSONSchema specification https://json-schema.org/understanding-json-schema/about.html#about.
- Schemas should comply with version `draft-07` of the spec.

## Schema structure
```
{
	title,
	description,
	...
	inputs: {
		# JSONSchema describing all input parameters
		properties: {
			...
		}
	},
	outputs: {
		# Custom DSL describing all output artifacts
		"output.tif": {
			...
		}
	}
}
```

## Schema authoring quick start

- Bookmark https://rjsf-team.github.io/react-jsonschema-form/
- Bookmark https://json-schema.org/understanding-json-schema/


The Schema **inputs** should represent all possible input parameters for a function.

Schemas can be a simple set of flat parameters, or a complex heirachy of and objects and conditionals.

## Schema conditional rules (branching)
JSON schema supports conditional validation using a combination of clauses such as `oneOf|anyOf|allOf` and `if/then/else`. 
These can be used to drive powerful schema behaviours, for both validation and frontend presentation.  They also add a degree of complexity to schema evaluation.

### General advice on branching and conditionals:

- Use `if/then/else` over `dependencies` where possible. It is more powerful and has less edge cases.
- It is highly recommened to implement complex object properties under `definitions` or `$defs` and then use `$ref` to point to them in branching rules. This has several advantages,
  - Single source of truth, given props can be defined in multiple branch rules.
  - Much easier to read the branching logic.
  - Helps the API introspect schema (it has more limited schema resolution powers than the frontend). 

Example using `$ref`: 

```json
{
	"allOf": [
		{ 
			"properties": {
				"some_prop": { "$ref": "#/definitions/some_prop" }
			}
		}
	],
	"definitions": { "some_prop": { "type": "string" } }
}
```

It is recommended to author schemas reasonably flat where possible, and to break out complexity into seperate files which can also be included using the `$ref` keyword.

For more information these topics see:

https://json-schema.org/understanding-json-schema/structuring

https://json-schema.org/understanding-json-schema/reference/conditionals

## Schema `required` property

The schema `required` directive determines whether validation will pass based on the presence of a property listed in the `required` array.
This is an essential part of schema validation but there are extra considerations when a property of type `object` is required.

An object listed under `required` will be considered valid if it exists, but that can just be `{}`. The actual properties of the object must be validated.

This is where it gets complicated. Setting `required` within the object will solve this, but it also means those properties will be expected always. Even when the parent object itself is optional.  This can be solved with the use of `dependencies`

### React Json Schema Form (RJSF) issues
- Dont use single value array types eg. `"type": ["string"]`. Just use `"type": "string"`.


## Workflow Templates
Workflow Templates are required when using the **Workflow API**.

TODO more authoring notes..

- Declare 'Dataset' components at the top for clarity.
- Try keep it simple. If the template contains too many 'path mapped' or other exotic configurations, you may be on the wrong track.
- When specifying schema url's for Function or Parameter types, try avoid really complex pathing. Generally it's best to seperate complex workflows into multiple, flat(er) schemas, rather than referencing one single mega schema.
- TemplateComponent `name` will ideally remain stable over the life of a definition. It should be considered non unique identifer, in that components of the same name can exist more than once in a project. Generally they are not relied on as part of the implementation, but there are some edge cases.
  - When migrating a definition to an alternative schema url, `TemplateComponent.name` is used to identify and update `ProjectComponent` records.
  - `TemplateComponent.name` has some very limited behaviour implemented in the UI around presentation, generally this should be avoided.



