# Workflow Functions - Dev and debugging

## Prereading
This guide assumes basic familarity with Docker, Docker Compose, command line (bash) and Nextflow.

It is written primarily for MacOS and Linux but should be reproducable using Windows WSL.

## Running a pipeline and R scripts locally

Local development has the following minimum requirements:
- A local copy of this repository
- Nextflow 
- Docker
- Environment file with platform information (`.env`)
- A user account on the Biosecurity Commons platform (to access platform data)

### Create a local copy of this repository

```shell
# Download repository
git clone https://gitlab.com/ecocommons-australia/biosecuritycommons-platform/bsc-functions.git

# Navigate to downloaded repository
cd bsc-functions
```

### Ensure Python is up to date
The default Python installed on a Mac is usually old.  This process requires at least Python `3.10`.

The easiest way to get a newer Python on Mac is to use [brew](https://brew.sh/).

```shell
brew install python3
```

### Install Nextflow

Nextflow requires an updated version of Java.
Java installed on a Mac by default is probably not going to work. 
The best way to install new versions of Java is with [SdkMan](https://sdkman.io/install).

#### Install Java 17.0.11 (Temurin) with SdkMan

```shell

sdk install java 17.0.11-tem
# Open a new shell for this to take effect
```

```shell
# Nextflow downloads to the folder the install script is run from.
curl -s https://get.nextflow.io | bash
# Copy nextflow to a $PATH location to make it a generally available command.
cp ./nextflow /usr/local/bin
```

### Environment and Authorisation
Executing pipelines involves talking to the platform. As such credentials are required.

An env file `.env.example` configured for our Dev environment is available (see [here](.env.example)).

To activate it run create a copy and rename as `.env`. This can be done using the following shell command:

```shell
# This will copy .env.example and save it as .env
cp .env.example .env
```

It is important to note that this file does not contain any actual credentials. 
You will be prompted for these details after running:

#### Login to the platform

```shell
./bin/login.sh
```  

This will open a browser window with the standard login.

When successfully logged in, a file `.env.auth` will be created containing an access token. 
This will automatically be used when running a pipeline via the below commands (until it expires).

### Build docker containers

Images must first be built with the command.

```shell
docker compose build
```

Note, there are several images. You may only want to build some of them for example:

```shell
docker compose build bsc bsc-utils
```

### Run the pipeline
The script `./bin/run_pipeline.sh` is designed to run a pipeline using locally built docker images. 

The exact script arguments are provided by running with no arguments.

A pipeline can be run locally to reproduce execution similar to how it occurs in the platform.

`run_pipeline.sh`  will run any pipeline in the repo eg. bsspread.nf’ with any param.json file.  This will perform all the steps the platform would in terms of pulling the data based on UUIDs, setting up the folder structure all locally.  It will execute all steps except for the final `upload_result` step which may fail.

```shell
./bin/run_pipeline.sh ./combine_layers.nf ./tests/bsrmap/combine_layers.params.json
```

Or alternatively:

```shell
./bin/run_job.sh ./combine_layers.nf JOB_UUID
```

Artefacts created by the Nextflow job:
```
./nxf_work/input/
./nxf_work/output/
./nxf_work/scripts/
```

### Debuging R scripts locally with platform data.

`run_rscript.sh` is a short cut to ‘re run’ just the R script.  This only works if `run_pipeline.sh` has been executed at least once before hand. It will run the R script in the nextflow work dir `nxf_work/script/script.R`.

The integration script can be tweaked and rerun.  Just be sure to copy any changes made in the workdir script back to the original source.

```shell
./bin/run_rscript.sh SCRIPT_NAME_WITHOUT_PATH
```

This allows for basic debugging but deeper introspection and dev should be done using the integrated RStudio environment.

### Debuging prepare_inputs only

`run_prepare_inputs.sh` executes the prepare_inputs.py script directly and can be used for debugging this process. 

```shell
./bin/run_prepare_inputs.sh FUNC_NAME PARAMS (container relative) [any additional args for prepare_inputs.py]
```

## RStudio integration 

`Dockerfile-rstudio-sdm` (for SDM functions) or `Dockerfile-rstudio` (for all other BSC functions) can be optionally built for local R development directly integrated with a RStudio web IDE.

Requires the following dependency to be unzipped and placed in the repo root under `rocker_scripts/`

https://griffitheduau.sharepoint.com/sites/EcoCommonsAustralia-ImplementationTeam/Shared%20Documents/Forms/AllItems.aspx?id=%2Fsites%2FEcoCommonsAustralia%2DImplementationTeam%2FShared%20Documents%2FImplementation%20Team%2FEcoCommons%202020%2D2023%2FACTIVITY%20STREAMS%2FStream%203%20%2D%20Scientific%20and%20Data%20Workflows%2FPlatform%20Testing&viewid=50feca04%2Dca32%2D4c28%2Db55b%2D3a47aed2f3e8


### Setup example

#### Build the RStudio Dockerfile

```bash
docker build -f Dockerfile-rstudio -t bsc_rstudio .
```
Or alternatively for SDM functions:
```bash
docker build -f Dockerfile-rstudio-sdm -t bsc_rstudio_sdm .
```

#### Launch the container (example paths, exact local setup will vary)
This mounts a pipeline work dir previously created with `run_pipeline.sh` for interactive debugging.

```bash
# Start
docker run --rm -d --name rstudio -v ~/dev/:/home/rstudio/ -v ~/dev/bsc-functions/nxf_work/:/workdir -p 127.0.0.1:8787:8787 -e DISABLE_AUTH=true bsc_rstudio

# Stop
docker stop rstudio
```
Or alternatively for SDM functions:
```bash
# Start
docker run --rm -d --name rstudio_sdm -v ~/dev/:/home/rstudio/ -v ~/dev/bsc-functions/nxf_work/:/workdir -p 127.0.0.1:8787:8787 -e DISABLE_AUTH=true bsc_rstudio_sdm

# Stop
docker stop rstudio_sdm
```

#### Open the RStudio web IDE 

http://127.0.0.1:8787/


### Debug a pipeline

This assumes `./bin/run_job.sh` or `./bin/run_pipeline.sh` has been run at least once and `./nxf_work` contains the working files.

- Open the RStudio web IDE.
- In *console* run `setwd('/workdir/script')`.
- Under *Files* navigate to `/workdir/script/` and open the integration R script.
- Select part or all of the script and run with Ctrl+Enter.

## Docker Compose

Docker Compose helps build images and a few supporting tasks. It is not part of Nextflow.
 
Check docker-compose.yml and docker-compose.override.example.yml for build and run options.

