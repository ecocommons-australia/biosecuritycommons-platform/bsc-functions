# Biosecurity Commons Functions - Deployment

Commits trigger pipelined deployment following branch/environment 
relationships that using the [standard pattern defined for the platform](/ecocommons-australia/operational-notes-and-scripts/ci-pipelines).

## Deployment Overview 

<img src="docs/resources/docker-images.png"  width="700">

## Deployment Procedure
Docker images are built via the Gitlab pipeline.  
The protected branches `develop` and `master` build with predefined image tags 
and are automatically synced with the Dev and Test execution clusters.

Production releases are triggered by creating a semver tag in Gitlab.

**Deployment actual** occurs when the `import template` stage of a tagged pipeline executes successfully 
as this is what tells the platform what Git tag to use when running a Nextflow pipeline.

### Rollback
The platform API's always make the most recently published schema available to the user.  
If a release needs to be rolled back, deleting the tag is not sufficient. 
The simplest way to roll back is to create a new tag from a known working ref (commit, tag etc).

### Tagging
BSC Functions uses the following Docker tag convention for deployment to specific environments.

Git ref | Tag        | Target environment
--------|------------|---------------------
develop | latest-dev | Development (dev.*.org.au)
master  | latest     | Test (test.*.org.au)
1.0.0   | 1.0.0      | Production (app.*.org.au)

## Nextflow profiles
The repository `nextflow.config` file is configured with two *profiles* for different execution scenarios. 

Profile     | Tag              | Target environment
------------|------------------|---------------------
versioned  	| $JOB_VERSION     | Dev, Test, Prod
standard   	| 1.0.0      	   | Prod, ad-hoc pipeline execution

Note, `latest`, `master` or similar should NEVER be used as the **'standard'** tag 
as they are an unversioned and transient resource.


## Production Deployment
Dev and Test and Production environments use the **'versioned'** profile, 
which expects the NF executor to provide the envvar `JOB_VERSION` with a value matching 
a specific Git reference and image tag. However the **'standard'** profile should still be 
kept up to date with the latest tag versions.  This is to ensure consistency when executing 
current and historic pipelines via methods other than the platform.


### Pipeline image prefetch
The pipelines prefech Docker images into the cluster environments to improve Nextflow performance.

### Building the 'base image'
The Docker builds are stratified into two levels to seperate high turn over repository changes from underlying dependencies, 
which can take a considerable amount of time to build.  

The base image is tagged using the var `{ENV}_ENV_BASEIMG_TAG` defined in `.gitlab-ci`.  
For Production deployments this should be incremented before build, using a semver pattern.

The base image build can be triggered by using the `#buildbase` 
commit message hint or the pipeline variable `BUILD_BASE=1`.

### Dockerfile
Name               | Trigger                          | Purpose
-------------------|----------------------------------|---------------------
Dockerfile 		   | auto (branch dependent)          | Function integration code, frequently changing.
Dockerfile-base    | manual (#buildbase commit hint)  | Base dependencies, infrequently changing.
Dockerfile-utils   | auto (branch dependent)          | Function support code, frequently changing.
Dockerfile-rstudio | manual (local dev only)          | Function development support.

## Artifacts that deploy from this repo
- Function Docker images
- Function Schemas
- Function Workflow Templates
- Test data, Template examplar data.

## Function integration and Docker images
- R package dependencies defined in Dockerfile(s)
- R integration scripts defined in `src/toolkit/*/*.R`

## Commit message pipeline behaviour hints
Hint          | Action     
---           | ---                 
#buildbase    | Build the Base image (R dependencies, takes a long time)
#notest       | Skip tests pipeline job  
#nodata       | Skip import data pipeline job  
#noschema     | Skip import schemas pipeline job  
#notemplate   | Skip import templates pipeline job  
#data         | Always import data pipeline job  
#schema       | Always import schemas pipeline job  
#template     | Always import templates pipeline job  

## Platform imported data (Functions and Workflow Templates)
On commit this repository will import **Function** and **Workflow Template** definitions into the platform.  
The pipeline will fail if this data does not meet integrity checks. 

### Function Schemas
All **Functions** implement a schema which provides the source of truth of the Function interface.

Schemas are located at `src/schemas/` and are currently included as part of Docker image builds, 
as well as imported into the platform 'Function Manager' via pipeline.

---
**Current limitations of schema implementation**

bsdesign and bsspread Functions are broken down into multiple schemas to ease maintainability and template authoring.  
However the pipeline execution is not yet adapted to traverse schema `$ref`.  
This means it must use a seperate 'resolved' schema which must be manually placed in `src/schemas`.

Keeping this schema up to date is important but not critical for new deployments. 
It aids human readable metadata within the Function execution payload, 
but is not yet a part of validation and older versions often produce similar results.

It can be generated via the Function Manager API
```
https://api.job-manager.dev.ecocommons.org.au/workflow/schema?format=json&id=https%3A%2F%2Fbiosecuritycommons.org.au%2Fbsdesign%2Fbsdesign.json&resolve_refs=1
```
```
https://api.job-manager.dev.ecocommons.org.au/workflow/schema?format=json&id=https%3A%2F%2Fbiosecuritycommons.org.au%2Fbsspread%2Fbsspread.json&resolve_refs=1
```
---

#### Schema development resources
- https://json-schema.org/learn/
- https://rjsf-team.github.io/react-jsonschema-form/
- https://rjsf-team.github.io/react-jsonschema-form/docs/

### Workflow Templates
The Workflow API provides modular template driven workflows that are defined in json.  
The source is stored in `src/workflows/`.