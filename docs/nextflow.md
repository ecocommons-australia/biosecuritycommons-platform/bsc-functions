# Biosecurity Commons (BSC) Functions - Nextflow pipelines

## Nextflow
The BSC workflows are implemented as [Nextflow computational pipelines](https://www.nextflow.io/).

This provides a way to divide the workflow execution into individual containerized 
processes each with their own set of dependencies and resource allocations. 
Nextflow orchestrates the data flow between processes and provides an 
abstraction for provisioning the underlying compute resources.

A basic workflow consists of 3 processes that follow a Fetch-Run-Store pattern. 
This handles the staging of input data, model execution and storing of outputs.
The Fetch and Store processes represent specific platform actions, 
with the Run process containing the model executor itself.

Nextflow Process              | Description
---                           | ---
prepareInputs                 | Python util to fetch platform or external data and stage the execution workspace.
runFunction                   | Python wrapper to execute an R payload and collate output metadata.
uploadResult                  | Upload output artefacts to platform and object store backends.

More complex workflows implement additional processes as required however to effectively integrate with the 
platform all workflows should implement *prepareInputs* and *uploadResult* at least once.

### Nextflow DSL2
From tag `1.9.0` the pipelines use Nextflow DSL v2. This has been supported by Nextflow for some time, 
however new versions have dropped support for DSL v1.  
Nextflow will need to be downgraded to `22.10.0` when running older pipeline revisions.

```bash
NXF_VER=22.10.0 nextflow run ann.nf ...
```

### Nextflow pipeline execution
Nextflow pipelines are the entrypoint for BSC workflows. 
Pipelines are designed to execute in the platform Work Execution Cluster, which provides an API for submitting Jobs.

Pipelines can also be tested using Nextflow locally by providing platform user credentials. 
See [README.localdev.md](./README.localdev.md). 

### Resources
- https://seqera.io/blog/optimizing-nextflow-for-hpc-and-cloud-at-scale/

#### Data parameters
Each pipeline accepts a JSON parameter file which contains everything required to execute the workflow. 
This includes basic scalar values, structured data, eg, geojson and pointers to external resources 
such as GeoTIFF's which the pipeline will fetch. Examples can be found in the `tests` dir.

The parameters must validate against a [JSONSchema](https://json-schema.org/) [provided for each pipeline](./src/schemas). 
Validation does not occur as part of pipeline execution, there is an expectation parameters *have been validated* 
against a given schema when the pipeline executes. This is typically done via the platform frontend, 
but other methods are in development.

The schema source is stored in this repository and also published to the platform via an API.

Platform data defined in workflow input parameters generally follows the 
'[Dataset Layers](./src/schemas/generic/dataset.json)' interface. 
This describes a platform convention for specifying data originally based around single or 
multi layered geospatial data, however it can be used to specify any type of platform or external data.

An example Dataset Layers minimal JSONSchema:

```json
"dataset_layers": {
    "title": "Dataset layers",
    "description": "Dataset spatial layers interface",
    "type": "object",
    "properties": {
        "uuid": {
            "title": "Dataset",
            "description": "UUID for platform data (dataset|resultset)",
            "type": "string",
            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
        },
        "url": {
            "title": "External URL (external)",
            "description": "External URL",
            "type": "string"
        },
        "layers": {
            "description": "List of layer names for multi layered data",
            "type": "array",
            "items": {
                "description": "Layer name",
                "type": ["string"]
            },
            "minItems": 0,
            "uniqueItems": true
        },
        "source_type": {
            "description": "Type of input source",
            "type": "string",
            "enum": [
                "dataset",
                "resultset",
                "external",
            ],
            "default": "dataset"
        }
    },
    "dependencies": {
        "uuid": [
          "layers",
          "source_type"
        ],
    }
}
 ```

#### Platform data example

 ```json
 {
 	"some_spatial_data": {
 		"source_type": "dataset",
 		"uuid": "acde070d-8c4c-4f0d-9d8a-162843c10333",
 		"layers": [ "the_first_layer" ]
 	}
 }
 ```

#### External data example

 ```json
 {
 	"some_spatial_data": {
 		"source_type": "external",
 		"url": "http://data-place/interesting-data.tif",
 	}
 }
 ```
