#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function    = 'sdm/climatch'
params.functions   = null
params.use_coord_cleaner = false
params.upload      = true

include { sdm } from './sdm'

workflow {
    if (params.functions instanceof List){
        createParamsJson(
            channel.fromList(params.functions)
        ) | sdm

    } else if (params.inputs && params.function && params.job_uuid){
        sdm(
            params.function,
            params.job_uuid,
            channel.fromPath(params.inputs)
        )

    } else {
        error "Cannot determine exec strategy, params should contain either 'functions:List' or ('inputs:string', 'function:string', 'job_uuid:string')"
    }
}