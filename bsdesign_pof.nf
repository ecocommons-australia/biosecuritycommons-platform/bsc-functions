#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsdesign_pof"

include { _main } from './main'

workflow {
    _main()
}