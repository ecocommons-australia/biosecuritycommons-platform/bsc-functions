#!/usr/bin/env nextflow

import java.time.*;
import java.security.MessageDigest

def maxConcurrent(gen, tasks) { 
    if (gen == 1 && tasks > 20)
        Math.ceil(tasks / 2)
    else
        tasks
}

workflow RTMGen {
    take:
    gen
    data
    job_uuid
    params_json
    n_particles
    n_tasks
    time_limit_min

    main:
    time_limit = LocalDateTime.now().plusMinutes(time_limit_min)
    concurrency = Channel.of(0..(maxConcurrent(gen, n_tasks)-1))
    particles = Channel.topic("particles_${gen}")
    particle_output = Channel.topic("particle_output_${gen}")

    particleStopCond = { it ->
        println("[Gen ${gen}] Generated ${it} of ${n_particles} particles")
        if (LocalDateTime.now() > time_limit)
            error("Generation ${gen} exceeds time limit (${time_limit_min}min)")
        it >= n_particles
    }

    concurrency
        .collect()
        .view { "[Gen ${gen}] Task concurrency of ${it.size()}" }

    // This is evaluated every time a task emits a value to the particle topic channel.
    // It compares the total particles for all completed tasks
    // and if less than the desired (num_particles) a new channel (limiter) 
    // is derived that triggers additional tasks
    particles
        .map { p -> p instanceof List ? p.size() : 1 }  // Map each output to a count for number of particles
        .scan { acc, size -> acc + size }               // Combine this map to get the total count
        .until(particleStopCond)                        // Stop if num_particles is reached
        .mix(concurrency)                               // Otherwise "Seed" the channel to create a number of concurrent tasks
        .set { limiter }

    "RTMStepx${gen}"(job_uuid, gen, limiter.combine(data.toList()))

    particles
        .collect()
        .view { "[Gen ${gen}] Total particles generated ${it.size()}" }
        .map { it.take(n_particles) }
        .view { "[Gen ${gen}] Taking ${it.size()} particles" }

    RTMCollate(job_uuid, gen, data,
        particles.collect().map { it.take(n_particles) }
    )

    RTMCollate.out.concat(data).collect() | RTMPostABC

    all_output = RTMPostABC.out.concat(
        particle_output.collectFile(skip: 1, keepHeader: true),
    )

    if (params.upload)
        uploadResult(
            0,
            job_uuid,
            params_json,
            all_output.collect()
        )

    emit:
    RTMCollate.out.collect()
}

process prepareInputs {
    input:
    val function
    val job_uuid
    path params_json

    output:
    path 'input_params.json',   emit: params_json
    path 'input/*',             emit: inputdir

    """
    python /srv/app/manage.py runscript prepare_inputs --script-args \
        function=$function params=$params_json jobuuid=$job_uuid \
        schemadir=/srv/app/schemas/ workdir=.
    """
}

/**
* Publish pipeline outputs to object store, and metadata to the platform db.
*/
process uploadResult {
    tag "$job_uuid"

    input:
    val status
    val job_uuid
    path params_json
    path 'output/*'

    """
    python /srv/app/manage.py runscript tag_results --script-args \
        params=$params_json status=$status

    python /srv/app/manage.py runscript upload --script-args \
        uuid=$job_uuid result=output/ jobinfo=output/jobinfo.json
    """
}

process RTMInit {
    memory 2.GB

    input:
    path('In/*')
    val(clus)
    val(t_today)
    val(t_max)
    val(n_generations)

    output:
    path('Out/*')

    """
    #!/bin/bash
    cp /rtm/template/dyn_sim_rcpp.v15.cpp .
    mkdir -p Out

    export ABC_PARAMS_FILE="In/input_params.json"
    export ABC_INPUT_FILE="In/prepped_data.RData"
    export ABC_N_GEN=${n_generations}
    export ABC_CPUS=${params.cpus}
    export ABC_USE_GEN_FILE=0 

    time R --no-save --args $clus $t_today $t_max < /rtm/template/01_init.R
    """
}

process RTMStepx1 {
    tag "$job_uuid"
   
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_1", optional: true
    path('particle_output.csv'), topic: "particle_output_1", optional: true

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx2 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_2", optional: true
    path('particle_output.csv'), topic: "particle_output_2", optional: true

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx3 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_3", optional: true
    path('particle_output.csv'), topic: "particle_output_3", optional: true

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx4 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_4", optional: true
    path('particle_output.csv'), topic: "particle_output_4", optional: true

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx5 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_5"
    path('particle_output.csv'), topic: "particle_output_5"

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx6 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_6"
    path('particle_output.csv'), topic: "particle_output_6"

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx7 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_7"
    path('particle_output.csv'), topic: "particle_output_7"

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx8 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_8"
    path('particle_output.csv'), topic: "particle_output_8"

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx9 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_9"
    path('particle_output.csv'), topic: "particle_output_9"

    script:
    template 'rtm_stepx.sh'
}

process RTMStepx10 {
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    tuple val(taskId), path('In/*')

    output:
    path("Out/gen_${gen}*.RData"), topic: "particles_10"
    path('particle_output.csv'), topic: "particle_output_10"

    script:
    template 'rtm_stepx.sh'
}

process RTMCollate {
    stageInMode 'copy'
    memory 2.GB
    tag "$job_uuid"
    
    input: 
    val(job_uuid)
    val(gen)
    path('In/*')
    path("Out/gen_${gen}_task_0_particle_*_iter_0.RData", arity: '1..*')

    output:
    path("Out/smc.abc.g*.postclust.RData")

    """
    #!/bin/bash
    export GEN=${gen}
    cp -vrf ./In/* ./Out/
    time R --no-save < /rtm/template/03_collate.R
    """  
}

process RTMPreABC {
    memory 2.GB

    input:
    path(params_json)
    path('input/*')

    output:
    path('prepped_data.RData')

    """
    #!/bin/bash
    cp /rtm/functions_pre-abc.R ./
    cp /rtm/functions_post-abc.R ./
    time R --no-save --args $params_json < /rtm/pre-abc/dat_prep.R 
    """   
}

process RTMPostABC {
    memory 2.GB

    input:
    path('In??/')

    output:
    path('forecast/*')

    """
    #!/bin/bash
    cp /rtm/functions_post-abc.R ./
    export PROCESS_RDATA=In/fake_data_ABC.RData
    export ABC_OUTPUT_DIR=In/
    export FORECAST_DIR=forecast/
    export PARTICLE_MIN=5
    mkdir -p In && mv -vf In*/* In/
    time R --no-save < /rtm/post-abc/generate_forecast_outputs.R
    """   
}