#!/bin/bash
set +e

cp /rtm/template/dyn_sim_rcpp.v15.cpp .

TIMEOUT=\${TIMEOUT:-'5m'}

mkdir -p Out
cp -rf In/* Out/ 

echo "GEN=${gen} TASK=${taskId}"
export GEN=${gen}
export PARTICLE_OUT_PATH='particle_output.csv'
export PARTICLE_MAX_PER_TASK=10
export SLURM_ARRAY_TASK_ID=${taskId}
timeout --preserve-status \$TIMEOUT R --no-save < /rtm/template/02_stepx.R
status=\$?
echo "02_stepx.R exit status \${status}"

# Final status is determined by two factors.
# Whether the script completed and if any particles were produced.
# A non successful script status is ignored, this is due to edge cases where the script 
# can hard fail (having produced particles or not). We dont want these torpedoing the whole pipeline.  
# Conversely a successful execution with no particles likely indicates model param issues!
(compgen -G "Out/gen_*_task_${taskId}*" || [[ \$status > 0 ]]) && exit 0 || exit 244