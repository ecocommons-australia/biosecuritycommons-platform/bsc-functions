#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.label        = null
params.absWorkdir   = ''

process coordCleaner {
    container "registry.gitlab.com/ecocommons-australia/biosecuritycommons-platform/bsc-functions/coordcleaner:3.0.1-2"
    tag "$job_uuid"
    label params.label
    
    input:
    path params_json
    path inputdir,  stageAs: 'input/*'
    path scriptdir, stageAs: 'script/*'
    val job_uuid

    output:
    path 'output/*',                            emit: outputdir, optional: true
    path "output/${job_uuid}/jobinfo.json",     emit: jobinfo
    val  job_uuid

    """
    python /srv/app/manage.py runscript run_func --script-args \
        params=$params_json scriptdir=script cpus=${task.cpus} \
        tag_results=true zip_results=false

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        cp -vrf $params.absWorkdir/output output || true
    fi
    """
}