#!/usr/bin/env nextflow

params.upload = true
params._tests = null

include {
    prepareInputs;
    runFunction;
    uploadResult;
} from './process'

include { 
    prepareInputs as prepareInputsJobreq;
} from './process_jobreq'

include { 
    testResult;
} from './process_test'

include {
    runFunction as bsrmap_abiotic_suitability;
    runFunction as bsrmap_aggregate_categories;
    runFunction as bsrmap_arrival_likelihood;
    runFunction as bsrmap_biotic_suitability;
    runFunction as bsrmap_buffered_hull_layer;
    runFunction as bsrmap_combine_layers;
    runFunction as bsrmap_conform_layer;
    runFunction as bsrmap_distance_weight_layer;
    runFunction as bsrmap_distribute_features;
    runFunction as bsrmap_establishment_likelihood;
    runFunction as bsrmap_pathway_likelihood;
    runFunction as bsrmap_pest_suitability;

    runFunction as bsspread_bsspread;
    runFunction as bsdesign_bsdesign;
    runFunction as bsdesign_bsdesign_pof;
    runFunction as bsimpact_bsimpact;
    runFunction as bsmanage_bsmanage_design;
    runFunction as bsmanage_bsmanage_sim;
    runFunction as bsmanage_bsmanage_prev_control;
    
    runFunction as toolkit_create_region;
    runFunction as toolkit_reclass_layer;
    runFunction as toolkit_transform_layer;

    runFunction as bsspread;
    runFunction as bsdesign;
    runFunction as bsdesign_pof;
    runFunction as bsimpact;
    runFunction as bsmanage_design;
    runFunction as bsmanage_sim;
    runFunction as bsmanage_prev_control;
    runFunction as buffered_hull;
    runFunction as buffered_hull_layer;
    runFunction as create_region;
    runFunction as reclass_layer;
    runFunction as transform_layer;
} from './process'

include { 
    coordCleaner as toolkit_coord_cleaner;
} from './process_ccleaner'

/** 
 * Execute a standalone Job. 
 * This expects a param json file, function name and job uuid.
 */
workflow job {
    take: params_json
    take: function
    take: job_uuid
    main:
        process = function.replace('/', '_')
        inputdir = Channel.of('').collectFile('name': 'EMPTY')
        

        inputs = prepareInputs(
            function, 
            job_uuid, 
            params_json, 
            inputdir
        )

        out = "$process"(inputs)

        if (params._tests){
            testdir = Channel.fromPath("$projectDir/tests")
            testResult(
                testdir.combine(out.outputdir),
                params._tests,
                job_uuid
            )
        }
        else if (params.upload == true){
            uploadResult(out)
        }
}

/**
 * EXPERIMENTAL
 * Execute a JobRequestFunction. This behaves similar to the job workflow,
 * but is intended for multi job pipelines. It takes a JRF object and an input channel which 
 * accumulates all fetched or created data entities, and it emits the function output dir.
 */
workflow jobReqFunc {
    take: 
        jrf
        inputdir

    main:
        out = prepareInputsJobreq(jrf, inputdir) | runFunction

        if (params.upload == true){
            uploadResult(out)
        }

    emit:
        out.outputdir
}