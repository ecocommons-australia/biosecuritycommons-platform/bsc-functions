#!/usr/bin/env nextflow
nextflow.enable.dsl=2

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

params.label                = null
params.absWorkdir           = ''
params.upload               = false

/**
 * Write JobRequestFunction parameters as as JSON file 
 * so it can be consumed by the prepareInputs process.
 */
process createParamsJson {
    input:
    val jobReqFunc

    output:
    val jobReqFunc.function,        emit: function_name
    val jobReqFunc.uuid,            emit: uuid
    path 'params.json',             emit: params_json

    """
    echo '${JsonOutput.prettyPrint(JsonOutput.toJson(jobReqFunc.parameters))}' > params.json
    """
}

/**
 * Write a JSON file by extracting a key from an existing parameter file.
 */
process extractParamJson {
    memory '256 MB'
    cpus = 0.5

    input:
    val json
    val key

    output:
    path 'params.json', emit: params_json

    """
    echo '${
        js = new JsonSlurper(); 
        JsonOutput.prettyPrint(
            JsonOutput.toJson(js.parse(json)[key])
        )
    }' > params.json
    """
}

/**
* Fetch and pre-process platform and external data.
*/
process prepareInputs {
    // tag "$job_uuid"
    label 'auth'
    debug false

    input:
    val function
    val job_uuid
    path params_json
    path inputdir,  stageAs: 'input/*'

    output:
    path 'input_params.json',   emit: params
    path 'input/*',             emit: inputdir,  type: 'any', includeInputs: true
    path 'script/*',            emit: scriptdir, type: 'any'
    val job_uuid,               emit: uuid

    script:
    """
    python /srv/app/manage.py runscript prepare_inputs --script-args \
        function=$function params=$params_json jobuuid=$job_uuid \
        workdir=$params.absWorkdir schemadir=/srv/app/schemas/

        # Legacy behaviour, absolute path workdir.
        if [ -n "$params.absWorkdir" ]; then
            cp -vrf $params.absWorkdir/script script || true
        fi
    """
}

/**
* Wrapper for running R scripts.
*/
process runFunction {
    tag     "$job_uuid"
    label   params.label
    label   params.function.replace('/', '_')

    input:
    path params_json
    path inputdir,          stageAs: 'input/*'
    path scriptdir,         stageAs: 'script/*'
    val job_uuid
    
    output:
    path 'output/*',                            emit: outputdir
    path "output/${job_uuid}/jobinfo.json",     emit: jobinfo
    val  job_uuid

    script:
    """
    export TASK_CPUS=${task.cpus}
    # export DEBUG_DIR="$params.absWorkdir"

    python /srv/app/manage.py runscript run_func --script-args \
        params=$params_json scriptdir=script cpus=${task.cpus} \
        tag_results=true zip_results=false

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        mkdir -p $params.absWorkdir/output
        cp -vrf $params.absWorkdir/output output || true
    fi
    """

}

/**
* Collate pipeline outputs and generate 'jobinfo' metadata required
* for upload to the platform.
*/
process tagResults {
    tag "$job_uuid"
    
    input:
    val status
    val job_uuid
    path params_json
    path "output/*"
    
    output:
    path 'output/*',                emit: outputdir, includeInputs: true
    path "output/jobinfo.json",     emit: jobinfo
    val  job_uuid

    script:
    """
    python /srv/app/manage.py runscript tag_results --script-args \
        params=$params_json status=$status
    """
}

/**
* Publish pipeline outputs to object store, and metadata to the platform db.
*/
process uploadResult {
    tag "$job_uuid"
    label 'auth'

    input:
    path 'output/*'
    path jobinfo
    val job_uuid

    """
    python /srv/app/manage.py runscript upload --script-args \
        uuid=$job_uuid result=output/$job_uuid jobinfo=$jobinfo
    """
}
