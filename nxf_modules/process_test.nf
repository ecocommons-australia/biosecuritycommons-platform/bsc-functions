#!/usr/bin/env nextflow
nextflow.enable.dsl=2

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

process testResult {
    debug true
    tag "$job_uuid"
    container "registry.gitlab.com/ecocommons-australia/lib/django-func-utils/support:develop"
    
    input:
    tuple path('ref/*'), path('output/*')
    val tests_json
    val job_uuid

    """
    export A_BASEPATH=output/${job_uuid}/
    export B_BASEPATH=ref/
    export MSE_MAX=0.1
    export SSIM_MIN=0.9

    echo '${
        js = new JsonSlurper(); 
        JsonOutput.prettyPrint(
            JsonOutput.toJson(tests_json)
        )
    }' > tests.json
 
    python3 -m support.compare tests.json
    """
}
