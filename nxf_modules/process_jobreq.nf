#!/usr/bin/env nextflow
nextflow.enable.dsl=2

import groovy.json.JsonOutput

process prepareInputs {
    tag "$jobFunc.uuid"
    label 'auth'
    debug false
    
    input:
    val jobFunc
    path 'input/*'

    output:
    path 'input_params.json',   emit: params
    path 'input/*',             emit: inputdir,  type: 'any', includeInputs: true
    path 'script/*',            emit: scriptdir, type: 'any'
    val jobFunc.uuid,           emit: uuid

    script:
    """
    echo '${JsonOutput.prettyPrint(JsonOutput.toJson(jobFunc.parameters))}' > params.json
    
    python /srv/app/manage.py runscript prepare_inputs --script-args \
        function=$jobFunc.function params=params.json jobuuid=$jobFunc.uuid \
        schemadir=/srv/app/schemas/
    """
}