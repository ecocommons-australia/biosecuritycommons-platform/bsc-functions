#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.label         = null
params.absWorkdir   = ''

process SDM {
    tag "$job_uuid"
    label params.label

    input:
    path params_json
    path inputdir,  stageAs: 'input/*'
    path scriptdir, stageAs: 'script/*'
    val job_uuid
    
    output:
    path 'output/*',                            emit: outputdir
    path "output/${job_uuid}/jobinfo.json",     emit: jobinfo
    val  job_uuid

    """
    export TASK_CPUS=${task.cpus}
    
    # Ensure coordinates csv is in the input dir root.
    cp -v ./input/${job_uuid}/*.csv ./input/ 2>/dev/null || true 

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        cp ./input/${job_uuid}/*.csv $params.absWorkdir/input/ 2>/dev/null || true 
    fi

    # Copy forward any Csv, R scripts or logs (ccleaner output).
    mkdir -p ./output/${job_uuid}/
    cp -v ./input/${job_uuid}/*.R* ./output/${job_uuid}/ 2>/dev/null || true 
    cp -v ./input/${job_uuid}/*.csv ./output/${job_uuid}/ 2>/dev/null || true 

    python /srv/app/manage.py runscript run_func --script-args \
        params=$params_json scriptdir=script cpus=${task.cpus} \
        tag_results=true zip_results=false

    # Legacy behaviour, absolute path workdir.
    if [ -n "$params.absWorkdir" ]; then
        cp -vrf $params.absWorkdir/output/* output || true
    fi
    """
}