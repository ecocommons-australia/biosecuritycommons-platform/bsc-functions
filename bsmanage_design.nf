#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsmanage_design"

include { _main } from './main'

workflow {
    _main()
}