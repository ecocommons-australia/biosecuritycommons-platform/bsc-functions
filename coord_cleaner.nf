#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "toolkit/coord_cleaner"

include { _main } from './main'

workflow {
    _main()
}