#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/combine_layers"

include { _main } from './main'

workflow {
    _main()
}