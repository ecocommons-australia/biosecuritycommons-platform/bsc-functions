## 
- Fix bsrmap.combine_layers weights not being applied correctly.


## 1.11.0
- Rework pipelines to use Nextflow input/outputs for all data handling. This replaces hard coded absolute paths that do not scale well.

## 1.10.0
- Update Dockerfile with bsspread v0.1.7
- Add BSSPREAD distance_adjust param
- Add BSSPREAD region.two_tier param
- BSSPREAD schema reworks to use more if/then and less dependencies
- BSSPREAD add region_type to sub-schemas to simplify schema rules/frontend overrides
- Fix for incorrect param type config for BSDESIGN.region
- Import Results for use in Workflow Templates exemplars
- Add RM MEHW SDM output as real Result

## 1.9.0 
- Fix missing param in BSRMAP MEHW template
- Update BSRMAP pipelines to Nextflow DSL2 and consolidate into common modules

## 1.8.0 
- Rework BSSPREAD initializer schema to use if/then instead of dependencies
- Misc schema fixes to comply with strict validation
- Add more conditionals for faster CI, especially develop branch
- Change BSRMAP output genre to `DataGenreBSRiskMappingLikelihood` for establish_likelihood

## 1.7.3
- Fix dist_weight_layer schema regression

## 1.7.2
- Add BSRMAP.DISTANCE_WEIGHT_LAYER as toolkit function for all workflows
- Add BSRMAP.DISTANCE_WEIGHT_LAYER `decay_type` enum with none/half/beta

## 1.7.1
- Change BSRMAP.COMBINE_LAYERS output genre to DataGenreSpatialRaster and fix some incorrect names

## 1.7.0
- Update BSSPREAD 0.1.5
- Pin base image version on upstream pkg listed requirements.
- Fix for aggregate categories when smaller template extent is selected with large raster.
- Fix for create_region schema to ensure template is required.
- Improve BSSPREAD attractors schemas to better implement validation rules.
- Added Dockerfile-rstudio which uses the Function image as a base image and applies RStudio integration.
- Added DockerCompose entry to auto build above as bsc-functions-rstudio.
- Added bin/run_job.sh helper to execute platform Jobs directly from local Nextflow.
- Migrate BSDESIGN, BSIMPACT schema id's to new canonical name schema.bsc...

## 1.6.8
- bsspread@0.1.5
- bsrmap@0.1.6
- Fix for aggregate categories when smaller template extent is selected with large raster

## 1.6.7
- Ensure BSSPREAD attractor multiplier is null by default

## 1.6.6 
- Improve BSSPREAD attractors schema, enforce required properly and move more logic to schema and out of ui

## 1.6.5 
- Implement max_retries/backoff for data fetches to improve reliability.

## 1.6.4 
- Change BSRMAP.COMBINE_LAYERS output genre to `DataGenreSpatialRaster`
- Improve BSSPREAD attractors schemas to better implement validation rules
- Added Dockerfile-rstudio which uses the Function image as a base image and applies RStudio integration
- Added DockerCompose entry to auto build above as bsc-functions-rstudio
- Added bin/run_job.sh helper to execute platform Jobs directly from local Nextflow

## 1.6.3
- Tweak generic dataset schema so 'field' params dont cause validation errors
- Fix schema pipeline url import error.
- Allow for retries when fetching datasets via platform APIs 

## 1.6.2
- Fix for buffered_hull integration
- Now uses `django-func-utils` for platform dependencies instead of sdm-function base image

## 1.6.1
- Enable generic toolkit base template for all templates
- Fix some labels

## 1.6.0
- BSRMAP finally working (from this repo)

## 1.5.2
- Refactor BSIMPACT value method `asset_layers` to consolidate all associated parameters.  This will break existing projects

## 1.5.0
- BSIMPACT value method initial release schema, template.

## 1.4.2
- BSRMAP update package `cebra-analytics/bsrmap@0.1.4`
- Transform_layer fix bad output name

## 1.4.1
- Ensure all BSC workflow pipelines can run with up to 8 cores
- BSDESIGN_POF fix missing output for bayesian method

## 1.4.0
- BSDESIGN fix bad logical parmater for spatial method
- BSIMPACT initial prototype
- Update base image dependencies for all functions to the latest BSRMAP image from sdm-function

## 1.3.3
- BSDESIGN Add Conform Layer to all raster inputs
- BSDESIGN_POF add branching rules to schema parameters

## 1.3.2
- update efficacy label to Occurrence

## 1.3.1
- Fixes for BSDESIGN schema validation

## 1.3.0
- BSDESIGN schema validation tweaks
- BSSPREAD lookup table now uses col headings `distance/direction` `probability`
- BSSPREAD fix for lookup table param, cleanup some R helper funcs to be more readable
- BSSPREAD simplify OHW template
- BSRMAP Tweak labels and minor schema cleanups
- BSDESIGN fix issue with `descrete_alloc` param
- Add transform_layer toolkit function
- Add support for 'user-drawn' to `create_region`
- Add template example data imported via pipeline
- Add Orange Hawkweed exemplars for BSDESIGN and BSSPREAD
- Upgrade CEBRA packages to latest

## 1.2.0
- Add `helloworld` function example
- Aust Templates for Custom Region
- BSRMAP Update schemas, add attribution
- BSRMAP template - NVIS subgroups
- BSRMAP template - ABARES Tertiary
- BSRMAP Dist_weight_layer now defines fields interface to indicate acceptable csv fields.
- BSRMAP Combine_layers updated weights description
- BSRMAP MEHW template is now a delta of orig.
- BSDESIGN `0.1.5`
- BSDESIGN reorder integration script a bit more naturally
- BSDESIGN default False for bool param discrete_alloc
- BSDESIGN Add spatial `discrete_alloc` param

## 1.1.0
- Implement 'create_region' function and add to BSDESIGN, BSRMAP, BSPREAD
- Seperate out unreleased BSDESIGN functionality into 'dev' template
- `bsdesign@0.1.2`, implement `min_alloc` param for spatial, add design.csv output.
- BSDESIGN `sample_area` param now single numeric value (not per region)
- Implement pipelined test data import

## 1.0.6
- BSSPREAD template: Attractors define an output genre `DataGenreDispersalAttractorParam` 
- BSDESIGN branching rules for spatial method schemas

## 1.0.3
- BSSPREAD use dataset_spatial_raster interface for all rasters, 
- dataset_spatial_raster allow string and null values legal layers. Not ideal but working around persistent null creeping in from frontend.

## 1.0.2
- Fix BSSPREAD gravity dispersal not reading permabilitty parameter
- Import `generic.json` schema to replace API deployed version
- BSDESIGN Groundwork to support point data for Spatial method 
- BSDESIGN test data
- BSDESIGN Spatial add branch rules to schema for different `optimal` values

## 1.0.1
- Update Utils to `1.0.4` base image to resolve issue with `time_domain` parameter.
- BSC schemas use `accepts` property to provide criteria for what data is suitable as parameter input.
- generic/dataset.json `accepts` now differentiates between spatial-point and aspatial.
- generic/dataset.json `accepts` uses genres as criteria, eg. DataGenreSpatialPoint.
- mimetype retained for backwards compat.