#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/establishment_likelihood"

include { _main } from './main'

workflow {
    _main()
}