#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsimpact"

include { _main } from './main'

workflow {
    _main()
}