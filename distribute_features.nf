#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/distribute_features"

include { _main } from './main'

workflow {
    _main()
}