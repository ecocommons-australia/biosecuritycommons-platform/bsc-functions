ARG BASEIMG=r-base
ARG BASEIMG_VERSION=4.4.2

FROM ${BASEIMG}:${BASEIMG_VERSION}

ARG BUILD_ID
ENV BUILD_ID=$BUILD_ID

ENV TZ Australia/Brisbane

RUN apt-get update --fix-missing && apt install -y cmake gpg gpg-agent dirmngr libcurl4-openssl-dev

# Install pak (separate layer for caching)
RUN R -e 'install.packages("pak")'
# Install packages
RUN R -e 'pak::pkg_install(c( \
      "rjson@0.2.23", \
      "Rcpp@1.0.14", \
      "dplyr@1.1.4", \
      "BH@1.81.0-1", \
      "zoo@1.8-12" \
    ), \
    upgrade = FALSE, dependencies = NA \
  )'

# Ensure Docker build fails if any of the following packages are not installed
# (build with --progress=plain to see the output)
RUN R --vanilla -e ' \
  packages <- c("rjson", "Rcpp", "dplyr", "BH", "zoo"); \
  versions <- sapply(packages, function(x) as.character(packageVersion(x))); \
  cat( \
    "Installed package versions:\n", \
    paste( \
      " - ", \
      paste(packages, versions, sep = ": "), \
      collapse = "\n" \
    ), \
    "\n", \
    sep = "" \
  )'

# RTM src
RUN mkdir -p /rtm
WORKDIR /rtm
COPY ./src/content/toolkit/rtm /rtm

ARG BUILD_ID
ENV BUILD_ID=$BUILD_ID