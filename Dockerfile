ARG BASEIMG=registry.gitlab.com/ecocommons-australia/biosecuritycommons-platform/bsc-functions/base
ARG BASEIMG_VERSION=1.0

FROM ${BASEIMG}:${BASEIMG_VERSION}

RUN R -e 'remotes::install_github("cebra-analytics/bsrmap@0.1.10", upgrade="never", dependencies = FALSE)' \
  && R -e 'remotes::install_github("cebra-analytics/bsspread@0.1.22", upgrade="never", dependencies = TRUE)' \
  && R -e 'remotes::install_github("cebra-analytics/bsdesign@0.1.11", upgrade="never", dependencies = TRUE)' \
  && R -e 'remotes::install_github("cebra-analytics/bsimpact@0.0.5", upgrade="never", dependencies = TRUE)' \
  && R -e 'remotes::install_github("cebra-analytics/bsmanage@0.1.8", upgrade="never", dependencies = TRUE)'

RUN mkdir -p /srv/app
WORKDIR /srv/app

COPY requirements.txt /srv/app
RUN pip install --no-cache-dir -r requirements.txt
COPY ./src /srv/app

ARG BUILD_ID
ENV BUILD_ID=$BUILD_ID