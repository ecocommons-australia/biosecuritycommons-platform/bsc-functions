#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "toolkit/reclass_layer"

include { _main } from './main'

workflow {
    _main()
}