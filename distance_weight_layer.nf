#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/distance_weight_layer"

include { _main } from './main'

workflow {
    _main()
}