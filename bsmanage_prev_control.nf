#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsmanage_prev_control"

include { _main } from './main'

workflow {
    _main()
}