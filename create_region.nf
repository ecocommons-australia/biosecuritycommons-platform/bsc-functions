#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "toolkit/create_region"

include { _main } from './main'

workflow {
    _main()
}