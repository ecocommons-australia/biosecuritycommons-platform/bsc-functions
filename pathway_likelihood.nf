#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/pathway_likelihood"

include { _main } from './main'

workflow {
    _main()
}