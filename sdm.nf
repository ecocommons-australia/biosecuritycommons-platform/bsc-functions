#!/usr/bin/env nextflow
nextflow.enable.dsl=2

/**
 * Execute one or more SDM algorithms.
 * Expects params.functions to implement the JobRequestFunction interface.
 *
 * Note, This is a standalone pipeline and is not 
 * integrated with the combined BSC jobreq execution strategy.
*/

params.functions  = []
params.use_coord_cleaner = false
params.upload     = true
params._tests = null

include { 
    createParamsJson;
    prepareInputs;
    prepareInputs as prepareInputsCC;
    uploadResult;
} from './nxf_modules/process'

include { 
    SDM
} from './nxf_modules/process_sdm'

include { 
    coordCleaner
} from './nxf_modules/process_ccleaner'

include { 
    testResult;
} from './nxf_modules/process_test'

workflow sdm {
    take: function
    take: job_uuid
    take: params_json
    main:
        if (params.use_coord_cleaner){
            prepareInputsCC('coord_cleaner',
                job_uuid,
                params_json,
                Channel.of('').collectFile('name': 'EMPTY')
            ) | coordCleaner

            inputdir = coordCleaner.out.outputdir
        } else {
            inputdir = Channel.of('').collectFile('name': 'EMPTY')
        }

        prepareInputs(
            function,
            job_uuid,
            params_json,
            inputdir
        ) | SDM

        if (params._tests){
            testdir = Channel.fromPath("$projectDir/tests")
            testResult(
                testdir.combine(SDM.out.outputdir),
                params._tests,
                job_uuid
            )
        }
        else if (params.upload == true){
            uploadResult(SDM.out)
        }
}

workflow {
    if (params.functions.size == 0)
        error "params.functions should be a List of JobReqFunction representing each algorithm!"

    createParamsJson(
        channel.fromList(params.functions)
    ) | sdm
}

