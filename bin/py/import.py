#!/usr/bin/env python3
""" 
Deploy Schema and Workflow Template artifacts and test/example data to the platform.

Also submit JobRequests as specified by a curated Workflow Template.
This creates a Workflow Project in order to correctly marshall the jobrequest payload.

This is intended to be used as part of a deployment pipeline. 

Expects the following environment:
    
    KEYCLOAK_SERVER_URL
    KEYCLOAK_REALM
    KEYCLOAK_CLIENT_ID
    KEYCLOAK_CLIENT_SECRET_KEY

    CI_SERVER_PROTOCOL
    CI_SERVER_HOST
    CI_PROJECT_PATH

    DATA_INGESTER_API_URL
    JOB_MANAGER_API_URL
    FUNCTION_MANAGER_API_SCHEMA_URL
    WORKFLOW_MANAGER_API_TEMPLATE_URL
"""

import os
import os.path
import glob
import base64
import json
import logging
import sys
from urllib.parse import quote
from pathlib import Path
from func_utils.core.authsession import AuthSession
from requests.exceptions import HTTPError

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

log = logging.getLogger('Import')

if os.getenv('SOURCE_REPO_URL'):
    SOURCE_REPO_URL = os.getenv('SOURCE_REPO_URL')
    log.info(f"Using SOURCE_REPO_URL ({SOURCE_REPO_URL})")
else:
    # Determine from Gitlab CI vars
    SOURCE_REPO_URL = (os.getenv('CI_SERVER_PROTOCOL', '')
                       + '://'
                       + os.getenv('CI_SERVER_HOST', '')
                       + '/'
                       + os.getenv('CI_PROJECT_PATH', '')
                       + '/') if 'CI_SERVER_HOST' in os.environ else None

JOB_MANAGER_API_URL = os.getenv('JOB_MANAGER_API_URL', '')
JOB_REQ_ENDPOINT = JOB_MANAGER_API_URL+"api/jobrequest/<uuid>?target=k8"
PROJ_ENDPOINT = JOB_MANAGER_API_URL+"workflow/project/"
PROJCOMP_JOBREQ_ENDPOINT = JOB_MANAGER_API_URL+"workflow/project_component/<uuid>/jobrequest"

FUNCTION_MANAGER_API_SCHEMA_URL = os.getenv('FUNCTION_MANAGER_API_SCHEMA_URL')
WORKFLOW_MANAGER_API_TEMPLATE_URL = os.getenv(
    'WORKFLOW_MANAGER_API_TEMPLATE_URL',
    JOB_MANAGER_API_URL + "workflow/template/"
)

DATA_INGESTER_API_URL = os.getenv('DATA_INGESTER_API_URL')
DATA_INGESTER_API_IMPORT_ENDPOINT = "importInternal/"
DATA_INGESTER_API_DELETE_ENDPOINT = "api/dataset/{UUID}/delete"

JOB_MANAGER_API_URL = os.getenv('JOB_MANAGER_API_URL', '')
JOB_REQ_ENDPOINT = JOB_MANAGER_API_URL+"api/jobrequest/<uuid>?target=k8"
PROJ_ENDPOINT = JOB_MANAGER_API_URL+"workflow/project/"
PROJCOMP_JOBREQ_ENDPOINT = JOB_MANAGER_API_URL+"workflow/project_component/<uuid>/jobrequest"


class ImportException(Exception):
    def __init__(self, message: str, response=None):
        super().__init__(message)
        self.response = response
        if response is not None:
            dump(response.content, 2)


def dump(out, colidx=0):
    """ TDD helper. Nicer print."""
    try:
        out = json.dumps(out, indent=4, sort_keys=True)
    except Exception as e:
        pass

    colors = [
        "\033[96m",     # cyan
        "\x1b[33;20m",  # yellow
        "\x1b[31;20m",  # red
        "\x1b[31;1m"    # bold red
    ]
    reset = "\x1b[0m"
    print(colors[colidx] + str(out).replace('\\n', '\n').replace('\\t', '\t') + reset + '\n')


def usage():
    usage_str = (f"Usage: import.py TYPE PATH [VERSION] [COMMIT_REF]\n"
                 f"\n"
                 f" import.py schema    schema.json    0.1   develop\n"
                 f" import.py data      data.json\n"
                 f" import.py template  template.json  0.1\n"
                 f" import.py project   template.json\n"
                 f" import.py jobreq    jobrequest.json\n"
                 )
    print(usage_str)


def run(args):
    try:
        if args[1] is None or args[2] is None:
            raise Exception("Missing parameters")
    except Exception:
        usage()
        sys.exit(1)

    import_type = args[1]
    import_path = args[2]
    version = None
    ref = None
    try:
        version = args[3]
    except IndexError:
        version = os.getenv('CI_COMMIT_SHA')
    try:
        ref = args[4]
    except IndexError:
        ref = os.getenv('CI_COMMIT_SHA')

    overwrite = os.getenv('IMPORT_OVERWRITE', '').lower() in ("true", "1")

    try:
        log.info(f"Import.py {import_type} {import_path} {version} {ref}")

        session = AuthSession()

        def _import(file_path):
            if import_type == 'template':
                return import_template(session, file_path, version, overwrite)
            if import_type == 'schema':
                return import_schema(session, file_path, version, ref)
            if import_type == 'data':
                return import_data(session, file_path, overwrite)
            if import_type == 'project':
                return import_project_jobreq(session, file_path, overwrite)
            if import_type == 'jobreq':
                return submit_jobreq(session, load_json(file_path), overwrite)
            log.error(f"Unknown Import type ({import_type})")

        for ipath in import_path.splitlines():
            if os.path.isdir(ipath):
                for filename in os.listdir(ipath):
                    f = os.path.join(ipath, filename)
                    if os.path.isfile(f):
                        _import(f)
            else:
                _import(ipath)

        log.info(f"Operation complete")
        sys.exit(0)

    except (ImportException, HTTPError) as ex:
        log.error(f"Error ({str(type(ex).__name__ )}) {str(ex)}")
        if ex.response is not None:
            dump(ex.response.content, 2)
        sys.exit(1)


def import_template(session: AuthSession, import_path: dict, version: str, overwrite: bool):
    """ 
    Import Workflow Templates to the Workflow API.
    """
    if WORKFLOW_MANAGER_API_TEMPLATE_URL is None:
        raise ImportException('WORKFLOW_MANAGER_API_TEMPLATE_URL not exported')

    log.info(f"Template: {import_path}")
    tmpl = json.load(open(import_path))
    payload = {
        **tmpl,
        'version': version,
        'overwrite': True
    }
    if 'components' in tmpl:
        payload['components'] = json.dumps(tmpl.get('components'))

    log.debug(payload)

    resp = session.post(
        WORKFLOW_MANAGER_API_TEMPLATE_URL + '?overwrite=1',
        json=payload
    )
    dump(resp.content)

    if resp.status_code not in (200, 409):
        raise ImportException(f"Failed to import Template: {str(resp.status_code)}")

    # Attempt to import any Jobs for this Template.
    if len([
        x for x in tmpl.get('components', {}).values() if 'job_request_uuid' in x
    ]) > 0:
        import_project_jobreq(session, import_path, overwrite)


def import_schema(session: AuthSession, schema_path: str, version: str, ref: str):
    """ 
    Import Function schemas to the Function Manager API
    """
    if FUNCTION_MANAGER_API_SCHEMA_URL is None:
        raise ImportException('FUNCTION_MANAGER_API_SCHEMA_URL not exported')
    if version is None:
        raise ImportException('CI_COMMIT_SHA not exported (version is empty)')
    if os.getenv('CI_PROJECT_PATH') is None:
        raise ImportException('CI_PROJECT_PATH not exported')

    log.info(f"import_schema: {schema_path}")

    try:
        schema = json.load(open(schema_path))

        if not isinstance(schema, dict):
            raise ImportException(f"Could not parse '{schema_path}'")

        # Nextflow pipeline URL.
        # This uses a repo naming convention.
        # It is expected to match schema name ie: bsdesign.json -> bsdesign.nf
        # Only specify url if the schema describes a top level Function interface (input, output).
        if (SOURCE_REPO_URL and isinstance(schema.get('input'), dict) and isinstance(schema.get('output'), dict)):
            pipeline = os.path.join(SOURCE_REPO_URL, Path(os.path.basename(schema_path)).stem + '.nf')
        else:
            pipeline = ''

        schemaid = schema.get('$id', schema.get('input', {}).get('$id'))
        if not isinstance(schemaid, str):
            raise ImportException(f"Schema does not define input.$id'")

        api_url = FUNCTION_MANAGER_API_SCHEMA_URL

        payload = {
            'name':           schema.get('title', schema.get('name')),
            'description':    schema.get('description'),
            'schemaid':       schemaid,
            'pipeline':       pipeline,
            'mdpath':         schema_path,
            'version':        version,
            'hash':           ref if ref else version,
            'projectid':      os.getenv('CI_PROJECT_PATH')
        }

        resp = session.get(api_url+'?schemaid='+quote(schemaid, safe=''))
        if resp.status_code != 200:
            raise ImportException(f"Failed to import: {str(resp.status_code)}")
        dump(resp.content)

        if (len(resp.json()) > 0):
            # update
            uuid = resp.json()[0].get('uuid')
            resp = session.post(api_url+uuid+'/action', json=payload)
            if resp.status_code != 200 and resp.status_code != 409:
                raise ImportException(f"Failed to import: {str(resp.status_code)}", resp)
        else:
            # register
            resp = session.post(api_url, json=payload)
            if resp.status_code != 200:
                raise ImportException(f"Failed to import: {str(resp.status_code)}", resp)

    except Exception as e:
        raise ImportException(f"Failed to import: {e}") from e


def load_json(fpath: str) -> dict:
    with open(fpath, "r", encoding="utf-8") as f:
        return json.load(f)


def read_file_base64(fpath: str, encoding="utf-8") -> str:
    with open(fpath, "rb") as f:
        return base64.b64encode(f.read()).decode(encoding)


def import_data(session: AuthSession, import_json_path: str, overwrite=False):
    """ Import test data """
    if DATA_INGESTER_API_URL is None:
        raise ImportException('DATA_INGESTER_API_URL not exported')

    for item in load_json(import_json_path):
        log.debug(f"Processing import file ({item})")

        if item.get('delete') is True or overwrite is True:
            session.delete(
                DATA_INGESTER_API_URL + 'api/dataset/' + item.get('uuid') + '/delete?force=true'
            )

        if 'uploadedFiles' in item:
            payload = {
                **item,
                'uploadedFiles': [{
                    'name': os.path.basename(item['uploadedFiles'][0]),
                    'data': read_file_base64(item['uploadedFiles'][0])
                }]
            }
            url = DATA_INGESTER_API_URL + DATA_INGESTER_API_IMPORT_ENDPOINT

            resp = session.post(url, json=payload)

            log.info(f"Response: ({url})")
            dump(resp.content)
            if resp.status_code != 200 and resp.status_code != 409:
                raise ImportException(f"Failed to import Dataset: {str(resp.status_code)}")


def import_project_jobreq(
    session: AuthSession,
    payload_path: str,
    overwrite=False
):
    """ 
        Submit Project Job Requests. This will submit a Jobreq
        For any component of type Function that specifies 'job_request_uuid'
    """
    if JOB_MANAGER_API_URL is None:
        raise APIException('JOB_MANAGER_API_URL not exported')

    log.info(f"Template: {payload_path} (importing JobRequests)")

    # Create Project
    wf_tmpl = load_json(payload_path)
    resp = session.post(PROJ_ENDPOINT, json={
        'label': wf_tmpl.get('label'),
        'description': wf_tmpl.get('description'),
        'template_name': wf_tmpl.get('name')
    })
    resp.raise_for_status()
    dump(resp.json())
    proj_uuid = resp.json().get('uuid')

    # Get Project
    resp = session.get(f'{PROJ_ENDPOINT}{proj_uuid}')
    resp.raise_for_status()

    # Get executable components
    to_exec = []
    for k, c in wf_tmpl.get('components').items():
        if 'job_request_uuid' in c:
            to_exec.append(k)
    log.info(f"ProjectComponents: to execute {to_exec}")

    try:
        for c in resp.json().get('components'):
            if c.get('name') in to_exec and c.get('type') == 'Function':
                resp = session.get(
                    PROJCOMP_JOBREQ_ENDPOINT.replace('<uuid>', c.get('uuid'))
                )
                resp.raise_for_status()
                jobreq_payload = resp.json()
                dump(jobreq_payload)

                # Submit new Jobrequest
                submit_jobreq(session, {
                    **jobreq_payload,
                    "is_public": True,
                    "metadata": {
                        "user-agent": "au.org.biosecuritycommons.workflow",
                        "user-agent-name": "Workflow Builder",
                        "user-agent-job-client-url": (f"https://workflow.app.biosecuritycommons.org.au/"
                                                      f"project?create={wf_tmpl.get('name')}&tab=create"),
                        "project_id": proj_uuid
                    }
                }, overwrite)
    finally:
        session.delete(f'{PROJ_ENDPOINT}{proj_uuid}').raise_for_status()


def submit_jobreq(session: AuthSession, payload: dict, overwrite=False, pipeline_ver=None):
    """ Submit a JobRequest """
    if JOB_MANAGER_API_URL is None:
        raise APIException('JOB_MANAGER_API_URL not exported')

    log.info(f"JobRequest: {payload.get('title')}")

    jobreq_uuid = payload.get('uuid')
    if jobreq_uuid is None:
        raise Exception(f"UUID not in JobRequest payload")
    jobreq_url = JOB_REQ_ENDPOINT.replace('<uuid>', jobreq_uuid)

    resp = session.get(jobreq_url)
    if resp.status_code == 200:
        if overwrite is True:
            session.delete(jobreq_url)

        elif resp.json().get('status') == 'FAILED':
            log.warning("JobRequest: Existing status is FAILED, deleting and submitting again!")
            session.delete(jobreq_url)
            # ImportException("JobRequest: Already exists but status is FAILED. "
            #                 "Manually delete to re-import.")

    # Inject pipeline version
    if pipeline_ver:
        payload['version'] = pipeline_ver

    resp = session.put(jobreq_url, data=payload)
    if resp.status_code != 200 and resp.status_code != 409:
        raise ImportException(f"JobRequest: Failed to submit ({str(resp.status_code)})")
    return resp


if __name__ == "__main__":
    run(sys.argv)
