#!/usr/bin/env python3
""" 
Submit a JobRequest to the platform.

Expects the following environment:
    
    KEYCLOAK_SERVER_URL
    KEYCLOAK_REALM
    KEYCLOAK_CLIENT_ID
    KEYCLOAK_CLIENT_SECRET_KEY
    JOB_MANAGER_API_URL
"""

import os
import json
import logging
import sys
from func_utils.core.authsession import AuthSession

logging.basicConfig(level=os.environ.get("LOGLEVEL", "INFO"))

log = logging.getLogger('Import')

JOB_MANAGER_API_URL = os.getenv('JOB_MANAGER_API_URL')
JOB_REQ_ENDPOINT = "api/jobrequest/<uuid>?target=k8"


class APIException(Exception):
    pass


def usage():
    usage_str = (f"Usage: submit_jobreq.py payload_json_path [pipeline_version]\n")
    print(usage_str)


def run(args):
    try:
        if args[1] is None:
            raise Exception("Missing arg 'payload_json_path'")
    except Exception:
        usage()
        sys.exit(1)

    payload_path = args[1] if len(args) > 1 else None
    pipeline_ver = args[2] if len(args) > 2 else None
    overwrite = os.getenv('IMPORT_OVERWRITE', '').lower() in ('true', '1')

    try:
        session = AuthSession()

        for ipath in payload_path.splitlines():
            if os.path.isdir(ipath):
                for filename in os.listdir(ipath):
                    f = os.path.join(ipath, filename)
                    if os.path.isfile(f):
                        submit_jobreq(session, f, overwrite, pipeline_ver)
            else:
                submit_jobreq(session, ipath, overwrite, pipeline_ver)

        sys.exit(0)

    except APIException as e:
        log.error(f"Error ({str(type(e).__name__ )}) {str(e)}")
        sys.exit(1)


def load_json(fpath: str) -> dict:
    with open(fpath, "r", encoding="utf-8") as f:
        return json.load(f)


def submit_jobreq(session: AuthSession, payload_path: str, overwrite=False, pipeline_ver=None):
    """ Submit a JobRequest """
    if JOB_MANAGER_API_URL is None:
        raise APIException('JOB_MANAGER_API_URL not exported')

    log.info(f"submit_jobreq: ({payload_path})")

    payload = load_json(payload_path)
    jobreq_uuid = payload.get('uuid')
    if jobreq_uuid is None:
        raise Exception(f"UUID not in JobRequest payload")
    jobreq_url = JOB_MANAGER_API_URL + JOB_REQ_ENDPOINT.replace('<uuid>', jobreq_uuid)

    resp = session.get(jobreq_url)
    if resp.status_code == 200:
        if overwrite is True:
            session.delete(jobreq_url)

        elif resp.json().get('status') == 'FAILED':
            Exception("JobRequest: Already exists but status is FAILED. "
                      "Manually delete or use IMPORT_OVERWRITE=true")

    # Inject pipeline version
    if pipeline_ver:
        payload['version'] = pipeline_ver

    resp = session.put(jobreq_url, data=payload)
    if resp.status_code != 200 and resp.status_code != 409:
        raise APIException(f"Failed to submit: {str(resp.status_code)}")


if __name__ == "__main__":
    run(sys.argv)
