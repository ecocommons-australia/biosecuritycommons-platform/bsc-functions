#!/usr/bin/env python3
""" 
Helper to convert RDoc comments to a JSON friendly string
"""
import re
import sys

def usage():
    usage_str = (f"Usage: rdoc_2_json.py 'source_file'\n")
    print(usage_str)


def run(args):
    if args[1] is None:
        usage()
        sys.exit(1)

    source_file = args[1]

    COMMENT = re.compile(r"^#'[\s\t]+", flags=re.MULTILINE)
    MARKUP = re.compile(r"\\", flags=re.MULTILINE)
    LN = re.compile(r"\n", flags=re.MULTILINE)

    with open(source_file, 'r', encoding='utf-8') as f:
        rdoc = f.read()
        rdoc = re.sub(COMMENT,  "", str(rdoc))
        rdoc = re.sub(MARKUP,   "\\\\\\\\", str(rdoc))
        rdoc = re.sub(LN,       "\\\\n", str(rdoc))
        print(rdoc)

if __name__ == "__main__":
    run(sys.argv)
