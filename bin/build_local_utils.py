#!/usr/bin/env bash
# Helper to build utils with a local copy of django-func-utils
# Note, this produces an image suitable for local dev only..

LIB_PATH=${LIB_PATH:-$HOME/dev/lib/django-func-utils}

docker build -t bsc-functions/utils -f - $LIB_PATH <<EOF
FROM --platform=linux/amd64 bsc-functions/utils
COPY ./func_utils/core/* /usr/local/lib/python3.10/site-packages/func_utils/core/
COPY ./func_utils/scripts/* /usr/local/lib/python3.10/site-packages/func_utils/scripts/
EOF