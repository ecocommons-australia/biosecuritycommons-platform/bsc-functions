#!/usr/bin/env bash
# LOCAL DEV

# Example: ./bin/run_pipeline.sh bsspread.nf src/schemas/examples/bsspread-example.json

set -e

[[ -z $1 || -z $2 ]] && printf "Usage: run_pipeline.sh PIPELINE_PATH PARAMS_PATH [JOB_ID]\n\n" && exit 1

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
TIMESTAMP=$(date +%s)
PIPELINE=$1
PARAMS=$2
JOB_ID=${3:-""}
UPLOAD=${UPLOAD:-"false"}
export CPUS=${TASK_CPUS:-2}
export AUTH_ENVFILE=${AUTH_ENVFILE:-"${SCRIPT_DIR}/../.env.auth"}
export NXF_ENVFILE=${NXF_ENVFILE:-"${SCRIPT_DIR}/../.env"}
export NXF_WORKDIR=${SCRIPT_DIR}/../nxf_work/

# Cleanup previous runs
rm -rf ${NXF_WORKDIR}/* ${SCRIPT_DIR}/../.nextflow.*.log

echo "using workdir: ${NXF_WORKDIR}"
echo "using env: ${NXF_ENVFILE} ${AUTH_ENVFILE}"

# Create AUTH_ENVFILE if it does not exist
touch ${AUTH_ENVFILE} || true

# Run pipeline
nextflow run $PIPELINE \
    -w ${NXF_WORKDIR} \
    -profile ${NXF_PROFILE:-"local"} \
    -params-file $PARAMS \
    -ansi-log 'false'\
    --absWorkdir '/workdir' \
    --jobUuid $JOB_ID \
    --job_uuid $JOB_ID \
    --upload $UPLOAD \
    --inputs $PARAMS \
    -name "job_${TIMESTAMP}" \
    -main-script $PIPELINE \
    -with-report "report.html" \
    -with-dag "flowchart.html" \
    -latest 1 $NXF_ARGS \
    -resume

# nextflow log "job_${TIMESTAMP}" -f stdout,stderr