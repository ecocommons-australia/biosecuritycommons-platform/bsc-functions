#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.functions = null

include {
    job;
    jobReqFunc as jrf0;
    jobReqFunc as jrf1;
    jobReqFunc as jrf2;
    jobReqFunc as jrf3;
    jobReqFunc as jrf4;
    jobReqFunc as jrf5;
    jobReqFunc as jrf6;
    jobReqFunc as jrf7;
    jobReqFunc as jrf8;
    jobReqFunc as jrf9;
} from './nxf_modules/workflow'

/**
    The main entrypoint for the combined BSC pipeline.
    This can execute any individual Function (via the Job interface), or a combined set (JobRequest interface).
    Either of these can be made up of any number of individual Nextflow processes.
*/
workflow {
    _main()
}

/** 
 * Run using either Job or JobRequest execution strategy.
 * This is determined based on the parameter signature.
 */
workflow _main {
    if (params.functions instanceof List){
        jobreq()

    } else if (params.inputs && params.function && params.job_uuid){
        job(
            channel.fromPath(params.inputs),
            params.function,
            params.job_uuid
        )

    } else {
        error "Cannot determine exec strategy, params should contain either 'functions:List' or ('inputs:string', 'function:string', 'job_uuid:string')"
    }
}

/**
 * Execute a JobRequest with multiple functions.
 * params.functions should implement the JobRequestFunction[] interface.
 *
 * Supports basic serial and parallel execution of Jobs (defined as NF workflows) by 
 * providing a numeric value for each JobReqFunc as either an equivalent numeric value,
 * (for parallel) or sequential (for series).
 *
 * This will execute to a max sequence depth of 10. It could be extended easily 
 * by adding more module aliases above.
 *
 * This operates with a path input channel that accumulates all platform data entities
 * as they are fetched or created by a process. This makes data available to every function
 * (regardless of whether it actually uses it).  In theory this could add overhead, 
 * but it's symlinked and lazy fetched across nodes (when using Fusion) so no major concern. 
 *
 */
workflow jobreq {
    functions_ch = channel.fromList(params.functions)
    inputs = Channel.of('').collectFile('name': 'EMPTY')

    params.functions.sequence.unique().sort().eachWithIndex { seq, idx ->
        ch = functions_ch.filter { it.sequence == idx }
        ch.collect().view { "Sequence ${seq} -> ${it.function}" }

        if (idx > 0)
            inputs = out.join(inputs, remainder: true)

        out = "jrf$idx"(ch, inputs.collect())
    }
}