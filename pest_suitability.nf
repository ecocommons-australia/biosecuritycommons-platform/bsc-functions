#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/pest_suitability"

include { _main } from './main'

workflow {
    _main()
}