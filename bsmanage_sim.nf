#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsmanage_sim"

include { _main } from './main'

workflow {
    _main()
}