{
    "name": "Range Bagging",
    "description": "The model building component of an implementation of the range bagging species distribution modelling (SDM) method (Drake, 2015)",
    "algorithm_category": "profile",
    "package_dependencies": "cebra-analytics/bssdm",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.biosecuritycommons.org.au/sdm/rangebagging.json",
        "$attribution": "Drake, J. M. (2015). Range bagging: a new method for ecological niche modelling from presence-only data. \\emph{Journal of the Royal Society Interface}, 12(107), 20150086. \\doi{10.1098/rsif.2015.0086}",
        "title": "Range Bagging SDM Function Parameters",
        "description": "Schema for Range Bagging SDM Function parameters",
        "type": "object",
        "properties": {
            "species_occurrence_dataset": {
                "title": "Species occurrence dataset",
                "description": "Species occurrence dataset",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "description": "UUID of occurrence dataset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    }
                },
                "required": [
                    "uuid"
                ]
            },
            "use_coord_cleaner": {
                "title": "Use coordinate cleaner",
                "description": "Automatically cleanup common spatial errors in the species occurrence records",
                "type": "boolean",
                "default": false
            },
            "absence": {
                "title": "species absence dataset",
                "description": "UUID of species absence dataset",
                "type": [
                    "string",
                    "null"
                ],
                "default": null
            },
            "base_layer": {
                "title": "base layer dataset",
                "description": "UUID of bias dataset",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset id",
                        "description": "UUID of environmental dataset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    }
                },
                "required": [
                    "uuid",
                    "layers"
                ],
                "default": null
            },
            "predictors": {
                "title": "predictors",
                "description": "List of environmental dataset UUID with a list of selected layer names.",
                "type": "array",
                "items": {
                    "type": "object",
                    "properties": {
                        "uuid": {
                            "title": "dataset id",
                            "description": "UUID of environmental dataset",
                            "type": "string",
                            "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                        },
                        "layers": {
                            "title": "layers",
                            "description": "List of layer names",
                            "type": "array",
                            "items": {
                                "description": "Layer name",
                                "type": "string"
                            },
                            "minItems": 1,
                            "uniqueItems": true
                        }
                    },
                    "required": [
                        "uuid",
                        "layers"
                    ]
                },
                "minItems": 1,
                "uniqueItems": true
            },
            "modelling_region": {
                "title": "modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            },
            "scale_down": {
                "title": "scale down",
                "description": "Resample by scaling down the resolution : user-defined; finest-resolution (lowest resolution); coarsest-resolution (highest resolution)",
                "type": "string",
                "default": "finest-resolution"
            },
            "random_seed": {
                "title": "random seed",
                "description": "seed used for generating random values. (algorithm parameter)",
                "type": [
                    "integer",
                    "null"
                ],
                "default": null,
                "minimum": -2147483648,
                "maximum": 2147483647
            },
            "n_models": {
                "title": "number of models",
                "description": "number of models hull models to build in sampled environment",
                "type": "integer",
                "default": 100,
                "exclusiveMinimum": 0
            },
            "n_dim": {
                "title": "number of dimensions",
                "description": "Number of dimensions (variables) of sampled convex hull models",
                "type": "integer",
                "default": 2,
                "exclusiveMinimum": 0
            },
            "sample_prop": {
                "title": "Proportion of occurrence records to sample for fitting",
                "description": "Proportion of occurrence records to sample for fitting",
                "type": "number",
                "default": 0.5,
                "exclusiveMinimum": 0,
                "maximum": 1
            },
            "limit_occur": {
                "title": "Limit occurrences",
                "description": "Logical to indicate whether to limit occurrence data to one per environment data cell",
                "type": "boolean",
                "default": true
                },    
            "unconstraint_map": {
                "title": "unconstraint map",
                "description": "Indicates whether to generate an unconstraint map or not. True by default.",
                "type": "boolean",
                "default": true
            },
            "generate_convexhull": {
                "title": "generate convex-hull polygon",
                "description": "Indicates to generate and apply a convex-hull polygon of the occurrence dataset to constraint. False by default.",
                "type": "boolean",
                "default": false
            }
        },
        "required": [
            "species_occurrence_dataset",
            "absence",
            "predictors",
            "modelling_region",
            "scale_down",
            "random_seed",
            "limit_occur",
            "n_dim",
            "n_models",
            "sample_prop"    
        ],
        "$defs": {}
    },
    "output": {
        "Rplots.pdf": {
            "skip": true
        },
        "eval/AUC.png": {
            "skip": true
        },
        "results.html": {
            "skip": true
        },
        "eval/Proj_current*.tif": {
            "title": "Projection to current climate",
            "genre": "DataGenreCP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 1
        },
        "eval/Proj_current*_unconstrained.tif": {
            "title": "Projection to current climate - unconstrained",
            "genre": "DataGenreCP_ENVLOP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 2
        },
        "eval/Proj_current_*.png": {
            "title": "Projection plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 3
        },
        "eval/Proj_current_*_unconstrained.png": {
            "title": "Projection plot - unconstrained",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 4
        },        
        "eval_tables/Occurrence_environmental_*.csv": {
            "title": "Occurrence points with environmental data",
            "genre": "DataGenreSpeciesOccurEnv",
            "mimetype": "text/csv",
            "order": 5
        },
        "eval/Response_curve_*.png": {
            "title": "Marginal Response Curve",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 8
        },
        "eval/*_response_curve_*.png": {
            "hidden": true,
            "title": "Marginal Response Curve",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 8
        },
        "eval_tables/Biomod2_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 9
        },
        "eval_tables/Maxent_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 10
        },
        "eval_tables/Evaluation-statistics_*.csv": {
            "title": "Model accuracy statistics",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 11
        },
        "eval/Dismo-presence-absence-plot_*.png": {
            "title": "Presence/absence density plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 12
        },
        "eval/Dismo-presence-absence-hist_*.png": {
            "title": "Presence background",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 13
        },
        "eval/Dismo-TPR-TNR_*.png": {
            "title": "Sensitivity/Specificity plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 14
        },
        "eval/Dismo-error-rates_*.png": {
            "title": "Error rates plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 15
        },
        "eval/Dismo-ROC_*.png": {
            "title": "ROC plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 16
        },
        "eval/Dismo-loss-functions_*.png": {
            "title": "Loss functions plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 17
        },
        "eval_tables/Loss-function-intervals-table_*.csv": {
            "title": "Loss functions table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 18
        },
        "eval/Dismo-loss-intervals_*.png": {
            "title": "Loss functions intervals",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 19
        },
        "eval_tables/Evaluation-data_*.csv": {
            "title": "Model evaluation data",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 20
        },
        "*.R": {
            "title": "Job Script",
            "genre": "JobScript",
            "mimetype": "text/x-r",
            "order": 21
        },
        "*_model_object.rds": {
            "title": "R SDM Model object",
            "genre": "DataGenreSDMModel",
            "mimetype": "application/x-r-data",
            "order": 23
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript",
            "order": 24
        },
        "modelling_region.json": {
            "title": "modelling region",
            "hidden": true,
            "genre": "DataGenreSDMModellingRegion",
            "mimetype": "application/json",
            "order": 29
        },
        "prediction_region.json": {
            "title": "prediction region",
            "hidden": true,
            "genre": "DataGenreSDMModellingRegion",
            "mimetype": "application/json",
            "order": 30
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json",
            "order": 31
        },
        "eval_tables/*.csv": {
            "title": "Model accuracy statistics",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 40
        },
        "eval/*.png": {
            "title": "New Model plots",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 50
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript",
            "order": 100
        }
    }
}