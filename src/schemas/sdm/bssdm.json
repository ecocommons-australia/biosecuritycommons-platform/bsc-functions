{
    "name": "Species Distribution Modelling (SDM)",
    "description": "Species Distribution Modelling (SDM)",
    "algorithm_category": "profile",
    "package_dependencies": "cebra-analytics/bssdm",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.biosecuritycommons.org.au/bssdm/bssdm.json",
        "$attribution": "Range Bagging: Drake, J. M. (2015). Range bagging: a new method for ecological niche modelling from presence-only data. \\emph{Journal of the Royal Society Interface}, 12(107), 20150086. \\doi{10.1098/rsif.2015.0086}\n\nClimatch: ABARES (2020). Climatch v2.0 User Manual. Canberra. \\url{https://climatch.cp1.agriculture.gov.au/} Accessed: November 2021.",
        "title": "SDM Parameters",
        "description": "Species Distribution Modelling",
        "type": "object",
        "allOf": [
            { "$ref": "https://schema.biosecuritycommons.org.au/bssdm/occurrences.json" },
            { "$ref": "https://schema.biosecuritycommons.org.au/bssdm/predictors.json" },
            { "$ref": "https://schema.biosecuritycommons.org.au/bssdm/algorithms.json" }
        ],
        "properties": {
            "modelling_region": {
                "title": "Modelling region",
                "description": "Geojson object describing the geographic constraint region",
                "type": "object",
                "properties": {
                    "geojson": {
                        "type": "string",
                        "description": "Geojson string describing the geographic constraint region"
                    },
                    "method": {
                        "type": "string",
                        "description": "Method selected for generation",
                        "enum": [
                            "convex hull",
                            "pre-defined region",
                            "environmental envelope",
                            "user-drawn",
                            "shapefile"
                        ],
                        "default": "convex hull"
                    },
                    "radius": {
                        "type": "number",
                        "description": "Buffer radius in Km",
                        "minimum": 0.0,
                        "default": 0.0
                    },
                    "predefined_region": {
                        "type": "object",
                        "properties": {
                            "region_type": {
                                "type": "string",
                                "description": "Region type selected for pre-defined region"
                            },
                            "regions": {
                                "type": "array",
                                "items": {
                                    "type": "string"
                                },
                                "description": "Regions selected for pre-defined region",
                                "minItems": 1,
                                "uniqueItems": true
                            }
                        },
                        "required": [
                            "region_type",
                            "regions"
                        ]
                    }
                },
                "required": [
                    "geojson",
                    "method"
                ]
            }
        },
        "required": [
            "occurrences",
            "predictors",
            "modelling_region",
            "algorithms"
        ],
        "definitions": {}
    },
    "output": {
        "Rplots.pdf": {
            "skip": true
        },
        "eval/AUC.png": {
            "skip": true
        },
        "results.html": {
            "skip": true
        },
        "eval/Proj_current*.tif": {
            "title": "Projection to current climate",
            "genre": "DataGenreCP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 1
        },
        "eval/Proj_current*_unconstrained.tif": {
            "title": "Projection to current climate - unconstrained",
            "genre": "DataGenreCP_ENVLOP",
            "mimetype": "image/geotiff",
            "layer": "projection_probability",
            "data_type": "Continuous",
            "order": 2
        },
        "eval/Proj_current_*.png": {
            "title": "Projection plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 3
        },
        "eval/Proj_current_*_unconstrained.png": {
            "title": "Projection plot - unconstrained",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 4
        },        
        "eval_tables/Occurrence_environmental_*.csv": {
            "title": "Occurrence points",
            "genre": "DataGenreSpeciesOccurEnv",
            "mimetype": "text/csv",
            "order": 5
        },
        "eval_tables/occurrences_cleaned*.csv": {
            "title": "Cleaned occurrence points",
            "genre": "DataGenreSpeciesOccurEnv",
            "mimetype": "text/csv",
            "order": 5
        },
        "eval/Response_curve_*.png": {
            "title": "Marginal Response Curve",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 8
        },
        "eval/*_response_curve_*.png": {
            "hidden": true,
            "title": "Marginal Response Curve",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 8
        },
        "eval_tables/Biomod2_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 9
        },
        "eval_tables/Maxent_like_VariableImportance_*.csv": {
            "title": "Variable importance table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 10
        },
        "eval_tables/Evaluation-statistics_*.csv": {
            "title": "Model accuracy statistics",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 11
        },
        "eval/Dismo-presence-absence-plot_*.png": {
            "title": "Presence/absence density plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 12
        },
        "eval/Dismo-presence-absence-hist_*.png": {
            "title": "Presence background",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 13
        },
        "eval/Dismo-TPR-TNR_*.png": {
            "title": "Sensitivity/Specificity plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 14
        },
        "eval/Dismo-error-rates_*.png": {
            "title": "Error rates plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 15
        },
        "eval/Dismo-ROC_*.png": {
            "title": "ROC plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 16
        },
        "eval/Dismo-loss-functions_*.png": {
            "title": "Loss functions plot",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 17
        },
        "eval_tables/Loss-function-intervals-table_*.csv": {
            "title": "Loss functions table",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 18
        },
        "eval/Dismo-loss-intervals_*.png": {
            "title": "Loss functions intervals",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 19
        },
        "eval_tables/Evaluation-data_*.csv": {
            "title": "Model evaluation data",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 20
        },
        "*.R": {
            "title": "Job Script",
            "genre": "JobScript",
            "mimetype": "text/x-r",
            "order": 21
        },
        "*_model_object.rds": {
            "title": "R SDM Model object",
            "genre": "DataGenreSDMModel",
            "mimetype": "application/x-r-data",
            "order": 23
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript",
            "order": 24
        },
        "modelling_region.json": {
            "title": "modelling region",
            "hidden": true,
            "genre": "DataGenreSDMModellingRegion",
            "mimetype": "application/json",
            "order": 29
        },
        "prediction_region.json": {
            "title": "prediction region",
            "hidden": true,
            "genre": "DataGenreSDMModellingRegion",
            "mimetype": "application/json",
            "order": 30
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json",
            "order": 31
        },
        "eval_tables/*.csv": {
            "title": "Model accuracy statistics",
            "genre": "DataGenreSDMEval",
            "mimetype": "text/csv",
            "order": 40
        },
        "eval/*.png": {
            "title": "New Model plots",
            "genre": "DataGenreSDMEval",
            "mimetype": "image/png",
            "order": 50
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript",
            "order": 100
        }
    }
}