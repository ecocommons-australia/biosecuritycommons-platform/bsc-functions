{
    "name": "Aggregate Categories",
    "description": "Aggregate categories within a spatial layer",
    "algorithm_category": "bsrmap",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "http://ecocommons.org.au/bsrmap/aggregate_categories.json",
        "$attribution": "Camac, JS., Baumgartner, J., Robinson, A., Elith J (2020) \\emph{Developing pragmatic maps of establishment likelihood for plant pests. Technical Report for CEBRA project 170607}. \\url{https://cebra.unimelb.edu.au/__data/assets/pdf_file/0012/3539397/170607_final_report.pdf}\n\nCamac, J.S., Baumgartner, J.B., Hester, S., Subasinghe, R., Collins, S. (2021) \\emph{Using edmaps & Zonation to inform multi-pest early-detection surveillance designs. Technical Report for CEBRA project 20121001.} \\url{https://cebra.unimelb.edu.au/__data/assets/pdf_file/0009/3889773/20121001_final_report.pdf}",
        "title": "Aggregate Categories Function Parameters",
        "description": "Aggregate the categories within a spatial layer to a coarser resolution, that of another (template) layer, based on the presence of each of the user-selected categories in each cell. The resulting aggregated layer cells may be binarized to one or zero, or indicate the proportion of selected categories present in the cell.",
        "type": "object",
        "properties": {
            "spatial": {
                "title": "Spatial",
                "description": "A raster representing the spatial layer of category values to be aggregated.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of environmental dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    },
                    "accepts": {
                        "readOnly": true,
                        "title": "Accepts",
                        "description": "Accepted data type identifiers",
                        "type": "array",
                        "items": {
                            "type": "string"
                        },
                        "uniqueItems": true,
                        "default": [ 
                            "spatialType:Categorical"
                        ]
                    }
                },
                "required": [
                    "uuid"
                ],
                "dependencies": {
                    "uuid": [
                      "layers",
                      "source_type"
                    ]
                }
            },
            "template": {
                "title": "Spatial template",
                "description": "A raster representing the spatial layer with the spatial resolution (and configuration) to aggregate (conform) to.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of environmental dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    },
                    "accepts": {
                        "readOnly": true,
                        "title": "Accepts",
                        "description": "Accepted data type identifiers",
                        "type": "array",
                        "items": {
                            "type": "string"
                        },
                        "uniqueItems": true,
                        "default": [ "mimetype:image/geotiff" ]
                    }
                },
                "required": [
                    "uuid"
                ],
                "dependencies": {
                    "uuid": [
                      "layers",
                      "source_type"
                    ]
                }
            },
            "categories": {
                "title": "Possible categories",
                "description": "All possible category values. If this is left NULL (default) the category values are extracted from the layer via possible pre-existing categories (if present) or via calculating unique cell values (slower).",
                "type": [
                    "null",
                    "array"
                ],
                "items": {
                    "description": "Category value",
                    "type": "number"
                },
                "default": null
            },
            "selected": {
                "title": "Selected categories",
                "description": "Selected category values for inclusion in the resultant aggregate layer.",
                "type": [
                    "null",
                    "array"
                ],
                "items": {
                    "description": "Category value",
                    "type": "number"
                },
                "default": null
            },
            "binarize": {
                "title": "Binarize",
                "description": "Indicate if the aggregated cells should be binarized, i.e. set to 1 for cells containing selected category values.",
                "type": "boolean",
                "default": true
            }
        },
        "required": [
            "spatial",
            "template",
            "categories",
            "selected",
            "binarize"
        ]
    },
    "output": {
        "aggregate_categories.tif": {
            "title": "Aggregate Categories",
            "genre": "DataGenreRiskMappingResult",
            "mimetype": "image/geotiff",
            "layer": "aggregate_categories",
            "data_type": "Continuous"
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript"
        }
    }
}
