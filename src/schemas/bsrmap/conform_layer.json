{
    "name": "Conform Layer",
    "description": "Conforms the spatial configuration of a spatial layer to that of a template layer.",
    "algorithm_category": "bsrmap",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.biosecuritycommons.org.au/bsrmap/conform_layer.json",
        "title": "Conform Layer Function Parameters",
        "description": "Conform the spatial configuration (CRS, extent, and resolution) of a spatial layer to that of another (template) layer and optionally apply additional transformations.",
        "type": "object",
        "properties": {
            "operation": {
                "title": "Operation",
                "description": "Normalize: Set to a value 0-1 based on cell-wise minimum and maximum values\nBinarize: Set to 1 for values > 0.",
                "type": "string",
                "oneOf": [
                    {   "const": "normalize", 
                        "title": "Normalize",
                        "description": "Indicating if the conformed cells should be normalized, i.e. set to a value 0-1 based on cell-wise minimum and maximum values, i.e. (value - min)/(max - min)."
                    },
                    {   "const": "binarize", 
                        "title": "Binarize",
                        "description": "Indicating if the combined cells should be binarized, i.e. set to 1 for values > 0."
                    }
                ]
            },
            "template": {
                "title": "Spatial template",
                "description": "A raster representing the spatial layer to conform all input to.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    },
                    "accepts": {
                        "readOnly": true,
                        "title": "Accepts",
                        "description": "Accepted data type identifiers",
                        "type": "array",
                        "items": {
                            "type": "string"
                        },
                        "uniqueItems": true,
                        "default": [ "mimetype:image/geotiff" ]
                    }
                },
                "required": [
                    "uuid"
                ],
                "dependencies": {
                    "uuid": [
                      "layers",
                      "source_type"
                    ]
                }
            },
            "spatial":{
                "title": "A spatial layer",
                "description": "A raster representing the spatial layer to be conformed.",
                "type": "object",
                "properties": {
                    "uuid": {
                        "title": "dataset/resultset id",
                        "description": "UUID of dataset/resultset",
                        "type": "string",
                        "pattern": "^[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[89ab][a-f0-9]{3}-[a-f0-9]{12}$"
                    },
                    "layers": {
                        "title": "layers",
                        "description": "List of layer names",
                        "type": "array",
                        "items": {
                            "description": "Layer name",
                            "type": "string"
                        },
                        "minItems": 1,
                        "uniqueItems": true
                    },
                    "source_type": {
                        "title": "source type",
                        "description": "Type of input source",
                        "type": "string",
                        "enum": [
                            "dataset",
                            "resultset"
                        ],
                        "default": "dataset"
                    },
                    "accepts": {
                        "readOnly": true,
                        "title": "Accepts",
                        "description": "Accepted data type identifiers",
                        "type": "array",
                        "items": {
                            "type": "string"
                        },
                        "uniqueItems": true,
                        "default": [ "mimetype:image/geotiff" ]
                    }
                },
                "required": [
                    "uuid",
                    "layers",
                    "source_type"
                ]
            }
        },
        "required": [
            "operation",
            "template",
            "spatial"
        ]
    },
    "output": {
        "conformed_layer.tif": {
            "title": "Conformed Layer",
            "genre": "DataGenreSpatialRaster",
            "mimetype": "image/geotiff",
            "layer": "conformed_layer",
            "data_type": "Continuous"
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript"
        }
    }
}
