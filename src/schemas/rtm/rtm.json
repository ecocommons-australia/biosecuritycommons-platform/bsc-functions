{
    "name": "Real-Time Modelling (RTM)",
    "description": "A real-time forecasting framework for emerging infectious diseases affecting animal populations using Approximate Bayesian Computation (ABC)",
    "algorithm_category": "",
    "package_dependencies": "",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "$id": "https://schema.biosecuritycommons.org.au/rtm/rtm.json",
        "$attribution": "Meryl Theng, Christopher M. Baker, Simin Lee, Andrew Breed, Sharon Roche, Emily Sellens, Catherine Fraser, Christopher Jewell, Mark A. Stevenson, Simon M. Firestone, **A real-time forecasting framework for emerging infectious diseases affecting animal populations**, XX, 2024.\n\npreprint: xxx",
        "title": "Real-Time Modelling (RTM)",
        "description": "A real-time forecasting framework for emerging infectious diseases affecting animal populations using Approximate Bayesian Computation (ABC)",
        "type": "object",
        "required": [
            "environment",
            "scenario",
            "simulation"
        ],
        "properties": {
            "environment": {
                "required": [
                    "data",
                    "t_detect",
                    "t_today",
                    "t_lag",
                    "t_max"
                ],
                "type": "object",
                "properties": {
                    "data" : {          
                        "allOf": [
                            {
                                "$ref": "https://schema.biosecuritycommons.org.au/dataset.json#definitions/dataset_spatial_point"
                            },
                            {
                                "title": "Location data",
                                "description": "Location data (CSV)",
                                "required": ["uuid"],
                                "properties": {
                                    "fields": {
                                        "type": "object",
                                        "required": ["id", "xcoord", "ycoord", "area", "total"],
                                        "properties": {
                                            "id": {
                                                "description": "Numeric identifier",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "integer"
                                                },
                                                "default": []
                                            },
                                            "status": {
                                                "description": "Status or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "string"
                                                },
                                                "default": []
                                            },
                                            "ip": {
                                                "description": "IP or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["number", "string", "null"]
                                                },
                                                "default": []
                                            },
                                            "exposure_date": {
                                                "description": "Exposure date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "clinical_date": {
                                                "description": "Clinical date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "notification_date": {
                                                "description": "Notification date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "removal_date": {
                                                "description": "Removal date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "recovery_date": {
                                                "description": "Recovery date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "string"
                                                },
                                                "default": []
                                            },
                                            "vacc_date": {
                                                "description": "Vacc date or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "region": {
                                                "description": "Region or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "county": {
                                                "description": "County or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "cluster": {
                                                "description": "Cluster or NA",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "xcoord": {
                                                "description": "X coordinate",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "number"
                                                },
                                                "default": []
                                            },
                                            "ycoord": {
                                                "description": "Y coordinate",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "number"
                                                },
                                                "default": []
                                            },
                                            "area": {
                                                "description": "Area",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "number"
                                                },
                                                "default": []
                                            },
                                            "type": {
                                                "description": "Type",
                                                "type": ["array"],
                                                "items": {
                                                    "type": ["string", "null"]
                                                },
                                                "default": []
                                            },
                                            "total": {
                                                "description": "Total",
                                                "type": ["array"],
                                                "items": {
                                                    "type": "number"
                                                },
                                                "default": []
                                            }
                                        }
                                    }
                                }
                            }
                        ]
                    },
                    "clus" : {          
                        "type": "string",
                        "title": "Cluster",
                        "description": "Optional subset (use only the 'cluster' column specified in the input data)",
                        "maxLength": 255
                    },
                    "assumptions": {
                        "title": "Assumptions",
                        "description": "Assumptions about disease periods",
                        "type": "object",
                        "properties": {
                            "incubation_eq": {
                                "title": "Incubation EQ",
                                "type": "string",
                                "default": "rgamma(n=n(), shape=4, rate=2)"
                            },
                            "latent_eq": {
                                "title": "Latent EQ",
                                "type": "number",
                                "default": 1
                            },
                            "infectios_eq": {
                                "title": "Infectios EQ",
                                "type": "number",
                                "default": 6
                            }
                        }
                    },
                    "t_detect": {
                        "title": "T Detect",
                        "description": "Day of first detection/notification (indexed in days)",
                        "type": "integer",
                        "minimum": 1
                    },
                    "t_today": {
                        "title": "T Today",
                        "description": "Current time/Time of observation/forecasting (indexed in days)",
                        "type": "integer"
                    },
                    "t_lag": {
                        "title": "T Lag",
                        "description": "Number of days before the current data date to fit model up to",
                        "type": "integer",
                        "exclusiveMinimum": 0
                    },
                    "t_max": {
                        "title": "T Max",
                        "description": "Last day of the outbreak (i.e., to forecast up to; indexed in days)",
                        "type": "integer"
                    }
                }
            },
            "scenario": {
                "required": [
                    "scenario",
                    "paras_index",
                    "im_low",
                    "im_upp",
                    "paras_fixed",
                    "fixed_vals",
                    "dt_length"
                ],
                "type": "object",
                "properties": {
                    "scenario" : {          
                        "type": "integer",
                        "title": "Scenario",
                        "description": "Which model to use?",
                        "enum": [
                            10, 20
                        ],
                        "enumNames": [
                            "EI", "FMD"
                        ],
                        "default": 10
                    },
                    "paras_index" : {          
                        "type": "array",
                        "title": "Paras.index",
                        "description": "Vector index of unknown parameters (i.e., to be inferred; see reference table)",
                        "items": { 
                            "$ref": "#/input/definitions/indexed_params"
                        }
                    },
                    "im_low" : {          
                        "type": "array",
                        "title": "Im.low",
                        "description": "Lower bound for uniform parameter priors (corresponding to paras.index)",
                        "items": { 
                            "type": "number"
                        }
                    },
                    "im_upp" : {          
                        "type": "array",
                        "title": "Im.upp",
                        "description": "Upper bound for uniform parameter priors (corresponding to paras.index)",
                        "items": { 
                            "type": "number"
                        }
                    },
                    "paras_fixed": {
                        "type": "array",
                        "title": "Paras fixed",
                        "description": "Vector index of fixed parameters (i.e., see reference table)",
                        "items": { 
                            "$ref": "#/input/definitions/indexed_params"
                        }
                    },
                    "fixed_vals": {
                        "title": "Fixed vals",
                        "description": "Value for fixed parameters (corresponding to paras.fixed)",
                        "type": "array",
                        "items": { 
                            "type": "number"
                        }
                    },
                    "dt_length" : {          
                        "type": "integer",
                        "title": "Dt.length",
                        "description": "Intra-premises simulation length (in days)"
                    }
                },
                "allOf": [
                    {
                        "if": {
                            "properties": { "scenario": { "const": 10 }},
                            "required": [ "scenario" ]
                        },
                        "then": {
                            "properties": {
                                "paras_index": {
                                    "default": [1,4,5,7,9,14,18]
                                },
                                "im_low": {
                                    "default": [1,0,0,0,-2,-2,0]
                                },
                                "im_upp": {
                                    "default": [20,0.02,0.02,20,2,2,1]
                                },
                                "paras_fixed": {
                                    "default": [2, 3, 6]
                                },
                                "fixed_vals": {
                                    "default": []
                                },
                                "dt_length" : {
                                    "default": 50
                                }
                            }
                        }
                    },
                    {
                        "if": {
                            "properties": { "scenario": { "const": 20 }},
                            "required": [ "scenario" ]
                        },
                        "then": {
                            "properties": {
                                "paras_index": {
                                    "default": []
                                },
                                "im_low": {
                                    "default": []
                                },
                                "im_upp": {
                                    "default": []
                                },
                                "paras_fixed": {
                                    "default": []
                                },
                                "fixed_vals": {
                                    "default": []
                                },
                                "dt_length" : {
                                    "default": 125
                                }
                            }
                        }
                    }
                ]
            },
            "simulation": {
                "required": [
                    "n_particles",
                    "generations",
                    "n_tasks",
                    "temporal_tolerance_daily",
                    "epsilon_spatial",
                    "cell_reso"
                ],
                "type": "object",
                "properties": {
                    "n_particles" : {          
                        "type": "integer",
                        "title": "Num Particles",
                        "description": "Number of particles to accept each generation",
                        "default": 1000,
                        "minimum": 20,
                        "maximum": 1000
                    },
                    "generations" : {          
                        "type": "integer",
                        "title": "Num Generations",
                        "description": "Number of ABC-SMC generations/steps",
                        "default": 8,
                        "minimum": 3,
                        "maximum": 10,
                        "enum": [
                            3,4,5,6,7,8,9,10
                        ]
                    },
                    "n_tasks" : {          
                        "type": "integer",
                        "title": "Num Tasks",
                        "description": "The overall compute resources allocated",
                        "default": 25,
                        "minimum": 10,
                        "maximum": 100,
                        "enum": [ 10, 25, 50, 100]
                    },
                    "temporal_tolerance_daily": {
                        "type": "array",
                        "title": "Temporal tolerence daily",
                        "description": "Temporal tolerance schedule corresponding to each generation (+/- daily case counts)",
                        "items": { 
                            "type": "number",
                            "exclusiveMinimum": 0
                        },
                        "default": [50, 40, 30, 20, 15, 10, 7.5, 6]
                    },
                    "epsilon_spatial": {
                        "type": "array",
                        "title": "Epsilon spatial",
                        "description": "Spatial tolerance schedule corresponding to each generation (correlation coefficient R)",
                        "items": { 
                            "type": "number",
                            "minimum": 0,
                            "maximum": 1
                        },
                        "default": [0.3, 0.4, 0.5, 0.6, 0.65, 0.675, 0.7, 0.725]
                    },
                    "cell_reso": {
                        "type": "integer",
                        "title": "Cell resolution",
                        "description": "Resolution for spatial target (number of grid cells to overlay onto outbreak extent, length-wise)",
                        "default": 20
                    }
                }
            }
        },
        "definitions": {
            "indexed_params": { 
                "type": "integer",
                "enum": [1,2,3,4,5,6,7,8,9,10,12,13,14,15,16,17,18],
                "enumNames":[
                    "beta_intra", "sigma_intra", "gamma_intra", "alpha", 
                    "beta0", "kernel_type", "psi", "rho0", "????", "zeta_1", "zeta_3", 
                    "chi", "xi", "xi_1", "xi_2", "xi_3", "theta"
                ]
            }
        }
    },
    "output": {
        "particle_output.csv": {
            "title": "Particle output",
            "genre": "DataGenreRTMParticles",
            "mimetype": "text/csv",
            "order": 1
        },
        "epicurve.png": {
            "title": "Epicurve (daily case counts)",
            "genre": "DataGenreRTMPlot",
            "mimetype": "image/png",
            "order": 2
        },
        "spatial_risk.png": {
            "title": "Spatial risk",
            "genre": "DataGenreRTMPlot",
            "mimetype": "image/png",
            "order": 3
        },
        "posteriors.png": {
            "title": "Posteriors",
            "genre": "DataGenreRTMPlot",
            "mimetype": "image/png",
            "order": 4
        },
        "abc_steps.png": {
            "title": "ABC steps",
            "genre": "DataGenreRTMPlot",
            "mimetype": "image/png",
            "order": 5
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r",
            "order": 20
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript",
            "order": 21
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json",
            "order": 22
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "application/json",
            "order": 23
        }
    }
}