{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "https://schema.biosecuritycommons.org.au/bsdesign/bsdesign_surv_multilevel.json",
    "$attribution": "Cannon, R. M. (2009). Inspecting and monitoring on a restricted budget - where best to look? *Preventive Veterinary Medicine*, 92(1–2), 163-174. doi:10.1016/j.prevetmed.2009.06.009\n\nKean, J. M., Burnip, G. M. & Pathan, A. (2015). Detection survey design for decision making dur-ing biosecurity incursions. In F. Jarrad, S. Low-Choy & K. Mengersen (eds.), Biosecurity surveillance: *Quantitative approaches* (pp. 238– 250). Wallingford, UK: CABI.",
    "title": "Multi Level Surveillance Design",
    "description": "Biosecurity Commons - Multi Level Surveillance Design Inputs",
    "type": "object",
    "required": [
        "method_name",
        "levels",
        "sample_type",
        "sample_sens",
        "lambda"
    ],
	"properties": {
        "method_name": { 
            "readOnly": true,
            "type": "string",
            "enum": ["multilevel"],
            "default": "multilevel"
        },
        "levels": {
            "title": "Levels",
            "description": "One or more multi stage levels, for the surveillance design.  The levels should be ordered from lowest to highest (e.g. leaves, trees, rows, orchards). Only the lowest level can utilize either continuous density-based or discrete sampling, higher levels can only utilize discrete sampling.  Note that more than five levels may not be computationally feasible",
            "type": "array",
            "items": {
                "type": "object",
                "required": [
                    "level",
                    "unit"
                ],
                "properties": {
                    "unit" : {          
                        "title": "Unit",
                        "description": "Unit",
                        "type": "string"       
                    },
                    "total_indiv": {
                        "title": "Total individual",
                        "type": ["null", "number"],
                        "description": "Total individual discrete sampling units (e.g. trees, traps) present at each division part (location, category, etc.) specified by divisions. Default is NULL",
                        "default": null
                    },
                    "sample_cost": {
                        "title": "Sample cost",
                        "type": ["null", "number"],
                        "description": "The cost of samples at each level. Default is 1. An attribute units may be used to specify the cost units (e.g. '$' or 'hours')",
                        "default": null  
                    }
                }
            }
        },
        "sample_type" : {
            "title": "Sample type",
            "description": "The type of sampling used",
            "type": "string",
            "enum": [
                "discrete",
                "continuous"
            ]
        },
        "sample_sens" : {
            "title": "Sample Sensitivity",
            "description": "A single sample sensitivity value for the lowest level of a multilevel/stage sampling.",
            "type": "number",
            "default": 1
        },
        "prevalence": {
            "title": "Prevalence",
            "type": ["null", "number"],
            "description": "Discrete sampling design prevalence values for each division part (location, category, etc.) specified by divisions. Note that this parameter may represent apparent prevalence (Cannon, 2009) when the 'sample_sens' is set to 1",
            "default": null
        },
        "design_dens": {
            "title": "Design Density",
            "type": ["null", "number"],
            "description": "A single design density value for when the lowest level of the multilevel sampling is continuous. Default is NULL",
            "default": null
        },
        "sample_area": {
            "title": "Sample area",
            "type": ["null", "number"],
            "description": "The area of a single sample when the lowest level for when the lowest level of the multilevel sampling is continuous. Note that when set to 1, the total number of samples will be equivalent to the total area sampled. Default is NULL",
            "default": null
        },
        "confidence": {
            "title": "Confidence",
            "type": ["null", "number"],
            "description": "The desired (minimum) system sensitivity or detection confidence of the surveillance design (e.g. 0.95). Default is NULL",
            "default": null
        }
    },
    "definitions": {
        "vector": {
            "type": ["array", "null"],
            "items": {
                "type": ["number", "string"]
            },
            "default": null
        }
    }
}
