{
    "name": "Resource Allocation - Design",
    "description": "Biosecurity Commons - Resource Allocation - Design",
    "algorithm_category": "bsmanage",
    "input": {
        "$schema": "http://json-schema.org/draft-07/schema#",
        "title": "Resource Allocation - Design",
        "$id": "https://schema.biosecuritycommons.org.au/bsmanage/design.json",
        "$attribution": "Baker, C. M., Bower, S., Tartaglia, E., Bode, M., Bower, H., & Pressey,R. L. (2018). Modelling the spread and control of cherry guava on LordHowe Island. \\emph{Biological Conservation}, 227, 252–258.\\doi{10.1016/j.biocon.2018.09.017}\n\nBradhurst, R., Spring, D., Stanaway, M., Milner, J., & Kompas, T. (2021).A generalised and scalable framework for modelling incursions,surveillance and control of plant and environmental pests.\\emph{Environmental Modelling & Software}, 139, N.PAG.\\doi{10.1016/j.envsoft.2021.105004}\n\nCacho, O. J., & Hester, S. M. (2022). Modelling biocontrol of invasiveinsects: An application to European Wasp (Vespula germanica) in Australia.\\emph{Ecological Modelling}, 467. \\doi{10.1016/j.ecolmodel.2022.109939}\n\nGarcía Adeva, J. J., Botha, J. H., & Reynolds, M. (2012). A simulationmodelling approach to forecast establishment and spread of Bactrocerafruit flies. \\emph{Ecological Modelling}, 227, 93–108.\\doi{10.1016/j.ecolmodel.2011.11.026}\n\nGormley, A. M., Holland, E. P., Barron, M. C., Anderson, D. P., & Nugent,G. (2016). A modelling framework for predicting the optimal balancebetween control and surveillance effort in the local eradication oftuberculosis in New Zealand wildlife.\\emph{Preventive Veterinary Medicine}, 125, 10–18.\\doi{10.1016/j.prevetmed.2016.01.007}\n\nKrug, R. M., Roura-Pascual, N., & Richardson, D. M. (2010). Clearing ofinvasive alien plants under different budget scenarios: using asimulation model to test efficiency. \\emph{Biological Invasions}, 12(12),4099–4112. \\doi{10.1007/s10530-010-9827-3}\n\nLustig, A., James, A., Anderson, D., & Plank, M. (2019). Pest control at aregional scale: Identifying key criteria using a spatially explicit,agent‐based model. \\emph{Journal of Applied Ecology}, 56(7 pp.1515–1527),1527–1515. \\doi{10.1111/1365-2664.13387}\n\nRout, T. M., Moore, J. L., & McCarthy, M. A. (2014). Prevent, search ordestroy? A partially observable model for invasive species management.\\emph{Journal of Applied Ecology}, 51(3), 804–813.\\doi{10.1111/1365-2664.12234}\n\nSpring, D., Croft, L., & Kompas, T. (2017). Look before you treat:increasing the cost effectiveness of eradication programs with aerialsurveillance. \\emph{Biological Invasions}, 19(2), 521.\\doi{10.1007/s10530-016-1292-1}\n\nWadsworth, R. A., Collingham, Y. C., Willis, S. G., Huntley, B., & Hulme,P. E. (2000). Simulating the Spread and Management of Alien RiparianWeeds: Are They Out of Control? \\emph{Journal of Applied Ecology}, 37,28–38. \\doi{10.1046/j.1365-2664.2000.00551.x}\n\nWarburton, B., & Gormley, A. M. (2015). Optimising the Application ofMultiple-Capture Traps for Invasive Species Management Using SpatialSimulation. \\emph{PLoS ONE}, 10(3), 1–14.\\doi{10.1371/journal.pone.0120373}\n\nZub, K., García-Díaz, P., Sankey, S., Eisler, R., & Lambin, X. (2022).Using a Modeling Approach to Inform Progress Towards Stoat EradicationFrom the Orkney Islands. \\emph{Frontiers in Conservation Science}, 2.\\doi{10.3389/fcosc.2021.780102}",
        "description": "Management design functionality for the effective allocation of management resources across one or more divisions (management responses, strategies, invasion pathways, invasive species, spatial locations, time, etc.) via methods that utilize management costs, savings, benefits, and/or overall management probability of success (or effectiveness)",
        "type": "object",
        "required": [
            "context",
            "method"
        ],
        "properties": {
            "context": {
                "$ref": "https://schema.biosecuritycommons.org.au/bsmanage/context.json"
            },
            "method": {
                "title": "Method",
                "type": "object",
                "properties": {},
                "oneOf": [
                    {
                        "$ref": "https://schema.biosecuritycommons.org.au/bsmanage/control_design.json"
                    },
                    {
                        "$ref": "https://schema.biosecuritycommons.org.au/bsmanage/method_other.json"
                    }
                ]
            }
        },
        "definitions" : {}
    },
    "output": {
        "bsmanage_design.tif": {
            "title": "Resource Allocation - Design",
            "genre": "DataGenreBSManageDesignSpatial",
            "mimetype": "image/geotiff",
            "layer": "bsmanage_design",
            "data_type": "Continuous",
            "order": 1
        },
        "*.R": {
            "title": "Job script",
            "genre": "JobScript",
            "mimetype": "text/x-r"
        },
        "*.Rout": {
            "title": "Log file",
            "genre": "DataGenreLog",
            "mimetype": "text/x-r-transcript"
        },
        "metadata.json": {
            "title": "Metadata",
            "genre": "DataGenreMetadata",
            "mimetype": "application/json"
        },
        "params.json": {
            "title": "Input parameters",
            "genre": "InputParams",
            "mimetype": "text/x-r-transcript"
        }
    }
}