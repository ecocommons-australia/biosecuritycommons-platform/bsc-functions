{
    "$schema": "http://json-schema.org/draft-07/schema#",
    "$id": "https://schema.biosecuritycommons.org.au/bsimpact/bsimpact_method_value.json",
    "title": "Impact Analysis Method Value",
    "description": "Represent quantitative impact analysis functionality for calculating and combining spatially-explicit value-based impacts of invasive species incursions across various assets of the environment, society, and/or economy.",
    "type": "object",
    "required": [
        "valuation_type",
        "asset_layers",
        "combine_function"
    ],
    "properties": {
        "valuation_type": { 
            "readOnly": true,
            "type": "string",
            "enum": ["monetary", "non-monetary"]
        },
        "asset_layers" : {
            "title": "Asset value layers",
            "description": "A list of spatial layers (raster) for each named asset value (mechanism, service, sector, asset type, etc.)",
            "type": "array",
            "items": { 
                "allOf": [
                    { "$ref": "https://schema.biosecuritycommons.org.au/dataset.json#definitions/dataset_spatial_raster" },
                    {
                        "properties": {
                            "name": { 
                                "title": "Asset name", "type": "string"
                            },
                            "loss_rate" : {
                                "title": "Loss rate",
                                "description": "Value loss rate for each named asset value (mechanism, service, sector, asset type, etc.)",
                                "type": "number",
                                "maximum": 1,
                                "minimum": 0
                            }
                        },
                        "required": [ "name", "loss_rate" ]
                    }
                ]
            },
            "minItems": 1
        },

        "combine_function" : {
            "title": "Combine function",
            "type": "string",
            "description": "The function used to combine value layers across assets of the environment, society, and/or economy. Set to 'none' when combining impacts is not applicable."
        },

        "mgmt_cost_unit": {
            "title": "Management cost unit",
            "description": "The unit of measure for management costs. This will typically be the same unit as 'Impact Measures'. One of '$' (default), 'hours', 'none'",
            "type": "string",
            "enum": [
                "$",
                "hours",
                "none"
            ]
        },

        "mgmt_costs" : {
            "allOf": [
                {
                    "$ref": "https://schema.biosecuritycommons.org.au/dataset.json#definitions/dataset_spatial_raster"
                },
                {
                    "title": "Management costs",
                    "description": "Optional spatial layer raster or vector of management costs at each location specified by the 'region', measured in the unit specified in the 'context'. Default is NULL"
                }
            ]   
        }
    },
    "allOf": [
        {
            "if": {
                "properties": { 
                    "valuation_type": { "const": "monetary" }
                }
            },
            "then": {
                "properties": { 
                    "combine_function": { 
                        "enum": [ "sum", "none" ],
                        "default": "sum"
                    },
                    "impact_measure": {
                        "title": "Impact measure",
                        "description": "Measure used to quantify or classify each asset, consistent with the valuation type. Monetary measure should specify the unit used (e.g. '$'). Default is '$'",
                        "type": "string",
                        "default": "$"
                    }
                }
            }
        },
        {
            "if": {
                "properties": { 
                    "valuation_type": { "const": "non-monetary" }
                }
            },
            "then": {
                "properties": { 
                    "combine_function": { 
                        "enum": [ "sum", "mean", "median", "max", "none" ],
                        "default": "none"
                    },
                    "asset_layers" : {
                        "type": "array",
                        "items": { 
                            "allOf": [
                                { "$ref": "https://schema.biosecuritycommons.org.au/dataset.json#definitions/dataset_spatial_raster" },
                                {
                                    "properties": {
                                        "name": { 
                                            "type": "string", "description": "Asset name"
                                        },
                                        "loss_rate" : {
                                            "title": "Loss rates",
                                            "description": "Value loss rate for each named asset value (mechanism, service, sector, asset type, etc.)",
                                            "type": "number",
                                            "maximum": 1,
                                            "minimum": 0
                                        },
                                        "impact_measure": {
                                            "title": "Impact measure",
                                            "description": "Measure used to quantify or classify each asset, consistent with the valuation type. Non-monetary (quantitative) measures should specify the unit used.",
                                            "type": "string"
                                        }
                                    },
                                    "required": [ "name", "loss_rate" ]
                                }
                            ]
                        },
                        "minItems": 1
                    }
                }
            }
        }
    ] 
}