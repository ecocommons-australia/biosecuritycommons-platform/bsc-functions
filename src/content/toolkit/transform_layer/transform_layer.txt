{
    "input": {
        "spatial": {
            "type": "dataset_layer",
            "plural": "single",
            "resultset_files": {
                "titles": ["genre:DataGenreSpatialRaster"],
                "search_op": "ANY"
            }
        }
    },
    "script": [
        "{function}/{function}.R"
    ],
    "metadata": {
        "Citation": "default"
    },
    "output": {
        "files": {
            "transformed.tif": {
                "title": "Transformed layer",
                "genre": "DataGenreSpatialRaster",
                "mimetype": "image/geotiff",
                "layer": "transformed",
                "data_type": "Continuous",
                "order": 1
            },
            "*.R": {
                "title": "Job script",
                "genre": "JobScript",
                "mimetype": "text/x-r",
                "order": 10
            },
            "*.Rout": {
                "title": "Log file",
                "genre": "DataGenreLog",
                "mimetype": "text/x-r-transcript",
                "order": 11
            },
            "metadata.json": {
                "title": "Metadata",
                "genre": "DataGenreMetadata",
                "mimetype": "application/json",
                "order": 12
            },
            "params.json": {
                "title": "Input parameters",
                "genre": "InputParams",
                "mimetype": "text/x-r-transcript",
                "order": 15
            }
        }
    }
}
