## Platform wrapper for CoordinateCleaner
##
## https://CRAN.R-project.org/package=CoordinateCleaner
## 

library(CoordinateCleaner)
paste("CoordinateCleaner:", packageVersion("CoordinateCleaner"))

# Load parameters
params = rjson::fromJSON(file="params.json")
input.params <- params$params
input.env <- params$env
rm(params)

# Set working directory
setwd(input.env$workdir)

# Point data as a data.frame (or matrix)
# with WGS84 lon and lat columns
coords_df <- utils::read.csv(input.params$species_occurrence_dataset$filename)

tests = c(
   "capitals", "centroids", "equal", "gbif",
   "institutions", "zeros", "seas"
)
if (!is.null(input.params$tests)){
	tests = input.params$tests
} else {
	print('Using default tests: ["capitals", "centroids", "equal", "gbif", "institutions", "zeros", "seas]')
}

capitals_rad = 5000
if (!is.null(input.params$capitals_rad)){
	capitals_rad = input.params$capitals_rad
} else {
	print('Using default capitals_rad: 5000')
}

centroids_rad = 5000
if (!is.null(input.params$centroids_rad)){
	centroids_rad = input.params$centroids_rad
} else {
	print('Using default centroids_rad: 5000')
}

inst_rad = 100
if (!is.null(input.params$inst_rad)){
	inst_rad = input.params$inst_rad
} else {
	print('Using default inst_rad: 100')
}

zeros_rad = 0.5
if (!is.null(input.params$zeros_rad)){
	zeros_rad = input.params$zeros_rad
} else {
	print('Using default zeros_rad: 0.5')
}

seas_scale = 50
if (!is.null(input.params$seas_scale)){
	seas_scale = input.params$seas_scale
} else {
	print('Using default seas_scale: 50')
}

lon_col = NULL
if ('decimalLongitude' %in% names(coords_df)) { 
   lon_col = 'decimalLongitude'
} else if ('longitude' %in% names(coords_df)) { 
   lon_col = 'longitude'
} else if ('lon' %in% names(coords_df)) { 
   lon_col = 'lon'
} else if ('x' %in% names(coords_df)) { 
   lon_col = 'x'
} 

lat_col = NULL
if ('decimalLatitude' %in% names(coords_df)) { 
   lat_col = 'decimalLatitude'
} else if ('latitude' %in% names(coords_df)) { 
   lat_col = 'latitude'
} else if ('lat' %in% names(coords_df)) { 
   lat_col = 'lat'
} else if ('y' %in% names(coords_df)) { 
   lat_col = 'y'
} 

species_col = NULL
if ('species' %in% names(coords_df)) { 
   print("Using species id column (species)")
   species_col = 'species'
} else if('verified' %in% names(coords_df)){
   print("Using species id column (verified)")
   species_col = 'verified'
}

# if ('countryCode' %in% names(coords_df)) { 
#    print("Using countryCode column (countryCode)")
# } else if('verified' %in% names(coords_df)){
#  print("No countryCode column found, cleaning without country")
# }

cleaned_df <- CoordinateCleaner::clean_coordinates(
    x = coords_df, 
    lon = lon_col, 
    lat = lat_col, 
    tests = tests,
    species = species_col,
    capitals_rad = capitals_rad, 
    centroids_rad = centroids_rad, 
    inst_rad = inst_rad, 
    zeros_rad = zeros_rad,
    value = "clean",
    verbose = FALSE,
    seas_scale = seas_scale
  )

# Set working directory to output
setwd(input.env$outputdir)

filename = 'occurrences_cleaned.csv'
utils::write.csv(cleaned_df, filename, row.names = TRUE)
