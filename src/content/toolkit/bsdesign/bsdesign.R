## Platform wrapper for bsdesign package functions
##
## https://github.com/cebra-analytics/bsdesign/
## 

library(bsdesign)
paste("bsdesign:", packageVersion("bsdesign"))
paste("terra:", packageVersion("terra"))

# Load parameters
params = rjson::fromJSON(file="params.json")
input.params <- params$params
input.env <- params$env
rm(params)

# Set terra tmpdir to job scratch space
terra::terraOptions(tempdir = input.env$workdir)

# Set working directory to output
setwd(input.env$outputdir)

# Common get parameter method
get_param <- function(params, name, divisions) {
    if (is.null(params[[name]]$type) || params[[name]]$type == "spatial") {
        switch(params$study_type,
            raster = {
                param_rast <- get_rast(params, name)
                if (!is.null(param_rast)) {
                    param_rast <- conform_layer(param_rast, divisions$get_template())
                    if (terra::nlyr(param_rast) > 1) {
                        if (length(unique(names(param_rast))) < terra::nlyr(param_rast)) {
                            names(param_rast) <- paste0(names(param_rast), 
                                                        1:terra::nlyr(param_rast))
                        }
                        param_series <- lapply(names(param_rast), function(n) {
                            param_rast[n][divisions$get_indices()][,1]
                        })
                        names(param_series) <- names(param_rast)
                        param_series
                    } else {
                        param_rast[divisions$get_indices()][,1]
                    }
                } else {
                    NULL
                }
            },
            point = {
                if (!is.null(params[[name]]$csv) && params[[name]]$csv) {
                    param_df <- divisions$get_data()
                } else {
                    param_df <- get_csv(params, name)
                    if (is.data.frame(param_df) && all(c("lon", "lat") %in% names(param_df))) {
                        region_idx <- order(divisions$get_data()[,"lon"], divisions$get_data()[,"lat"])
                        param_idx <- order(param_df[,"lon"], param_df[,"lat"])
                        param_df <- param_df[param_idx[order(region_idx)],]
                    }
                }
                get_df_col(param_df, name)
            },
            aspatial = {
                if (!is.null(params[[name]]$csv) && params[[name]]$csv) {
                    param_df <- divisions$get_data()
                } else {
                    param_df <- get_csv(params, name)
                    if (is.data.frame(param_df) && "id" %in% names(param_df)) {
                        param_df <- param_df[order(param_df[,"id"]),]
                    }
                }
                get_df_col(param_df, name)
            }
        )
    } else if (params[[name]]$type == "single") {
        params[[name]]$value
    }
}

# Discrete sampling method
method_discrete_sampling <- function(params, context) {

    # Region/divisions
    if (!is.null(params$study_type)) {
        region <- switch(params$study_type,
            raster = get_rast(params, 'region'),
            point = {
                get_csv(params, 'region')
            },
            aspatial = {
                region_df <- get_csv(params, 'region')
                if (is.data.frame(region_df) && "id" %in% names(region_df)) {
                    region_df <- region_df[order(region_df[,"id"]),]
                }
                region_df
            }
        )
    } else if (!is.null(params$csv)) { # retrofit old CSV points version
        region = get_csv(params, 'csv')
        params$study_type <- "point"
    }
    divisions <- bsdesign::Divisions(region)

    # Sample sensitivity
    sample_sens <- get_param(params, "sample_sens", divisions)

    # Proportion sampled few (<= 10%) or many (> 10%)?
    if (params$sampling_type == "many") {
        
        # Total individuals
        total_indiv <- get_param(params, "total_indiv", divisions)

    } else {
        total_indiv <- NULL
    }

    # Prevalence
    prevalence <- get_param(params, "prevalence", divisions)

    # Establishment probability
    establish_pr <- get_param(params, "establish_pr", divisions)
    if (is.logical(params$establish_pr$relative) && params$establish_pr$relative) {
        attr(establish_pr, "relative") <- TRUE
    }
    
    # Optimisation or existing?
    if (!is.null(params$alloc_design) && params$alloc_design == "existing") {
      params$optimal <- "none"
    }
    
    # Optimal params
    if (params$optimal == "cost") {
        mgmt_cost <- params$mgmt_cost
        benefit <- NULL
        exist_alloc <- NULL
    } else if (params$optimal == "saving") {
        mgmt_cost <- list()
        benefit <- get_param(params, "saving", divisions)
        exist_alloc <- NULL
    } else if (params$optimal == "benefit") {
        mgmt_cost <- list()
        benefit <- get_param(params, "benefit", divisions)
        exist_alloc <- NULL
    } else if (params$optimal == "detection") {
        mgmt_cost <- list()
        benefit <- NULL
        exist_alloc <- NULL
    } else if (params$optimal == "none") {
        mgmt_cost <- list()
        benefit <- NULL
        exist_alloc <- get_param(params, "exist_alloc", divisions)
    }
    
    # Other cost params
    sample_cost <- params$sample_cost
    if (!is.null(params$fixed_cost)) {
        fixed_cost <- get_param(params, "fixed_cost", divisions)
    } else {
        fixed_cost <- NULL
    }

    # Existing sensitivity
    if (!is.null(params$exist_sens)) {
        exist_sens <- get_param(params, "exist_sens", divisions)
    } else {
        exist_sens <- NULL
    }
    
    # Constraint
    if (!is.null(params$constraint) && params$constraint == "budget") {
      budget <- params$budget
      confidence <- NULL
    } else if (!is.null(params$constraint) && params$constraint == "confidence") {
      budget <- NULL
      confidence <- params$confidence
    } else {
      budget <- NULL
      confidence <- NULL
    }
    
    # Discrete and minimum allocations
    if (!is.null(params$discrete_alloc)) {
        discrete_alloc <- as.logical(params$discrete_alloc)
    } else {
        discrete_alloc <- FALSE
    }
    min_alloc <- params$min_alloc
    
    # Existing allocation time-series?
    if (params$optimal == "none" && is.list(exist_alloc)) {
      time_series <- names(exist_alloc)
    } else {
      time_series <- 1
      exist_alloc <- list(exist_alloc)
    }
    
    # Sampling design
    design <- lapply(time_series, function(ts) {
        bsdesign::SamplingSurvDesign(
            context = context,
            divisions = divisions,
            establish_pr = establish_pr,
            sample_sens = sample_sens,
            sample_type = "discrete",
            prevalence = prevalence,
            total_indiv = total_indiv,
            sample_area = NULL,
            optimal = params$optimal,
            mgmt_cost = mgmt_cost,
            benefit = benefit,
            sample_cost = sample_cost,
            fixed_cost = fixed_cost,
            budget = budget,
            confidence = confidence,
            min_alloc = min_alloc,
            discrete_alloc = discrete_alloc,
            exist_alloc = exist_alloc[[ts]],
            exist_sens = exist_sens
        )
    })

    # Existing allocation time-series?
    if (params$optimal == "none" && is.list(exist_alloc)) {
      
      # Get and save design sensitivities for each time interval
      names(design) <- time_series
      if (params$study_type == "point") {
          sensitivity_df <- divisions$get_coords(extra_cols = TRUE)
      } else if (params$study_type == "aspatial") {
          sensitivity_df <- divisions$get_data()
      }
      confidence_df <- data.frame(interval = 1:length(time_series),
                                  pr_detect = 0)
      rownames(confidence_df) <- time_series
      for (i in 1:length(time_series)) {
          sensitivity <- design[[i]]$get_sensitivity()
          if (params$study_type == "raster") {
              terra::writeRaster(
                  divisions$get_rast(sensitivity),
                  file = sprintf("sensitivity_%s.tif", i),
                  gdal = c("COMPRESS=LZW", "TILED=YES"),
                  overwrite = TRUE)
          } else if (params$study_type %in% c("point", "aspatial")) {
              sensitivity_df[[sprintf("sensitivity_%s", i)]] <- sensitivity
          }
          confidence_df$pr_detect[i] <- design[[i]]$get_confidence()
      }
      if (params$study_type %in% c("point", "aspatial")) {
          write.csv(sensitivity_df, file = "design.csv", row.names = FALSE)
      }
      write.csv(confidence_df, file = "system_sens.csv", row.names = FALSE)
      
    } else {
        
        # Get and save design allocations & sensitivities
        design <- design[[1]]
        allocation <- design$get_allocation()
        sensitivity <- design$get_sensitivity()
        if (params$study_type == "raster") {
            design$save_design(
                gdal = c("COMPRESS=LZW", "TILED=YES"),
                overwrite = TRUE
            )
        } else {
            design$save_design()
        }
        write.csv(data.frame(interval = 1, pr_detect = design$get_confidence()), 
                  file = "system_sens.csv", row.names = FALSE)
    }
}

# Legacy (<2025)
method_discrete_sampling_legacy <- function(params, context) {
    csv = get_csv(params, 'csv')

    if (name_exists(csv, 'mgmt_cost_detected') &&
        name_exists(csv, 'mgmt_cost_undetected')
    ){
        mgmt_cost <- list(
            detected = get_df_col(csv, 'mgmt_cost_detected'),
            undetected = get_df_col(csv, 'mgmt_cost_undetected')
        ) 
    } else {
        print(paste("method_sampling: mgmt_cost=NULL"))
        mgmt_cost = list()
    }

    if (name_exists(csv, 'sample_sens')){
        sample_sens = get_df_col(csv, 'sample_sens')
    } else {
        print(paste("method_sampling: WARNING sample_sens not provided. Falling back to package default of 1"))
        sample_sens = 1
    }
    
    divisions <- bsdesign::Divisions(csv['region_id'])

    design <- bsdesign::SamplingSurvDesign(
        context = context,
        divisions = divisions,
        establish_pr = get_df_col(csv, 'establish_pr'),
        sample_sens = sample_sens,
        sample_type = params$sample_type,
        prevalence = get_df_col(csv, 'prevalence'),
        total_indiv = get_df_col(csv, 'total_indiv'),
        design_dens = get_df_col(csv, 'design_dens'),
        sample_area = params$sample_area,
        optimal = params$optimal,
        mgmt_cost = mgmt_cost,
        benefit = get_df_col(csv, 'benefit'),
        sample_cost = get_df_col(csv, 'sample_cost'),
        fixed_cost = get_df_col(csv, 'fixed_cost'),
        budget = params$budget,
        confidence = params$confidence
    )

    design$get_allocation()
    design$get_sensitivity()
    design$get_confidence()
    design$save_design()
}

# Multi-level sampling method
method_multilevel_sampling <- function(params, context) {
    levels <- 1:length(params$levels)

    units = c()
    total_indiv = c()
    for (v in params$levels) {
        units <- append(units, v$unit)
        total_indiv <- append(total_indiv, v$total_indiv)
    }

    print(levels)
    print(units)
    print(total_indiv)

    divisions <- bsdesign::Divisions(
        data.frame(level = levels,
                   unit = units)
        )

    design <- bsdesign::MultilevelSurvDesign(
        context = context,
        divisions = divisions,
        sample_sens = params$sample_sens,
        sample_type = params$sample_type,
        prevalence = params$prevalence,
        total_indiv = total_indiv,
        design_dens = params$design_dens,
        sample_area = params$sample_area,
        sample_cost = params$sample_cost,
        confidence = params$confidence
    )

    design$get_allocation()
    design$get_sensitivity()
    design$save_design()
}

# Continuous surveillance method
method_continuous_surveillance <- function(params, context) {

    # Region/divisions
    region <- switch(params$study_type,
        raster = get_rast(params, 'region'),
        point = {
            get_csv(params, 'region')
        },
        aspatial = {
            region_df <- get_csv(params, 'region')
            if (is.data.frame(region_df) && "id" %in% names(region_df)) {
                region_df <- region_df[order(region_df[,"id"]),]
            }
            region_df
        }
    )
    divisions <- bsdesign::Divisions(region)

    # Continuous type determines sensitivity parameters
    if (params$continuous_type == "sampling") {
      
        # Sample sensitivity
        sample_sens <- get_param(params, "sample_sens", divisions)

        # Sample area
        sample_area <- params$sample_area
      
        # Design density
        design_dens <- get_param(params, "design_dens", divisions)

    } else if (params$continuous_type == "effort") {
        
        # Efficacy/lambda
        lambda <- get_param(params, "lambda", divisions)
    }
    
    # Establishment probability
    establish_pr <- get_param(params, "establish_pr", divisions)
    if (is.logical(params$establish_pr$relative) && params$establish_pr$relative) {
        attr(establish_pr, "relative") <- TRUE
    }
    
    # Optimisation or existing?
    if (!is.null(params$alloc_design) && params$alloc_design == "existing") {
        params$optimal <- "none"
    }
    
    # Optimal params
    if (params$optimal == "cost") {
        mgmt_cost <- params$mgmt_cost
        benefit <- NULL
        exist_alloc <- NULL
    } else if (params$optimal == "saving") {
        mgmt_cost <- list()
        benefit <- get_param(params, "saving", divisions)
        exist_alloc <- NULL
    } else if (params$optimal == "benefit") {
        mgmt_cost <- list()
        benefit <- get_param(params, "benefit", divisions)
        exist_alloc <- NULL
    } else if (params$optimal == "detection") {
        mgmt_cost <- list()
        benefit <- NULL
        exist_alloc <- NULL
    } else if (params$optimal == "none") {
        mgmt_cost <- list()
        benefit <- NULL
        exist_alloc <- get_param(params, "exist_alloc", divisions)
    }
    
    # Other cost params
    alloc_cost <- params$alloc_cost
    sample_cost <- params$sample_cost
    if (!is.null(params$fixed_cost)) {
        fixed_cost <- get_param(params, "fixed_cost", divisions)
    } else {
        fixed_cost <- NULL
    }

    # Existing sensitivity
    if (!is.null(params$exist_sens)) {
        exist_sens <- get_param(params, "exist_sens", divisions)
    } else {
        exist_sens <- NULL
    }
    
    # Constraint
    if (!is.null(params$constraint) && params$constraint == "budget") {
        budget <- params$budget
        confidence <- NULL
    } else if (!is.null(params$constraint) && params$constraint == "confidence") {
        budget <- NULL
        confidence <- params$confidence
    } else {
        budget <- NULL
        confidence <- NULL
    }
      
    # Discrete and minimum allocations
    if (!is.null(params$discrete_alloc)) {
        discrete_alloc <- as.logical(params$discrete_alloc)
    } else {
        discrete_alloc <- FALSE
    }
    min_alloc <- params$min_alloc
    
    # Existing allocation time-series?
    if (params$optimal == "none" && is.list(exist_alloc)) {
        time_series <- names(exist_alloc)
    } else {
        time_series <- 1
        exist_alloc <- list(exist_alloc)
    }
    
    # Continuous type determines design model
    if (params$continuous_type == "sampling") {
      
        # Sampling design
        design <- lapply(time_series, function(ts) {
            bsdesign::SamplingSurvDesign(
                context = context,
                divisions = divisions,
                establish_pr = establish_pr,
                sample_sens = sample_sens,
                sample_type = "continuous",
                design_dens = design_dens,
                sample_area = sample_area,
                optimal = params$optimal,
                mgmt_cost = mgmt_cost,
                benefit = benefit,
                sample_cost = sample_cost,
                fixed_cost = fixed_cost,
                budget = budget,
                confidence = confidence,
                min_alloc = min_alloc,
                discrete_alloc = discrete_alloc,
                exist_alloc = exist_alloc[[ts]],
                exist_sens = exist_sens
            )
        })
      
    } else if (params$continuous_type == "effort") {
      
        # Spatial design
        design <- lapply(time_series, function(ts) {
            bsdesign::SpatialSurvDesign(
                context = context,
                divisions = divisions,
                establish_pr = establish_pr,
                lambda = lambda,
                prevalence = params$prevalence,
                optimal = params$optimal,
                mgmt_cost = mgmt_cost,
                benefit = benefit,
                alloc_cost = alloc_cost,
                fixed_cost = fixed_cost,
                budget = budget,
                confidence = confidence,
                min_alloc = min_alloc,
                discrete_alloc = discrete_alloc,
                exist_alloc = exist_alloc[[ts]],
                exist_sens = exist_sens
            )
        })
    }
    
    # Existing allocation time-series?
    if (params$optimal == "none" && is.list(exist_alloc)) {
      
      # Get and save design sensitivities for each time interval
      names(design) <- time_series
      if (params$study_type == "point") {
          sensitivity_df <- divisions$get_coords(extra_cols = TRUE)
      } else if (params$study_type == "aspatial") {
          sensitivity_df <- divisions$get_data()
      }
      confidence_df <- data.frame(interval = 1:length(time_series),
                                  pr_detect = 0)
      rownames(confidence_df) <- time_series
      for (i in 1:length(time_series)) {
          sensitivity <- design[[i]]$get_sensitivity()
          if (params$study_type == "raster") {
              terra::writeRaster(
                  divisions$get_rast(sensitivity),
                  file = sprintf("sensitivity_%s.tif", i),
                  gdal = c("COMPRESS=LZW", "TILED=YES"),
                  overwrite = TRUE)
          } else if (params$study_type %in% c("point", "aspatial")) {
              sensitivity_df[[sprintf("sensitivity_%s", i)]] <- sensitivity
          }
          confidence_df$pr_detect[i] <- design[[i]]$get_confidence()
      }
      if (params$study_type %in% c("point", "aspatial")) {
          write.csv(sensitivity_df, file = "design.csv", row.names = FALSE)
      }
      write.csv(confidence_df, file = "system_sens.csv", row.names = FALSE)
      
    } else {
        
        # Get and save design allocations & sensitivities
        design <- design[[1]]
        allocation <- design$get_allocation()
        sensitivity <- design$get_sensitivity()
        if (params$study_type == "raster") {
            design$save_design(
                gdal = c("COMPRESS=LZW", "TILED=YES"),
                overwrite = TRUE
            )
        } else {
            design$save_design()
        }
        write.csv(data.frame(interval = 1, pr_detect = design$get_confidence()), 
                  file = "system_sens.csv", row.names = FALSE)
    }
}

# Legacy (<2025)
method_spatial_rast_legacy <- function(params, context) {
    region_rast         <- get_rast(params, 'region')
    divisions           <- bsdesign::Divisions(region_rast)

    alloc_cost_rast     <- get_rast(params, 'alloc_cost')
    establish_pr_rast   <- get_rast(params, 'establish_pr')
    benefit_rast        <- get_rast(params, 'benefit')
    fixed_cost_rast     <- get_rast(params, 'fixed_cost')
    
    alloc_cost      <- conform_layer(alloc_cost_rast,    region_rast)
    establish_pr    <- conform_layer(establish_pr_rast,  region_rast)[divisions$get_indices()][,1]
    benefit         <- conform_layer(benefit_rast,       region_rast)[divisions$get_indices()][,1]
    fixed_cost      <- conform_layer(fixed_cost_rast,    region_rast)[divisions$get_indices()][,1]

    if (is.numeric(params$lambda)){
        lambda <- params$lambda
    } else {
        lambda_rast <- get_rast(params, 'lambda')
        lambda <- conform_layer(lambda_rast, region_rast)[divisions$get_indices()][,1]
    }

    mgmt_cost = params$mgmt_cost
    if (is.null(mgmt_cost)){
        mgmt_cost <- list()
    }
    
    if(! is.null(params$discrete_alloc)) {
        discrete_alloc = as.logical(params$discrete_alloc)
    } else {
        discrete_alloc = FALSE
    }

    design = bsdesign::SpatialSurvDesign(
        context = context,
        divisions = divisions,
        establish_pr = establish_pr,
        lambda = lambda,
        prevalence = params$prevalence,
        optimal = params$optimal,
        mgmt_cost = mgmt_cost,
        benefit = benefit,
        alloc_cost = alloc_cost,
        fixed_cost = fixed_cost,
        budget = params$budget,
        min_alloc = params$min_alloc,
        discrete_alloc = discrete_alloc,
        confidence = params$confidence
    )

    design$get_allocation()
    design$save_design(
        datatype = ifelse(discrete_alloc, "INT4S", "FLT4S"),
        gdal = c("COMPRESS=LZW", "TILED=YES"),
        overwrite = TRUE
    )
}

# Legacy (<2025)
method_spatial_point_legacy <- function(params, context) {
    region          <- get_csv(params, 'region')
    divisions       <- bsdesign::Divisions(region)

    establish_pr    <- get_csv(params, 'establish_pr')
    benefit         <- get_csv(params, 'benefit')
    alloc_cost      <- get_csv(params, 'alloc_cost')
    fixed_cost      <- get_csv(params, 'fixed_cost')
    lambda          <- get_csv(params, 'lambda')

    design = bsdesign::SpatialSurvDesign(
        context = context,
        divisions = divisions,
        establish_pr = establish_pr,
        lambda = lambda,
        prevalence = params$prevalence,
        optimal = params$optimal,
        mgmt_cost = params$mgmt_cost,
        benefit = benefit,
        alloc_cost = alloc_cost,
        fixed_cost = fixed_cost,
        budget = params$budget,
        confidence = params$confidence
    )

    design$get_allocation()
    design$get_sensitivity()
    design$save_design()
}

# Device range surveillance method
method_device_range_surveillance <- function(params, context) {
    region <- get_rast(params, 'region')
    divisions <- bsdesign::Divisions(region)

    establish_pr_rast <- get_rast(params, 'establish_pr')
    establish_pr <- conform_layer(establish_pr_rast, region)[divisions$get_indices()][,1]
    
    # Is establishment probability relative?
    if (is.logical(params$establish_pr_relative) && params$establish_pr_relative) {
      attr(establish_pr, "relative") <- TRUE
    }
    
    design = bsdesign::RangeKernelSurvDesign(
        context = context,
        divisions = divisions,
        establish_pr = establish_pr,
        lambda = params$lambda,
        sigma = params$sigma,
        intervals = params$intervals,
        prevalence = params$prevalence,
        optimal = params$optimal,
        budget = params$budget,
        confidence = params$confidence
        #exist_alloc = exist_alloc,
        #exist_sens = exist_sens,
    )

    design$get_allocation()
    design$save_design(
        gdal = c("COMPRESS=LZW", "TILED=YES"),
        overwrite = TRUE
    )
}

# Area growth method
method_areagrowth <- function(params, context) {
    r = get_csv(params, 'region')

    divisions <- bsdesign::Divisions(get_df_col(r, 'subregion'))
    
    design = bsdesign::AreaGrowthSurvDesign(
        context = context,
        divisions = divisions,
        subregion_area = get_df_col(r, 'subregion_area'),
        establish_rate = get_df_col(r, 'establish_rate'),
        sample_sens = get_df_col(r, 'sample_sens'),
        growth_rate = params$growth_rate,
        size_class_max = params$size_class_max,
        class_pops_max = params$class_pops_max,
        mgmt_cost = params$mgmt_cost,
        sample_cost = params$sample_cost,
        budget = params$budget
    )

    design$get_allocation()
    design$get_sensitivity()
    design$save_design()
}

setup_context <- function(params) {
    print(paste("setup_context:"))

    context <- Context(
        species_name            = params$species_name,
        species_type            = params$species_type,
        surveillance_purpose    = params$surveillance_purpose,
        surveillance_type       = params$surveillance_type,
        surv_qty_unit           = params$surv_qty_unit,
        cost_unit               = params$cost_unit,
        time_unit               = params$time_unit,
        dist_unit               = params$dist_unit,
        incursion_status        = params$incursion_status,
        area_freedom            = FALSE,
        market_access           = FALSE)
}
context <- setup_context(input.params$context)

run_method <- function(params, context) {
    
    if ("parameters" %in% names(params)) { # handle legacy parameter nesting
        params <- params$parameters
    }
    print(paste("run_method:", params$method_name))

    switch(params$method_name,
        discrete        = method_discrete_sampling(params, context),
        sampling        = method_discrete_sampling_legacy(params, context),
        multilevel      = method_multilevel_sampling(params, context),
        continuous      = method_continuous_surveillance(params, context),
        spatial         = switch(params$study_type,
                            raster = method_spatial_rast_legacy(params, context),
                            point = method_spatial_point_legacy(params, context)
                        ),
        range_kernel    = method_device_range_surveillance(params, context),
        area_growth     = method_areagrowth(params, context)
    )
}
run_method(input.params$method, context)

print("Simulation complete.")
