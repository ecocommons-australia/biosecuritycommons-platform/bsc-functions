## Platform wrapper for bsrmap::distance_weight_layer function
##
## https://github.com/cebra-analytics/bsrmap/
## 

SOURCE_DISTANCE_RAST    = 'distance-rast'
SOURCE_LOCATION_POINTS  = 'location-points'
DECAY_TYPE_NONE         = 'no-decay'
DECAY_TYPE_HALF         = 'half-decay'
DECAY_TYPE_BETA         = 'beta'
OUTPUT_FILENAME         = 'distance_weight_layer.tif'

library(bsrmap) 
paste("bsrmap:", packageVersion("bsrmap"))
paste("terra:", packageVersion("terra"))

# Load parameters
params = rjson::fromJSON(file="params.json")
input.params <- params$params
input.env <- params$env
rm(params)

# Set terra tmpdir to job scratch space
terra::terraOptions(tempdir = input.env$workdir)

# Set working directory
setwd(input.env$workdir)


x         <- NULL
y         <- NULL
weights   <- NULL
max_distance <- NULL
half_decay <- NULL


# Decay type
if ('decay_type' %in% names(input.params)){
  if (input.params$decay_type == DECAY_TYPE_NONE){
    beta = 0
  } else if (input.params$decay_type == DECAY_TYPE_HALF){

    if ('distance_unit' %in% names(input.params) && input.params$distance_unit == "km"){
      half_decay = input.params$half_decay
    } else {
      half_decay = input.params$half_decay/1000
    }

    beta = log(0.5)/half_decay
  } else {
    # Fallback to DECAY_TYPE_BETA
    beta = input.params$beta
  }
} else {
  # Legacy fallback
  beta = input.params$beta
}


# Precalculated raster
if ('source' %in% names(input.params) && input.params$source == SOURCE_DISTANCE_RAST){
  if ("distance_rast" %in% names(input.params)){
    print("source=distance-rast")

    x       <- terra::rast(input.params$distance_rast$filename)
    y       <- NULL
    
    weights <- NULL

    max_distance <- input.params$max_distance
    if ('distance_unit' %in% names(input.params) && input.params$distance_unit == "km"){
      x <- x*1000
      if (!is.null(max_distance)) {
        max_distance <- max_distance*1000
      }
    }
    
    # Set working directory to output
    setwd(input.env$outputdir)
    
    bsrmap::distance_weight_layer(
      x,
      beta = beta,
      max_distance = max_distance,
      weights = weights,
      filename = OUTPUT_FILENAME,
      gdal = c("COMPRESS=LZW", "TILED=YES"),
      NAflag = -3.4e+38,
      overwrite = TRUE)

  } else {
    stop("Param 'distance_rast' must be provided with source=distance-rast")
  }

} else {

  # Calculate from points (fallback)
  if ("location" %in% names(input.params)){
    print("source=location-points")

    x <- terra::rast(input.params$template$filename)
    y <- utils::read.csv(input.params$location$filename)

    if ("weights" %in% names(y)) {
      weights <- "weights" 
    } else { 
      weights <- input.params$weights
    }

    max_distance <- input.params$max_distance

    # Set working directory to output
    setwd(input.env$outputdir)

    bsrmap::distance_weight_layer(
      x,
      y,
      beta = beta,
      max_distance = max_distance,
      weights = weights,
      filename = OUTPUT_FILENAME,
      gdal = c("COMPRESS=LZW", "TILED=YES"),
      NAflag = -3.4e+38,
      overwrite = TRUE)

  } else {
    stop("Param 'location' must be provided with source=location-points")
  }
}





