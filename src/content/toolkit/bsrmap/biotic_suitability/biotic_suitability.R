## Platform wrapper for bsrmap::biotic_suitability function
##
## https://github.com/cebra-analytics/bsrmap/
## 

library(bsrmap)
paste("bsrmap:", packageVersion("bsrmap"))
paste("terra:", packageVersion("terra"))

# To do: Disable auxially file production, but does not work!
#rgdal::setCPLConfigOption("GDAL_PAM_ENABLED", "FALSE")

# Load parameters
params = rjson::fromJSON(file="params.json")
input.params <- params$params
input.env <- params$env
rm(params)

# Set terra tmpdir to job scratch space
terra::terraOptions(tempdir = input.env$workdir)

# Set working directory
setwd(input.env$workdir)


# Get biotic layers
biotic_layers <- sapply(input.params$biotic_layers, function(x) x$filename)

# First conform the input layers to the template
if (!is.null(input.params$template)) {
  template <- terra::rast(input.params$template$filename)
  biotic_layers <- terra::rast(lapply(biotic_layers, function(x) {
      bsrmap::conform_layer(
        terra::rast(x),
        template,
        normalize = input.params$normalize,
        binarize = input.params$binarize)
    }))
} else {
  biotic_layers <- terra::rast(biotic_layers)
}

# Set working directory to output (script runner takes care of it)
setwd(input.env$outputdir)

bsrmap::biotic_suitability(
  biotic_layers,
  use_fun = input.params$use_fun,
  na.rm = input.params$na.rm,
  filename = "biotic_suitability.tif",
  gdal = c("COMPRESS=LZW", "TILED=YES"),
  NAflag = -3.4e+38,
  overwrite = TRUE)
