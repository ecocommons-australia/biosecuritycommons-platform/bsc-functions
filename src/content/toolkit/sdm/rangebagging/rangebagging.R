# RANGEBAG SDM model
#
# Number of cpu cores to optimise for
DEFAULT_CPUS=8

if (Sys.getenv("TASK_CPUS") > 0){
  cpus = as.numeric(Sys.getenv("TASK_CPUS"))
} else {
  cpus = DEFAULT_CPUS
}

paste("available cpus:", cpus)

library(ecocommons)

# Read in the input parameters from param.json

source_file <- EC_read_json(file = "params.json")

print.model_parameters(source_file)

input.params  <- source_file$params # species, environment data and parameters
input.env     <- source_file$env # set workplace environment

# Print out parameters used
parameter.print(input.params)

# set random seed
EC_set_seed(input.params$random_seed)

# Set temp directory for terra
terra::terraOptions(tempdir = input.env$workdir)

# Set working directory (script runner takes care of it)
setwd(input.env$workdir)

# CoordinateCleaner
# Expects 'input/occurrences_cleaned.csv' to be present.
if (input.params$use_coord_cleaner == TRUE){
    occurrences_cleaned_path = file.path(input.env$workdir, 'input/occurrences_cleaned.csv')
    message('Using "', occurrences_cleaned_path, '" as occurrences')
    input.params$species_occurrence_dataset$filename = occurrences_cleaned_path
    if (file.exists(input.params$species_occurrence_dataset$filename) == FALSE){
      error('use_coord_cleaner = TRUE but cleaned records not found')
    }
}

# Always set FALSE so an unconstrained output is not created,
# as the standard output is effectively unconstrained.
input.params$unconstraint_map <- FALSE

# Data for modelling
response <- EC_build_response(input.params) # species data

predictor <- EC_build_predictor(input.params) # environmental data

constraint <- EC_build_constraint(input.params) # constraint area data

# read data and constraint to region of interest
dataset <- EC_build_dataset(input.env, predictor, constraint, response)

library(bssdm)

# Set parameters to perform modelling

# Specify the algorithm running the experiment
model_algorithm <- "rangebag"

species_algo_str <- ifelse(is.null(input.params$subset),
                           paste0(
                             response$occur_species, "_",
                             model_algorithm
                           ),
                           paste0(
                             response$occur_species, "_",
                             model_algorithm, input.params$subset
                           )
)

# Rangebag specific requirements

if (!all(predictor$type == "continuous")) {
  stop("Model doesn't run as Rangebag not support categorical data")
}
setwd(input.env$outputdir)

# Run rangebag with raster of enviro data, occurrence points, and other config
model_sdm <- rangebag(
  x = dataset$current_climate,
  p = dataset$occur,
  n_models = input.params$n_models,
  n_dim = input.params$n_dim,
  sample_prop = input.params$sample_prop,
  limit_occur = input.params$limit_occur
)

# Save the model object
EC_save(model_sdm,
        name = paste0(species_algo_str, "_model_object.rds"),
        input.env$outputdir
)

# Prediction over current climate scenario
projection_strings <- EC_save_projection_prep(
  input.env,
  species_algo_str
)

# Projection with constraint
model_proj <- predict(
  object = model_sdm,
  x = dataset$current_climate,
  raw_output = FALSE,
  filename = projection_strings$filename_tif,
  parallel_cores = cpus
)

# Save/move the projection as png and tif files
EC_save_projection_generic(
  model_proj = model_proj,
  filename_gg = projection_strings$filename_gg,
  filename_tif = projection_strings$filename_tif,
  plot_title = projection_strings$plot_title,
  move_tiff = FALSE
)

message("Rangebag profile completed!\n")
