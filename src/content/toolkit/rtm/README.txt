READ ME
Meryl Theng
14 Feb 2014

Pass the following command in Spartan with two arguments (cluster name, t_today)
`sbatch 00_god-handler.slurm Hunter 33`

The script will do the following 
- make a directory with the arguments as it's name (if it doesn't already exist)
- copy all the files from the template directory to that directory
- run the analysis