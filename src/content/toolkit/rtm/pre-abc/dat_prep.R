# Prepare the full data (i.e., all reported cases by end of outbreak) and distance matrix
source("functions_pre-abc.R")
source("functions_post-abc.R")
library(dplyr)
library(sf)

'------ Run via shell script ------'
# get the input passed from the shell script
args <- commandArgs(trailingOnly = TRUE)
str(args)
cat(args, sep = "\n")
if (length(args) != 1) {
  stop("Exactly 1 argument must be supplied (JSON params file).\n", call. = FALSE)
} else {
  print(paste("Arg input: ", args[1], sep = ' '))
  params_json = args[1]
}

'------ Parameters JSON ------'
params = rjson::fromJSON(file=params_json)$params

# return value of source[name] if defined, otherwise return the specified 'default' value.
get_param <- function(source, name, default) {
    if (name %in% names(source)) { 
        return(source[[name]])
    }
    print(paste0("get_param: no value for ", name, ", using default ", default))
    return(default)
}

# assumptions about disease periods
incubation_eq   = get_param(params$environment$assumptions, "incubation_eq", "rgamma(n=n(), shape=4, rate=2)")
incubation_eq
latent_eq       = get_param(params$environment$assumptions, "latent_eq", "1")
latent_eq
infectios_eq    = get_param(params$environment$assumptions, "infectios_eq", "6")
infectios_eq

# raw location data
dat_file = get_param(params$environment$data, "filename")
dat_file
dat_crs = get_param(params$environment$data, "crs", "epsg:4283")
dat_crs

dat_x = "xcoord"
dat_y = "ycoord"

# import and prep data
dat <-
  read.csv(dat_file) %>%
  crs_transform(dat = ., lon = dat_x, lat = dat_y, from = dat_crs, to = "epsg:3577") %>%
  mutate(clinical_date = as.Date(clinical_date, format = "%d/%m/%Y"), # for csv
         notification_date = as.Date(notification_date, format = "%d/%m/%Y"),
         removal_date = as.Date(removal_date, format = "%d/%m/%Y"),
         vacc_date = as.Date(vacc_date,format = "%d/%m/%Y"),
         x_km = round(changed_lon/1000, 3), y_km = round(changed_lat/1000, 3), area_h = area/10000
  ) %>%
  mutate(exposure_date = case_when(!is.na(clinical_date) ~ 
                                     clinical_date - round(eval(parse(text = incubation_eq), 0)),
                                                           TRUE ~ clinical_date))

# day 1 is set to arbitrary 7 days before first exposure
date0 = min(dat$exposure_date, na.rm=T) - 7

# last day of simulation
t_max = 100

# process event times as days
dat <- dat %>%
  mutate(t_c = as.numeric(clinical_date - date0),
         t_n = as.numeric(notification_date - date0),
         t_e = as.numeric(exposure_date - date0),
         t_i = t_c - round(eval(parse(text = latent_eq), 0)),
         #t_r = as.numeric(recovery_date - date0),
         t_r = t_i + round(eval(parse(text = infectios_eq), 0)),
         t_d = as.numeric(removal_date - date0),
         t_v = as.numeric(vacc_date - date0))

dat_cases <- prep_obs(dat, 1, t_max)
plot(dat_cases$E, type = "l", xlab="Day", ylab="No. cases")

# save
save(dat, date0, file = "prepped_data.RData")

