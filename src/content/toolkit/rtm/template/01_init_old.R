# ABC initialisation script

library(dplyr)
library(Rcpp)
library(BH)

'------ Run via shell script ------'
# get the input passed from the shell script
args <- commandArgs(trailingOnly = TRUE)
str(args)
cat(args, sep = "\n")

# test if there are three arguments: if not, return an error
if (length(args) != 3) {
  stop("Exactly 3 arguments must be supplied (input file).\n", call. = FALSE)
} else {
  print(paste("Arg input:", args[1], args[2], args[3]))
  ver = args[1]
  t_today = as.numeric(args[2])
  t_max = as.numeric(args[3])
}

'------ Set parameters (user inputs) ------'
# (I) Key timings
t_today; t_max # set in command line
t_lag = Sys.getenv('ABC_T_LAG', 3) # time lag in data (in days); model is fit to data up to t_today-t_lag

# (II) Epidemic model 
scenario  = Sys.getenv('ABC_SCENARIO', 10) # EI = 10, FMD = 20, etc.
dt.length = Sys.getenv('ABC_DT_LENGTH', 50) #50 for EI, 125 for FMD

# priors
para.names<-c('beta_intra','sigma_intra','gamma_intra',
              'alpha','beta0',
              'kernel_type','psi','rho0',
              'zeta','zeta_1','zeta_2','zeta_3',
              'chi',
              'xi','xi_1','xi_2','xi_3',
              'theta')

paras.index <- c(1,4,5,7,9,14,18) # to be inferred for EI
lm.low <- c(1,0,0,0,-2,-2,0) # unif prior lower bound for params
lm.upp <- c(20,0.02,0.02,20,2,2,1) # unif prior upper bound
paras.index.ratio <- NULL # paras on ratio scale; none for EI
paras.index.unif <- paras.index[which(!paras.index %in% paras.index.ratio)]
para.names[paras.index.unif]
paras.fixed <- c(2,3,6) # fixed params: sigma_intra, gamma_intra and kernel type (index)
para.names[paras.fixed]
fixed.vals <- c(1/1, 1/6,4)  # fixed values for: sigma_intra, gamma_intra and kernel type (values)


# intra-premises ODE simulation length in days
# dt.length = 50

# ------------------------------------------------------------------------------
# settings for FMD
# paras.index <- c(1,4,5,7:8,10:13,15:17) 
# lm.low<-c(1/10.8,0,0,0,0,              log10(c(.1,.1,.1)),  0,  log10(c(.1,.0001,.1))) 
# lm.upp<-c(30/10.8,0.0005,0.05,5,5,     log10(c(10,320,10)), 1,  log10(c(10,.1,10))) 
# cbind(para.names[paras.index], lm.low, lm.upp)
# paras.index.ratio <- c(10:12, 15:17); para.names[paras.index.ratio] #paras on ratio scale (i.e., zeta1 is cattle relatively to sheep[reference])
# paras.index.unif<-paras.index[which(!paras.index %in% paras.index.ratio)]; para.names[paras.index.unif]
# paras.fixed <- c(2:3, 6, 18)
# fixed.vals <- c(1/1.5, 1/10.8, 3, 0)
# dt.length = 125
# ------------------------------------------------------------------------------

# (III) ABC-SMC
N.particles = Sys.getenv('ABC_N_PARTICLES', 1000) # number of particles
n.cpus      = Sys.getenv('ABC_CPUS', 50) # this has to correspond to the no. of arrays you asked for in `02_stepx.slurm`, i.e.: #SBATCH --array=1-n.cpus
G           = Sys.getenv('ABC_N_GEN', 8) # number of generations


'------ Prep input data ------'
if (Sys.getenv('ABC_USE_GEN_FILE') != 0){
  g <- 1
  dput(g, 'Out/g.txt')
}

# load prepared data (full)
bp.file     = Sys.getenv('ABC_INPUT_FILE', "Data/prepped_data.RData")
load(bp.file)


# set key dates based on the data
t_detect <- min(dat$t_n, na.rm=T); t_detect

# epsilon per gen/step (tolerance)
temporal.tolerance.daily <- c(50, 40, 30, 20, 15, 10, 7.5, 6) # (i.e., +/- 50, 40, etc. cases per day); vector length =  G
temporal.tolerance.daily <- ceiling(temporal.tolerance.daily / 1000 * 
                                      nrow(dat)) # (optional) scale tolerance acc. to no. of premises in cluster
epsilon.temporal <- temporal.tolerance.daily * length(t_detect:(t_today-t_lag)); epsilon.temporal # overall temporal tolerance = daily tolerance * no. of days in fitting window

# spatial
epsilon.spatial <- c(0.3, 0.4, 0.5, 0.6, 0.65, 0.675, 0.7, 0.725) # correlation coefficient R; vector length =  G
cell.reso = 5 # resolution for spatial target (number of cells length-wise)

# set file location to save setup info for ABC (saved at the end of this script)
setup.file = "Out/data_ABC.RData" # this file needs to be setup before ABC

date0 + t_detect
date0 + t_max

# set start and end of simulation
t_start=1; t_end = t_max
t_detect; t_today # check times 

#############################################################
# Observed time delays
# Delay from onset of clinical signs to notification (dtCN), used as observed data in simulation
id <- which(dat$status == "IP")
dtCN <- dat$t_n[id] - dat$t_c[id]
dtCN[dtCN < 0] <- NA
# boxplot(dtCN ~ dat$t_n[id]) # boxplot of dtCN by notification time

# Compute time delays summary statistics
na.locf.hack<-function(vec){
  vec[which(is.infinite(vec))]<-NA
  if(is.na(vec[1])){vec[1]<-vec[min(which(!is.na(vec)))]}
  zoo::na.locf(vec)
}
time_delays <- tibble(t = t_detect:t_max) %>%
  rowwise() %>%
  mutate(
    dtCN_values = list(dtCN[dat$t_n[id] == t]),
    dtCN_mean = round(mean(dtCN_values, na.rm = TRUE)),
    dtND_mean = NA,
    dtCN_median = round(median(dtCN_values, na.rm = TRUE)),
    dtND_median = NA,
    dtCN_max = round(max(dtCN_values, na.rm = TRUE)),
    dtND_max = NA
  ) %>%
  select(-dtCN_values) %>%
  ungroup()

time_delays <- time_delays %>%
  mutate( # Replace Inf and NaN with Last Observation Carried Forward
    dtCN_mean = na.locf.hack(time_delays$dtCN_mean),
    dtCN_median = na.locf.hack(time_delays$dtCN_median),
    dtCN_max = na.locf.hack(time_delays$dtCN_max)
  )

# Delay from notification to depop (dtND) # FMD
if(any(!is.na(dat$t_d[id]))){ 
  dtND<-dat$t_d[id] - dat$t_n[id]
  dtND[dtND<0]<-NA
  # boxplot(dtND~dat$t_n[id])
  
  time_delays <- time_delays %>%
    mutate(
      dtND_values = list(dtND[dat$t_n[id] == t]),
      dtND_mean = round(mean(dtND_values, na.rm = TRUE)),
      dtND_median = round(median(dtND_values, na.rm = TRUE)),
      dtND_max = round(max(dtND_values, na.rm = TRUE))
    ) %>%
    select(-dtND_values) %>%
    ungroup() %>%
    mutate(
      dtND_mean = na.locf.hack(time_delays$dtCN_mean),
      dtND_median = na.locf.hack(time_delays$dtCN_media),
      dtND_max = na.locf.hack(time_delays$dtCN_max)
    )
}

# preserve cluster dat before setting t_today and subsetting
dat_real<-dat

#############################################################
# Prepare dat_today (i.e., what data would've looked like on day X)

# how many IPs are detected at this point
id<-which(dat$t_n<t_today); length(id)
table(dat$t_n[id])
  
# recodings for this run
dat$ip<-NA
dat$ip[id]<-1:length(id)
dat$status<-"NIL"
dat$status[id]<-"IP"

# non-IPs
dat$exposure_date[-id]<-NA
#dat$subclinical_date[-id]<-NA #FMD
dat$clinical_date[-id]<-NA
dat$notification_date[-id]<-NA
dat$removal_date[-id]<-NA
dat$t_c[-id]<-NA
dat$t_n[-id]<-NA
dat$t_e[-id]<-NA
dat$t_i[-id]<-NA
dat$t_r[-id]<-NA
dat$t_d[-id]<-NA
dat$t_v[-id]<-NA

dat_today <- dat # preserve dat_today, which will be used to prepare ABC targets 

#from t_today, set time_delays to the mean of last 7 days
time_delays_real<-time_delays #preserve
time_delays$dtCN_mean[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtCN_mean[(t_today-t_detect+2-1:7)]))
time_delays$dtCN_median[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtCN_median[(t_today-t_detect+2-1:7)]))
time_delays$dtCN_max[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtCN_max[(t_today-t_detect+2-1:7)]))
if(any(!is.na(time_delays$dtND_mean))){
  time_delays$dtND_mean[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtND_mean[(t_today-t_detect+2-1:7)]))
  time_delays$dtND_median[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtND_median[(t_today-t_detect+2-1:7)]))
  time_delays$dtND_max[(t_today-t_detect+2):nrow(time_delays)]<-round(mean(time_delays$dtND_max[(t_today-t_detect+2-1:7)]))
}
  
#############################################################
# set up data at t_detect (`dat`) to seed simulation with
# fit is between t_detect and t_today, seeded with status at t_detect

# how many are exposed at this point
id<-which(dat$t_e<t_detect); length(id) 

# recodings for this run
dat$ip<-NA
dat$ip[id]<-1:length(id)
dat$status<-"NIL"
dat$status[id]<-"IP"

# non-IPs
dat$t_c[-id]<-NA
dat$t_n[-id]<-NA
dat$t_e[-id]<-NA
dat$t_i[-id]<-NA
dat$t_r[-id]<-NA
dat$t_d[-id]<-NA
dat$t_v[-id]<-NA #keep the provided vaccination dates for EI

#############################################################
# Initialise parameters and present states, setup functions and accounting vectors, etc
N = nrow(dat); N

# setting up premises status accounting vectors (capitalised)
table(dat$t_e)
table(dat$t_i)

# indices of premises in each state, at time t (in order of the dataset, i.e. id_t)
dat$id_t<-1:N

# everyone exposed already
E_t <- dat$id_t[id]; E_t

# remove those infectious at t_det from E_t and put in I_t
id<-which(dat$t_i < t_detect)
I_t <- id; I_t
dat$t_i[I_t]
id<-match(I_t, E_t)
if(length(id)>0){E_t <- E_t[-id]}; E_t

R_t <- dat$id_t[which(dat$t_r < t_detect)]; R_t

# remove R_t from I_t
id<-match(R_t, I_t)
if(length(id)>0){I_t<-I_t[-id]}

# depoped
D_t <- dat$id_t[which(dat$t_d < t_detect)]; D_t
D <- length(D_t); D

id<-sort(c(E_t, I_t, R_t, D_t))
S_t <- dat$id_t[-id]; head(S_t)

# counts of premises in each state
S = length(S_t); S
E = length(E_t); E
I = length(I_t); I
R = length(R_t); R 
N
(S+E+I+R+D)==N

#############################################################
# calculate distance matrix with cpp function
Sys.setenv("PKG_CXXFLAGS"="-std=c++11") # set up cpp
sourceCpp("dyn_sim_rcpp.v15.cpp") #load cpp functions

distmat<-round(distmat_cppfunc(x=as.numeric(dat$x_km), y=as.numeric(dat$y_km), coord_type="cartesian")$distmat,3)

"------- Setup ABC targets --------"
# a. temporal target
E.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_e))) #exposures (infections)
I.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_i))) # onset of infectiousness
C.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_c))) # clinical onset
N.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_n))) # notification
R.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_r))) # recovery
D.obs <- sapply(t_detect:(t_today-t_lag), function(x) length(which(x == dat_today$t_d))) # depopulation

target.temporal <- data.frame(t = t_detect:(t_today-t_lag),
                                E = E.obs,
                                I = I.obs,
                                C = C.obs,
                                N = N.obs,
                                R = R.obs,
                                D = D.obs)


# b. spatial target
x_min = round(min(dat_real$x_km) - 1, 0)
x_max = round(max(dat_real$x_km) + 1, 0)
y_min = round(min(dat_real$y_km) - 1, 0)
y_max = round(max(dat_real$y_km) + 1, 0)

# exposures (infections) per cell at t_today
dat_ip <- dat_today[which(dat_today$t_e %in% t_detect:(t_today-t_lag)),] # infections (exposures) at t_today (minus t_lag)

target.spatial <- dat_ip %>% 
  mutate(
    cut_x = cut(x_km, breaks = seq(x_min, x_max, (x_max-x_min)/cell.reso), include.lowest = T),
    cut_y = cut(y_km, breaks = seq(y_min, y_max, (y_max-y_min)/cell.reso), include.lowest = T)) %>%
  count(cut_x, cut_y, .drop = F) %>%# number of infecteds per cell (final size)
  mutate(cell = 1:length(n)) %>%
  select(cell, n) 

# create index of cells with >0 premises (correlation calc only between cells that contain premises)
n_premises_cell <- dat_real %>% # no. of premises per cell
  mutate(
    cut_x = cut(x_km, breaks = seq(x_min, x_max, (x_max-x_min)/cell.reso), include.lowest = T),
    cut_y = cut(y_km, breaks = seq(y_min, y_max, (y_max-y_min)/cell.reso), include.lowest = T)) %>%
  count(cut_x, cut_y, .drop = F) %>%# number of infecteds per cell (final size)
  select(n)

cell_index <- which(n_premises_cell$n >0)

"------- Save setup file --------"
#with sim param setup stored
save.image(setup.file)
dput(setup.file, 'Out/setup.file.txt')

"------- 01_init.R finished ok --------"
  