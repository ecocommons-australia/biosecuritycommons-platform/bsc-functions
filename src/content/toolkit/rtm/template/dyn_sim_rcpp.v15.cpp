/*----------------------------*/
/*- header -------------------*/
/*----------------------------*/
/* C++ libraries to include */
#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <limits>
#include <iostream>
#include <iterator>
#include <sstream>
#include <stdio.h>          
#include <string>
#include <time.h>
#include <vector>          
#include <Rcpp.h>
// [[Rcpp::depends(BH)]]
#include <boost/random.hpp>
#include <boost/math/distributions.hpp>
#include <boost/generator_iterator.hpp>
#include <boost/random/uniform_real.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/numeric/odeint.hpp>

using namespace std;
//using namespace Rcpp; //leave commented out to explicitly denote Rcpp::functions
using namespace boost;
namespace odeint = boost::numeric::odeint; //creates namespace odeint::


/*----------------------------*/
/*- functions ----------------*/
/*----------------------------*/
int scenario;

//q_infectivity function
double zeta, zeta_1, zeta_2, zeta_3, chi;
double h_t_i;
int n_total_i, n_sheep_i, n_cattle_i, n_pigs_i, n_other_i, area_h_i;

inline double q_inf_func (int scenario, double zeta, double zeta_1, double zeta_2, double zeta_3, double chi,
                          double h_t_i, int n_total_i, int n_sheep_i, int n_cattle_i, int n_pigs_i, int n_other_i, double area_h_i){
    switch(scenario){
    case 10: //EI_baseline
        return(h_t_i * pow((n_total_i / area_h_i), zeta));
        break;
    case 20: //FMD_baseline
        return(h_t_i * (pow(n_sheep_i, chi) + zeta_1*pow(n_cattle_i, chi) + zeta_2*pow(n_pigs_i, chi) + zeta_3*pow(n_other_i, chi)));
        break;
    }
}

//s_susceptibility function
double xi, xi_1, xi_2, xi_3;
double theta_hat_j;
int n_total_j, n_sheep_j, n_cattle_j, n_pigs_j, n_other_j, area_h_j;

inline double s_susc_func (int scenario, double xi, double xi_1, double xi_2, double xi_3, double chi,
                           double theta_hat_j, int n_total_j, int n_sheep_j, int n_cattle_j, int n_pigs_j, int n_other_j, double area_h_j){
    switch(scenario){ 
    case 10: //EI_baseline
        return((1-theta_hat_j) * pow((n_total_j / area_h_j), xi));
        break;
    case 20: //FMD_baseline
        return((1-theta_hat_j) * (pow(n_sheep_j, chi) + xi_1*pow(n_cattle_j, chi) + xi_2*pow(n_pigs_j, chi) + xi_3*pow(n_other_j, chi)));
        break;
    }
}

//spatial kernels
double psi;
double rho0;
int kernel_type;
inline double kernel_func (double dist_ij, double psi, double rho0, int kernel_type){
    
    switch (kernel_type) {
      case 1: //fat-tailed
        return(1 / pow(dist_ij / rho0, psi));
        break;
      case 2: //power_law, type 1
        return(1.0 / (1.0 + pow(dist_ij, psi)));
        break;
      case 3: //power_law, type 2
        return(pow((1 + dist_ij/rho0), -psi));
        break;    
      case 4: //cauchy, type 1
        return(psi/(pow(psi,2) + pow(dist_ij,2)));
        break;
      case 5: //cauchy, type 2
        return(1 / (psi*(1 + pow(dist_ij / psi, 2.0))));
        break;     
      case 6: //exponential, type 1
        return(exp((-psi)*dist_ij));
        break;
      case 7: //exponential, type 2
        return(1 - exp(-pow(dist_ij/rho0, -psi)));
        break;    
      case 8: //exponential, type 3
        return(exp(-pow(dist_ij/rho0, psi)));
        break;  
    }
}



long double eucli_dist (double x_1 , double y_1, double x_2, double y_2, const string& coord_type_arg){
  
  long double eucli_dist_return;
  
  //cartesian coordinate system
  if (coord_type_arg == "cartesian") {
    eucli_dist_return = sqrt(pow((x_1 - x_2), 2) + pow((y_1 - y_2), 2)); //coordinates inputted in kilometres
  }
  
  
  //longlat coordinate system
  if (coord_type_arg == "latlong") {
    double pi = 3.1415926535897;
    double rad = pi / 180.0;
    double x_1_rad = x_1 * rad;
    double y_1_rad = y_1 * rad;
    double x_2_rad = x_2 * rad;
    double y_2_rad = y_2 * rad;
    double dlon = x_2_rad - x_1_rad;
    double dlat = y_2_rad - y_1_rad;
    double a = pow((sin(dlat / 2.0)), 2.0) + cos(y_1_rad) * cos(y_2_rad) * pow((sin(dlon / 2.0)), 2.0);
    double c = 2.0 * atan2(sqrt(a), sqrt(1.0 - a));
    double R = 6378.145;  //radius of Earth in km, output is scaled in km
    eucli_dist_return = R * c;
  }
  
  return(eucli_dist_return);
}


///// Boost probability distributions
// definition of type of random number generator for seeding later in main
typedef boost::mt19937 rng_type;

typedef boost::uniform_real<double> Dunif;
double runif(double x0, double x1, rng_type& rng_arg) {
    boost::variate_generator<rng_type &, Dunif > rndm(rng_arg, Dunif(x0, x1));
    return rndm();
}

typedef boost::uniform_int<int> Dunif_int;
int runif_int(int x0, int x1, rng_type& rng_arg)
{
    boost::variate_generator<rng_type &, Dunif_int > rndm(rng_arg, Dunif_int(x0, x1));
    return rndm();
}

// typedef boost::normal_distribution<double> Dnorm;
// double rnorm(double mean, double sd, rng_type& rng_arg) {
//   boost::variate_generator<rng_type &, Dnorm > rndm(rng_arg, Dnorm(mean, sd));
//   return rndm();
// }

typedef boost::gamma_distribution<double> Dgamma;
double rgamma(double shape, double scale, rng_type& rng_arg) { //note scale, not rate
    boost::variate_generator<rng_type &, Dgamma > rndm(rng_arg, Dgamma(shape, scale));
    return rndm();
}

// typedef boost::exponential_distribution<double> Dexp;
// double rexp(double rate, rng_type& rng_arg) { //seeded differently through rcpp than VS 
//   boost::variate_generator<rng_type &, Dexp > rndm(rng_arg, Dexp(rate));
//   return rndm();
// }

typedef boost::random::weibull_distribution<double> Dweibull;
double rweibull(double scale, double shape, rng_type& rng_arg) { //note reversed scale and shape from boost itself (shape=1.0 for exponential distribution)
    boost::variate_generator<rng_type &, Dweibull > rndm(rng_arg, Dweibull(shape, scale));
    return rndm();
}

typedef boost::poisson_distribution<int> Dpois;
int rpois(double mean, rng_type& rng_arg) {
    boost::variate_generator<rng_type &, Dpois > rndm(rng_arg, Dpois(mean));
    return rndm();
}

// typedef boost::bernoulli_distribution<double> Dbern;
// int rbern(double p, rng_type& rng_arg) {
//   boost::variate_generator<rng_type &, Dbern > rndm(rng_arg, Dbern(p));
//   return rndm();
// }

// empirical distribution random sampler (depends on runif function)
int edf_sample(vector<double> vec, rng_type& rng_arg) {
    double s = 0;
    vector<double> edf; // dynamic vector
    for (int k = 0; k < (int)(vec.size()); k++){
        s += vec[k];
        edf.push_back(s);
    }
    double u = s * runif(0.0, 1.0, rng_arg);
    vector<double>::iterator low;
    low = lower_bound(edf.begin(), edf.end(), u); // fast search for where to locate u in vector
    int i = int(low - edf.begin());
    return i;  // index from 0 of sampled element that u is within range
}


//seir (intra premises) ODE solver
typedef std::array<double,4> state_type; //array to store states for the seir table (1 column per state)
double beta_intra, sigma_intra, gamma_intra;
int n;
void seir_ode_func (const state_type &x, state_type &dxdt, double t_intra){
    
    dxdt[0] = - (beta_intra*x[0]*x[2]/n);
    dxdt[1] = + (beta_intra*x[0]*x[2]/n) - (sigma_intra*x[1]);
    dxdt[2] = + (sigma_intra*x[1]) - (gamma_intra * x[2]);
    dxdt[3] = + (gamma_intra * x[2]);
    
}


/*-------------------------------------------------------*/
/*- Rcpp functions exported to R ------------------------*/
/*-------------------------------------------------------*/


// [[Rcpp::export]]
Rcpp::List odes_cppfunc(
        double n_,
        double e_,
        Rcpp::NumericVector & paras,
        int dt_length
) {
    /*- int main -----------------*/
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    // Declarations
    
    Rcpp::NumericMatrix seir_table(dt_length, 5);
    colnames(seir_table) = Rcpp::CharacterVector({"time", "s", "e", "i", "r"});
    
    odeint::runge_kutta4< state_type > rk4;
    double t = 0.0;
    const double dt = 1;
    
    
    // variables
    beta_intra = paras[0];  //within-farm beta transmission rate (S to E)
    sigma_intra = paras[1]; //within-farm 1/latent period transmission rate (E to I)
    gamma_intra = paras[2]; //within-farm 1/infectious duration transmission rate (I to R)
    
    n = n_;
    double e = e_;
    double s = n-e;
    
    state_type states = {s, e, 0, 0};
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    // Main implementation
    
    seir_table(0, 0) = t;
    seir_table(0, 1) = states[0];
    seir_table(0, 2) = states[1];
    seir_table(0, 3) = states[2];
    seir_table(0, 4) = states[3];
    
    for( int i=1 ; i<dt_length ; ++i )
    {
        rk4.do_step( seir_ode_func, states, t , dt );
        t += dt;
        
        seir_table(i, 0) = t;
        seir_table(i, 1) = states[0];
        seir_table(i, 2) = states[1];
        seir_table(i, 3) = states[2];
        seir_table(i, 4) = states[3];
        
    }
    
    
    /*----------------------------*/
    /*----------------------------*/
    /*----------------------------*/
    //  Returns to R ----------------------------------------------
    return Rcpp::List::create(
        Rcpp::Named("seir_table") = seir_table);
}


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/

// [[Rcpp::export]]
Rcpp::List distmat_cppfunc(
    Rcpp::NumericVector & x,
    Rcpp::NumericVector & y,
    string coord_type
) {
  /*----------------------------*/
  /*- int main -----------------*/
  /*----------------------------*/  
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
  // Declarations
  
  Rcpp::NumericMatrix distmat(x.size(), x.size());
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
  // Main implementation
  
  
  for (int i = 0; i < x.size(); i++) {
    
    for (int j = 0; j < x.size(); j++) {
      
      distmat(i, j) = eucli_dist(x.at(i), y.at(i), x.at(j), y.at(j), coord_type);
      
    }
    
  }
  
  /*----------------------------*/
  /*----------------------------*/
  /*----------------------------*/
  //  Returns to R ----------------------------------------------
  return Rcpp::List::create(
    Rcpp::Named("distmat") = distmat);
}


/*----------------------------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------------------------*/

// [[Rcpp::export]]
Rcpp::List dyn_sim_cppfunc(
        int scenario_,
        int seed,
        int t_, //t_ used for i/o and elsewhere, because t saved for use in main loop
        int t_max,
        Rcpp::IntegerVector & n_total,
        Rcpp::IntegerVector & n_sheep,
        Rcpp::IntegerVector & n_cattle,
        Rcpp::IntegerVector & n_pigs,
        Rcpp::IntegerVector & n_other,
        Rcpp::IntegerVector & t_c,
        Rcpp::IntegerVector & t_n,
        Rcpp::IntegerVector & t_e,
        Rcpp::IntegerVector & t_i,
        Rcpp::IntegerVector & t_r,
        Rcpp::IntegerVector & t_d,
        Rcpp::IntegerVector & t_v,
        Rcpp::NumericVector & x_km,
        Rcpp::NumericVector & y_km,
        Rcpp::NumericVector & area_h,
        int S,
        int E,
        int I,
        int R,
        int D,
        int N,
        Rcpp::IntegerVector & S_t_, //S_t for vector for clearing
        Rcpp::IntegerVector & E_t_, //E_t for vector for clearing
        Rcpp::IntegerVector & I_t_, //I_t for vector for clearing
        Rcpp::IntegerVector & R_t_, //R_t for vector for clearing
        Rcpp::IntegerVector & D_t_,  //D_t for vector for clearing
        Rcpp::NumericVector & paras,
        Rcpp::NumericMatrix distmat,
        Rcpp::List h_list,
        int dt_length,
        Rcpp::NumericMatrix time_delays,
        bool print_prog
) {
    /*----------------------------*/
    /*- int main -----------------*/
    /*----------------------------*/  
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    // Declarations
    
    //random number generator
    rng_type rng(seed); //set a universal seed
    
    // constants
    scenario = scenario_;
    
    // variables
    beta_intra = paras[0];  //within-farm beta transmission rate (S to E)
    sigma_intra = paras[1]; //within-farm 1/latent period transmission rate (E to I)
    gamma_intra = paras[2]; //within-farm 1/infectious duration transmission rate (I to R)
    
    double alpha = paras[3]; //background transmission rate
    double beta0 = paras[4]; //baseline transmission rate
    
    kernel_type = paras[5];
    psi = paras[6];          //spatial kernel shape para
    rho0 = paras[7];         //spatial kernel shape para (Half-life)
    
    zeta = paras[8]; //the infectivity of all animals combined (i.e., for EI - single species)
    zeta_1 = paras[9]; //the infectivity of cattle to sheep
    zeta_2 = paras[10]; //the infectivity of pigs to sheep
    zeta_3 = paras[11]; //the infectivity of 'other' to sheep
    
    chi = paras[12]; //an index of the counts per species (see Jewell 2009, Bayesian Analysis)
    
    xi = paras[13]; //the susceptibility of all animals combined (i.e., for EI - single species)
    xi_1 = paras[14]; //the susceptibility of cattle to sheep
    xi_2 = paras[15]; //the susceptibility of pigs to sheep
    xi_3 = paras[16]; //the susceptibility of 'other' to sheep
    
    double theta = paras[17]; //vaccination effectiveness
    
    
    
    //copy Rcpp::vectors to std::vectors for clear and other std functions
    vector<int> S_t = Rcpp::as<vector<int> >(S_t_);
    vector<int> E_t = Rcpp::as<vector<int> >(E_t_);
    vector<int> I_t = Rcpp::as<vector<int> >(I_t_);
    vector<int> R_t = Rcpp::as<vector<int> >(R_t_);
    vector<int> D_t = Rcpp::as<vector<int> >(D_t_);

    vector<int> E_stars, I_stars, R_stars, D_stars, Inf_stars;
    vector<double> h_t, q_inf, s_susc, inf_pressures_ij, inf_pressures_j, inf_pressures_E_stars;
    //vector<vector<double>> inf_pressures_ij_mat_return;
    vector<double> inf_pressures_E_stars_row;
    double K_xy_ij, tau_t, theta_hat_j;
    double inf_pressures_ij_sum, inf_pressure_t;
    int infs_star;
    vector<int> infs, Inf_mat_from, Inf_mat_to, Inf_mat_t;
    
    //ODE (intra-premises) solver decs 
    odeint::runge_kutta4< state_type > rk4;
    double t_ode = 0.0;
    const double dt = 1; //time step for reporting ODE output
    
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
    // Main implementation
    
    //0. Initialisation at t: run ODEs to calculate h(t) and t_r for each I
    int EI = E + I;
    vector<int> EI_t = E_t;
    EI_t.insert(EI_t.end(), I_t.begin(), I_t.end());
    for (int i = 0; i < EI; i++) {
        
        int ei_index = EI_t.at(i) - 1;
        
        //run intra seir for h(.), t_i and t_r, per new infectee
        n = n_total[ei_index];
        //randomly sample how many are exposed to start
        double e = runif_int(1, n, rng) * 1.0;  
        double s = (n-e) * 1.0;
        
        vector<double> h_list_i(dt_length);
        
        //set up initial states
        state_type states = {s, e, 0};
        
        //run the solver per time step
        for( int tx=1 ; tx<dt_length ; ++tx )
        {
            rk4.do_step( seir_ode_func, states, t_ode , dt);
            t_ode += dt;
            h_list_i.at(tx) = states[2]/n;
        }
        
        //store the h_t for this new i in the h_list
        h_list[ei_index] = h_list_i;
        
        //update the t_r as the last day that this new i had >=0.5 animals infected
        int t_r_star = dt_length;
        for(int hx=0; hx < dt_length ; hx++){
            if(t_r_star == (dt_length)){
                
                if(h_list_i.at(dt_length - hx - 1)>=0.5){t_r_star = hx;}
                
            }
        }
        t_r_star = t_e[ei_index] + t_r_star; // v13 update
        
        //only update t_r if is.na(t_d) | t_r_star<t_d
        if(Rcpp::IntegerVector::is_na(t_d[ei_index]) == 1){
            
            if(Rcpp::IntegerVector::is_na(t_r[ei_index]) == 1){
                t_r[ei_index] = t_r_star;
            } else { //!is.na(t_r[ei_index])
                int t_r_now = t_r[ei_index];
                t_r[ei_index] = min(t_r_now, t_r_star);
            }
            
        } else { //!is.na(t_d[ei_index])
            
            if(t_r_star < t_d[ei_index]){
                
                if(Rcpp::IntegerVector::is_na(t_r[ei_index]) == 1){
                    t_r[ei_index] = t_r_star;
                } else { //!is.na(t_r[ei_index])
                    int t_r_now = t_r[ei_index];
                    t_r[ei_index] = min(t_r_now, t_r_star);
                }
                
            }
            
        }
        
        
        
    }
    
    //1. For each daily time step until t_max
    // - Update maximal infection pressures and proposed number of infections in next time step
    
    for (int t = t_; t <= t_max; t++) {
        
        //print progress
        if(print_prog == TRUE){
          cout << "t = " << t << "    S = " << S << "    E = " << E << "    I = " << I << "    R = " << R <<  "    D = " << D << endl;
        }
        
        //2. Check for any new infections, recoveries or depops and update accounting vectors
        for (int i = 0; i < N; i++) {
            if (t_i.at(i) == t) {
                
                
                I_t.push_back(i + 1);
                sort(I_t.begin(), I_t.end());
                
                I = I_t.size();
                
                for (int j = 0; j < ((int)E_t.size()); j++) {
                    if (E_t.at(j) == i + 1) {
                        
                        if(E_t.size()==1){
                            E_t.clear();
                            E = 0;
                        }
                        if(E_t.size()>1){
                            E_t.erase(E_t.begin() + j);
                            E = E_t.size();
                        }
                    }
                }
                
            }
        }
        
        for (int i = 0; i < N; i++) {
            if (t_r.at(i) == t) {
                
                R_t.push_back(i + 1);
                sort(R_t.begin(), R_t.end());
                
                R = R_t.size();
                
                for (int j = 0; j < (int)(I_t.size()); j++) {
                    if (I_t.at(j) == i + 1) {
                        
                        if(I_t.size()==1){
                            I_t.clear();
                            I = 0;
                        }
                        if(I_t.size()>1){
                            I_t.erase(I_t.begin() + j);
                            I = I_t.size();
                        }
                    }
                    
                }
            }
        }
        
        for (int i = 0; i < N; i++) {
            if (t_d.at(i) == t) {
                
                D_t.push_back(i + 1);
                sort(D_t.begin(), D_t.end());
                
                D = D_t.size();
                
                //erase any depoped from S_t, E_t, I_t and R_t, then recalc N
                for (int j = 0; j < (int)(S_t.size()); j++) {
                    if (S_t.at(j) == i + 1) {
                        
                        if(S_t.size()==1){
                            S_t.clear();
                            S = 0;
                        }
                        if(S_t.size()>1){
                            S_t.erase(S_t.begin() + j);
                            S = S_t.size();
                        }
                    }
                    
                }
                for (int j = 0; j < (int)(E_t.size()); j++) {
                    if (E_t.at(j) == i + 1) {
                        
                        if(E_t.size()==1){
                            E_t.clear();
                            E = 0;
                        }
                        if(E_t.size()>1){
                            E_t.erase(E_t.begin() + j);
                            E = E_t.size();
                        }
                    }
                    
                }
                for (int j = 0; j < (int)(I_t.size()); j++) {
                    if (I_t.at(j) == i + 1) {
                        
                        if(I_t.size()==1){
                            I_t.clear();
                            I = 0;
                        }
                        if(I_t.size()>1){
                            I_t.erase(I_t.begin() + j);
                            I = I_t.size();
                        }
                    }
                    
                }
                for (int j = 0; j < (int)(R_t.size()); j++) {
                    if (R_t.at(j) == i + 1) {
                        
                        if(R_t.size()==1){
                            R_t.clear();
                            R = 0;
                        }
                        if(R_t.size()>1){
                            R_t.erase(R_t.begin() + j);
                            R = R_t.size();
                        }
                    }
                    
                }
            }
        }
        
        if ((E + I) > 0) {
            
            //3. update infection pressures
            
            // extract h(.) for each I_t at time t, and calculate q_infs
            h_t.resize(I, 0); //the prevalence of infective individuals on each premises i at time t.
            q_inf.resize(I, 0);
            for (int i = 0; i < I; i++) {
                
                int i_index = I_t.at(i) - 1;
                vector<double> h_list_i = h_list[i_index];
                if((t - t_i[i_index])>1){
                    h_t.at(i) = h_list_i[t - t_i.at(i_index) - 1];
                    if(h_t.at(i) < 1E-10 || std::isnan(h_t.at(i))) {h_t.at(i) = 0;} // prevent very small values resulting in nan
                }
                if((t - t_i.at(i_index))<=1){
                    h_t.at(i) = 0;  
                }
                
                q_inf.at(i) = q_inf_func(scenario, zeta, zeta_1, zeta_2, zeta_3, chi, 
                         h_t[i], n_total[i_index], n_sheep[i_index], n_cattle[i_index], n_pigs[i_index], n_other[i_index], area_h[i_index]);
                
            }
            
            // calculate infection pressures on each j, then sum over the I_ts per j
            s_susc.resize(S, 0.0);
            inf_pressures_j.resize(S, 0.0);
            inf_pressure_t = 0.0;
            //remake inf_pressures_ij_mat with correct dimensions - simple to add rows, but not columns to 2D vectors
            vector<double> row(I, 0); // Initializing a single row of zeros
            vector<vector<double>> inf_pressures_ij_mat(S, row) ; // Initializing the 2-D vector
            
            for (int j = 0; j < S; j++) {
                
                int j_index = S_t.at(j) - 1;
                
                theta_hat_j = 0.0;
                if(t > t_v[j_index]){
                    
                    if(t < (t_v[j_index] + 14)){  theta_hat_j = theta * (t - t_v[j_index])/14;}
                    if(t >= (t_v[j_index] + 14)){ theta_hat_j = theta;}
                    
                }
                
                s_susc.at(j) = s_susc_func(scenario, xi, xi_1, xi_2, xi_3, chi, 
                          theta_hat_j, n_total[j_index], n_sheep[j_index], n_cattle[j_index], n_pigs[j_index], n_other[j_index], area_h[j_index]);
                
                
                inf_pressures_ij.resize(I, 0.0); //infection pressure from I_ts (cols) on each j (row) at t            
                inf_pressures_ij_sum = 0.0;
                for (int i = 0; i < I; i++) {
                    
                    int i_index = I_t.at(i) - 1;
                    K_xy_ij = kernel_func(distmat(i_index, j_index), psi, rho0, kernel_type);
                    
                    inf_pressures_ij.at(i) = q_inf.at(i) * K_xy_ij;
                    inf_pressures_ij_sum += inf_pressures_ij.at(i);
                    //store the infection pressures per j from each i in the matrix
                    inf_pressures_ij_mat[j][i] = inf_pressures_ij.at(i);
                }
                
                inf_pressures_j.at(j) = beta0 * s_susc.at(j) * inf_pressures_ij_sum;
                inf_pressure_t += inf_pressures_j.at(j);
                
            }
            
            //calculate tau (sum of infection pressure in system)
            tau_t = alpha + inf_pressure_t;
            
            //calculate number of infections proposed to occur in next daily time step
            infs_star = rpois(tau_t, rng);
            
            //4. Propose new infections and update accounting vectors
            
            if(infs_star>0){
                
                if(infs_star>S){infs_star = S;}
                
                E_stars.resize(infs_star, 0);
                Inf_stars.resize(infs_star, 0);
                inf_pressures_E_stars.resize(infs_star, 0.0);
                inf_pressures_E_stars_row.resize(I, 0.0);
                infs.resize(infs_star, 0);
                for (int i = 0; i < infs_star; i++) {
                    
                    // - propose infectees using edf (probability weighted based on infectious pressure at t)
                    E_stars.at(i) = edf_sample(inf_pressures_j, rng);
                    // E_stars are zeroed index of S_t: in cpp S_t[E_stars], in R S_t[res$E_stars + 1]
                    inf_pressures_E_stars.at(i) = inf_pressures_j.at(E_stars.at(i));
                    //retrospectively propose infectors using edf (probability weighted based on infectious pressures)  
                    for(int x=0; x < I; x++){
                        inf_pressures_E_stars_row.at(x) = inf_pressures_ij_mat[E_stars.at(i)][x];
                    }
                    Inf_stars.at(i) = edf_sample(inf_pressures_E_stars_row, rng);
                    // Inf_stars are zeroed index of I_t: in cpp I_t[Inf_stars], in R I_t[res$Inf_stars + 1]
                    
                    //locally thin the Poisson process (Karr, 1991)
                    double u = runif(0.0, 1.0, rng);
                    //if h_t > runif(1) then this infection occurs
                    //record this for later
                    infs.at(i) = (h_t[Inf_stars.at(i)]) > u;
                    if(infs.at(i)){
                        
                        //register the infection (exposure) times
                        int e_star_index = S_t[E_stars.at(i)] - 1;
                        t_e[e_star_index] = t;
                        //update the clinical onset and notification times
                        switch(scenario){
                        case 10: //EI_baseline
                            t_c[e_star_index] = t + rgamma(4, 2, rng);
                            t_i[e_star_index] = t_c[e_star_index] - 1;
                            t_n[e_star_index] = t_c[e_star_index] + 1;
                            //no depop
                            break;
                        case 20: //FMD_baseline
                            t_i[e_star_index] = t + rweibull(3.974, 1.782, rng);
                            t_c[e_star_index] = t_i[e_star_index] + rgamma(1.222, 1/1.672, rng);
                            // t_n[e_star_index] = t_c[e_star_index] + 1;
                            // t_d[e_star_index] = t_n[e_star_index] + 1;
                            t_n[e_star_index] = t_c[e_star_index] + runif_int(0, time_delays(t - t_, 5), rng); //dtCN_max
                            t_d[e_star_index] = t_n[e_star_index] + runif_int(0, time_delays(t - t_, 6), rng); //dtND_max
                            // t_n[e_star_index] = t_c[e_star_index] + time_delays(t - t_, 1); //dtCN_mean
                            // t_d[e_star_index] = t_n[e_star_index] + time_delays(t - t_, 2); //dtND_mean
                            //Simin code
                            // ** delayed notification due to sheep ** //
                            // if (n_sheep[e_star_index] > 0) {
                            //   t_n[e_star_index] = t + rpois(9.4509804, rng); // ** delayed notification from exposure
                            //   if (t_n[e_star_index] < t_c[e_star_index]) {
                            //     t_n[e_star_index] = t_c[e_star_index] + 1;
                            //   }
                            // } else {
                            //   t_n[e_star_index] = t_c[e_star_index] + 1;
                            // }
                            // 
                            // // ** delayed depopulation due to limited resources ** //
                            // if (t_n[e_star_index] < 60) {
                            //   int delayedday = rpois(1.98969889, rng); 
                            //   if (delayedday > 0) {
                            //     t_d[e_star_index] = t_n[e_star_index] + delayedday;
                            //   } else {
                            //     t_d[e_star_index] = t_n[e_star_index] + 1;
                            //   }
                            // } else {
                            //   int delayedday = rpois(1.47172619, rng); 
                            //   if (delayedday > 0) {
                            //     t_d[e_star_index] = t_n[e_star_index] + delayedday;
                            //   } else {
                            //     t_d[e_star_index] = t_n[e_star_index] + 1;
                            //   }
                            // }
                            
                            break;
                        }
                        //ensure onset of infectiousness is at least next day after exposure
                        t_i[e_star_index] = max(t_e[e_star_index] + 1, t_i[e_star_index]);
                        
                        //run intra seir for h(.), t_i and t_r, per new infectee
                        n = n_total[e_star_index];
                        //randomly sample how many are exposed to start
                        double e = runif_int(1, n, rng) * 1.0;  
                        double s = (n-e) * 1.0;
                        
                        vector<double> h_list_estar(dt_length);
                        
                        //set up initial states
                        state_type states = {s, e, 0};
                        
                        //run the solver per time step
                        for( int tx=1 ; tx<dt_length ; ++tx )
                        {
                            rk4.do_step( seir_ode_func, states, t_ode , dt);
                            t_ode += dt;
                            h_list_estar.at(tx) = states[2]/n;
                        }
                        
                        //store the h_t for this new i in the h_list
                        h_list[e_star_index] = h_list_estar;
                        
                        //update the t_r as the last day that this new i had >=0.5 animals infected
                        int t_r_star = dt_length;
                        for(int hx=0; hx < dt_length ; hx++){
                            if(t_r_star == (dt_length)){
                                
                                if(h_list_estar.at(dt_length - hx - 1)>=0.5){t_r_star = hx;}
                                
                            }
                        }
                        t_r_star = t + t_r_star;
                        
                        //only update t_r if is.na(t_d) | t_r_star<t_d
                        if(Rcpp::IntegerVector::is_na(t_d[e_star_index]) == 1){
                            
                            if(Rcpp::IntegerVector::is_na(t_r[e_star_index]) == 1){
                                t_r[e_star_index] = t_r_star;
                            } else { //!is.na(t_r[e_star_index])
                                int t_r_now = t_r[e_star_index];
                                t_r[e_star_index] = min(t_r_now, t_r_star);
                            }
                            
                        } else { //!is.na(t_d[e_star_index])
                            
                            if(t_r_star < t_d[e_star_index]){
                                
                                if(Rcpp::IntegerVector::is_na(t_r[e_star_index]) == 1){
                                    t_r[e_star_index] = t_r_star;
                                } else { //!is.na(t_r[e_star_index])
                                    int t_r_now = t_r[e_star_index];
                                    t_r[e_star_index] = min(t_r_now, t_r_star);
                                }
                                
                            }
                            
                        }
                        
                        //record the infection for this infector in the Inf_mat
                        Inf_mat_from.push_back(I_t[Inf_stars.at(i)]);
                        Inf_mat_to.push_back(S_t[E_stars.at(i)]);
                        Inf_mat_t.push_back(t);
                        
                        
                        //update accounting vectors for the new exposures, after everything else
                        int S_t_star = S_t[E_stars.at(i)];
                        
                        E_t.push_back(S_t_star);
                        E = E_t.size();
                        
                        for (int j = 0; j < (int)(S_t.size()); j++) {
                            if (S_t.at(j) == S_t_star) {
                                
                                if(S_t.size()==1){
                                    S_t.clear();
                                    S = 0;
                                }
                                if(S_t.size()>1){
                                    S_t.erase(S_t.begin() + j);
                                    S = S_t.size();
                                }
                                
                            }
                        }
                        
                        //remove S_t_star from  inf_pressures_j so can't be resampled as a new E
                        inf_pressures_j.erase(inf_pressures_j.begin() + E_stars.at(i));
                        inf_pressures_ij_mat.erase(inf_pressures_ij_mat.begin() + E_stars.at(i));
                    }
                    
                }
                
                
            }
            
            //debug: if need to copy out inf_pressures_ij_mat use inf_pressures_ij_mat_return
            //std::copy(inf_pressures_ij_mat.begin(), inf_pressures_ij_mat.end(), back_inserter(inf_pressures_ij_mat_return));    
            
        }
        
    }
    //cout << error_thrower;
    /*----------------------------*/
    /*----------------------------*/
    /*----------------------------*/
    //  Returns to R ----------------------------------------------
    return Rcpp::List::create(
        Rcpp::Named("seed") = seed,
        Rcpp::Named("t_c") = t_c,
        Rcpp::Named("t_n") = t_n,
        Rcpp::Named("t_e") = t_e,
        Rcpp::Named("t_i") = t_i,
        Rcpp::Named("t_r") = t_r,
        Rcpp::Named("t_d") = t_d,
        Rcpp::Named("S_t") = S_t,
        Rcpp::Named("E_t") = E_t,
        Rcpp::Named("I_t") = I_t,
        Rcpp::Named("R_t") = R_t,
        Rcpp::Named("D_t") = D_t,
        //Rcpp::Named("h_list") = h_list,
        Rcpp::Named("Inf_mat_from") = Inf_mat_from,
        Rcpp::Named("Inf_mat_to") = Inf_mat_to,
        Rcpp::Named("Inf_mat_t") = Inf_mat_t);
}