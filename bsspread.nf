#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsspread"
params.label    = "xxbigmem"

include { _main } from './main'

workflow {
    _main()
}