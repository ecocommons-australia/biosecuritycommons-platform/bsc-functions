#!/usr/bin/env nextflow
nextflow.preview.topic = true

/**
REAL-TIME MODELLING pipeline.
This uses the preview Nextflow 'topic' feature to collect particle iterations.
Although it has been realtively stable for a while this has possible 
implications when upgrading Nextflow. Alternatives can be implemented.
*/

// JobReqFunction[] (one per generation).
params.functions = (1..params.simulation.generations).toList().collect { [ sequence: it, uuid: 'NA' ] }

// Number of parallel tasks to run
params.tasks = 20
params.cpus = 1
params.memory = 1.GB

// Time limit per generation
params.gen_time_limit_min = 240
params.gen_time_limit_mult = 1.5

// Optimisation parameters for parallel tasks
params.particle_limit_skew = 0.85
params.particle_limit_skew_task_factor = -0.1 
params.particle_per_task_limit = 10
params.time_per_task_limit = '5min'

params.upload = true

// --------------------------------------

include { scan } from 'plugin/nf-boost'

include { 
    prepareInputs;
    RTMPreABC;
    RTMInit;
    RTMGen as RTMGen1;
    RTMGen as RTMGen2;
    RTMGen as RTMGen3;
    RTMGen as RTMGen4;
    RTMGen as RTMGen5;
    RTMGen as RTMGen6;
    RTMGen as RTMGen7;
    RTMGen as RTMGen8;
    RTMGen as RTMGen9;
    RTMGen as RTMGen10;
} from './nxf_modules/workflow_rtm.nf'

/**
 * RTM (Real-Time Modelling)
 * (add attribution)
 */
workflow {
    // Platform data

    prepareInputs('rtm', '', Channel.fromPath(params.inputs)) | RTMPreABC
    params_json = prepareInputs.out.params_json

    // Initialize model

    init = RTMInit(
        RTMPreABC.out.concat(params_json).collect(),
        params.environment.clus,
        params.environment.t_today,
        params.environment.t_max,
        params.simulation.generations
    )

    // Run each generation

    if (params.functions.size == 0)
        error "params.functions should be a List of JobReqFunction representing each generation!"

    params.functions.sequence.unique().sort().eachWithIndex { seq, idx ->

        // - Concat data from previous generation.
        // - Increase the run time limit for next generation.
        if (idx > 0) {
            data = init.concat(out)
            time_limit_min = params.gen_time_limit_min // time_limit_min * params.gen_time_limit_mult
        } else {
            data = init
            time_limit_min = params.gen_time_limit_min
        }

        data.toList().view { "[Gen ${seq}] data -> ${it}" } 

        out = "RTMGen$seq"(seq, data.collect(), 
            params.functions[idx].uuid,
            prepareInputs.out.params_json,
            params.simulation.n_particles,
            params.simulation.n_tasks,
            time_limit_min
        )
    }
}

