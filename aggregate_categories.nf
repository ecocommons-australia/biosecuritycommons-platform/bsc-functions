#!/usr/bin/env nextflow
nextflow.enable.dsl=2

params.function = "bsrmap/aggregate_categories"
params.label    = "xxbigmem"

include { _main } from './main'

workflow {
    _main()
}