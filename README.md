# Biosecurity Commons (BSC) Functions

Biosecurity Commons platform Workflows and computational pipelines.

#### Biosecurity Risk Mapping (bsrmap)

#### Biosecurity Dispersal Modelling (bsspread)

#### Biosecurity Surveillance Design (bsdesign)

#### Biosecurity Proof of Freedom (bsdesign_pof)

#### Biosecurity Impact Analysis (bsimpact)

#### Biosecurity Resource Allocation (bsmanage)

This repository contains the Biosecurity Commons (BSC) platform implementation for all workflows.  
Each workflow is implemented as one or more [Nextflow](nextflow.io) pipelines, with all packages and 
dependencies deployed as containers that execute within the pipelines.

All associated platform Templates, JSON Schemas and exemplars that support the platform are also deployed from this repo.

The core scientific code is implemented in R packages which are hosted at https://github.com/cebra-analytics. 

All artefacts are deployed via a Gitlab pipeline following general [platform conventions](https://gitlab.com/ecocommons-australia/operational-notes-and-scripts/ci-pipelines/-/blob/main/README.md?ref_type=heads).


## Workflow execution
The primary intent of this repository is to provide execution pipelines that are integrated with the 
BSC platform data sources and API's, however they are not strictly bound to these and can be executed 
independently by specifying alternative data sources as Nextflow parameters. 

For more information see:

## Running locally
See [docs/localdev.md](./docs/localdev.md)

## Nextflow computational pipelines
See [docs/nextflow.md](./docs/nextflow.md)

## Deployment information
See [docs/deployment.md](./docs/deployment.md)

## Managing dhange
[change.md](./docs/change.md)

## Testing
See [docs/testing.md](./docs/testing.md)

## Workflows
See [docs/workflows.md](./docs/workflows.md)